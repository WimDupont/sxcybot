package com.wimdupont.sxcybot;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@Disabled
@SpringBootTest
@ActiveProfiles("dev")
public class SxcyBotApplicationTests {

	@Test
	public void contextLoads() {
	}

}
