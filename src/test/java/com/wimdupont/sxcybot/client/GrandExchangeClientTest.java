package com.wimdupont.sxcybot.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wimdupont.sxcybot.exceptions.EntityNotFoundException;
import net.dv8tion.jda.api.entities.channel.middleman.MessageChannel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
class GrandExchangeClientTest {

    private GrandExchangeClient grandExchangeClient;

    @MockBean
    private MessageChannel messageChannel;

    @BeforeEach
    void setup() {
        grandExchangeClient = new GrandExchangeClient(new ObjectMapper());
    }

    @Test
    void getPrice() throws EntityNotFoundException {
        String itemName = "tinderbox";
        String result = grandExchangeClient.getPrice(itemName, messageChannel);
        System.out.println(result);

        assertNotNull(result);
        assertTrue(Integer.parseInt(result) > 0);
    }

    @Test
    void getPriceInvalidItem() {
        String itemName = "invalidItem";
        EntityNotFoundException exception = assertThrows(EntityNotFoundException.class, () ->
                grandExchangeClient.getPrice(itemName, messageChannel));

        assertEquals(String.format("Item with name %s not found.", itemName), exception.getMessage());
    }

}
