package com.wimdupont.sxcybot.client;

import com.wimdupont.sxcybot.exceptions.EntityNotFoundException;
import com.wimdupont.sxcybot.model.OsrsStat;
import com.wimdupont.sxcybot.repository.osrs.dao.OsrsHiscoreStat;
import com.wimdupont.sxcybot.services.osrs.OsrsHiscoreBossService;
import com.wimdupont.sxcybot.services.osrs.OsrsHiscoreStatService;
import net.dv8tion.jda.api.entities.channel.middleman.MessageChannel;
import net.dv8tion.jda.api.requests.restaction.MessageCreateAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.BDDMockito.given;

@ExtendWith(SpringExtension.class)
class HiScoreClientTest {

    @MockBean
    private OsrsHiscoreBossService osrsHiscoreBossService;
    @MockBean
    private OsrsHiscoreStatService osrsHiscoreStatService;
    @MockBean
    private MessageChannel messageChannel;
    @MockBean
    private MessageCreateAction messageCreateAction;

    private HiScoreClient hiScoreClient;

    @BeforeEach
    void setup() {
        hiScoreClient = new HiScoreClient(osrsHiscoreBossService, osrsHiscoreStatService);
    }

    @Test
    void getHiScoreStatsValid() throws EntityNotFoundException {
        String playerName = "Zxxy";
        List<OsrsHiscoreStat> osrsHiscoreStats = List.of(OsrsHiscoreStat.Builder.newBuilder()
                        .name("Overall")
                        .orderValue(0)
                        .build(),
                OsrsHiscoreStat.Builder.newBuilder()
                        .name("Attack")
                        .orderValue(1)
                        .build(),
                OsrsHiscoreStat.Builder.newBuilder()
                        .name("Defence")
                        .orderValue(2)
                        .build(),
                OsrsHiscoreStat.Builder.newBuilder()
                        .name("Strength")
                        .orderValue(3)
                        .build(),
                OsrsHiscoreStat.Builder.newBuilder()
                        .name("Hitpoints")
                        .orderValue(4)
                        .build()
        );
        given(osrsHiscoreStatService.findAll()).willReturn(osrsHiscoreStats);

        List<OsrsStat> result = hiScoreClient.getHiScoreStats(playerName, messageChannel);

        assertFalse(result.isEmpty());
        assertEquals(5, result.size());

        assertEquals("Overall", result.get(0).name());
        assertTrue(1939 <= Integer.parseInt(result.get(0).level()));

        assertEquals("Strength", result.get(3).name());
        assertEquals(99, Integer.parseInt(result.get(3).level()));

        assertEquals("Hitpoints", result.get(4).name());
        assertEquals(99, Integer.parseInt(result.get(4).level()));
    }

    @Test
    void getHiscoreStatsNonExistingPlayer() {
        String playerName = "007thisplayershouldneverexist007";
        given(messageChannel.sendMessage("No results found. ()")).willReturn(messageCreateAction);
        Exception exception = assertThrows(EntityNotFoundException.class, () ->
                hiScoreClient.getHiScoreStats(playerName, messageChannel));

        assertEquals(exception.getMessage(), String.format("No hiscores available for %s.", playerName));
    }

    @Test
    void getHiscoreBossKcNonExistingPlayer() {
        String playerName = "007thisplayershouldneverexist007";
        given(messageChannel.sendMessage("No results found. ()")).willReturn(messageCreateAction);

        Exception exception = assertThrows(EntityNotFoundException.class, () ->
                hiScoreClient.getHiScoreBossKc(playerName, messageChannel));

        assertEquals(exception.getMessage(), String.format("No hiscores available for %s.", playerName));
    }
}
