CREATE TABLE rule (
    id VARCHAR(36) primary key NOT NULL,
    `number` SMALLINT(6) NOT NULL UNIQUE,
    description VARCHAR(500) DEFAULT NULL
);

CREATE TABLE user (
    id VARCHAR(36) primary key NOT NULL,
    name VARCHAR(100) NOT NULL UNIQUE,
    description VARCHAR(1000) DEFAULT NULL,
    banned TINYINT(1) DEFAULT 0,
    created_date DATE,
    created_by VARCHAR(50),
    last_modified_date DATE,
    last_modified_by VARCHAR(50)
);

CREATE TABLE poll (
    id VARCHAR(36) primary key NOT NULL,
    message_id VARCHAR(100) NOT NULL UNIQUE,
    created_date DATE,
    last_modified_date DATE
);

CREATE TABLE guild_role (
    id VARCHAR(36) primary key NOT NULL,
    name VARCHAR(100) NOT NULL UNIQUE,
    order_value INT,
    elevation INT NOT NULL
);

CREATE TABLE channel_detail (
    id VARCHAR(36) primary key NOT NULL,
    forum_url VARCHAR(500),
    bot_update_channel VARCHAR(100),
    pvm_role_channel VARCHAR(100),
    pvm_role_general VARCHAR(100),
    pvm_role_raids VARCHAR(100),
    pvm_role_wilderness VARCHAR(100)
);

CREATE TABLE osrs_hiscore_stat (
    id VARCHAR(36) primary key NOT NULL,
    name VARCHAR(100) NOT NULL,
    order_value INT NOT NULL
);

CREATE TABLE osrs_hiscore_boss (
    id VARCHAR(36) primary key NOT NULL,
    name VARCHAR(100) NOT NULL,
    multiplier DECIMAL DEFAULT 0,
    order_value INT NOT NULL,
    pvm_role INT DEFAULT 0
);

CREATE TABLE pvm_kc_snapshot (
    id VARCHAR(36) primary key NOT NULL,
    created_date DATETIME,
    last_modified_date DATETIME
);

CREATE TABLE pvm_role_user (
    id VARCHAR(36) primary key NOT NULL,
    discord_name VARCHAR(100) NOT NULL UNIQUE,
    rsn VARCHAR(100) NOT NULL UNIQUE,
    created_date DATETIME,
    created_by VARCHAR(50),
    last_modified_date DATETIME,
    last_modified_by VARCHAR(50)
);

CREATE TABLE pvm_user_kc (
    id VARCHAR(36) primary key NOT NULL,
    pvm_role_user_id VARCHAR(36),
    CONSTRAINT fk_pvmroleuser FOREIGN KEY (pvm_role_user_id) REFERENCES pvm_role_user (id),
    osrs_hiscore_boss_id VARCHAR(36),
    CONSTRAINT fk_osrshiscoreboss FOREIGN KEY (osrs_hiscore_boss_id) REFERENCES osrs_hiscore_boss (id),
    pvm_kc_snapshot_id VARCHAR(36),
    CONSTRAINT fk_pvmkcsnapshot FOREIGN KEY (pvm_kc_snapshot_id) REFERENCES pvm_kc_snapshot (id),
    created_date DATETIME,
    last_modified_date DATETIME
);

CREATE TABLE guild_event_dmer (
    id VARCHAR(36) primary key NOT NULL,
    name VARCHAR(100) NOT NULL UNIQUE
);
