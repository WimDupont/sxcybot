ALTER TABLE pvm_user_kc ADD COLUMN IF NOT EXISTS kc INTEGER DEFAULT 0;
ALTER TABLE pvm_kc_snapshot ADD COLUMN IF NOT EXISTS pvm_role_user_id VARCHAR(36);
ALTER TABLE pvm_kc_snapshot ADD CONSTRAINT fk_pvmroleuser_snapshot FOREIGN KEY IF NOT EXISTS (pvm_role_user_id) REFERENCES pvm_role_user (id);
