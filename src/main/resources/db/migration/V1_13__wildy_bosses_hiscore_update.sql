UPDATE osrs_hiscore_boss SET order_value = order_value+1 WHERE order_value > 72;

INSERT INTO osrs_hiscore_boss
VALUES
(UUID(),"Spindel", 2, 73, 3);

UPDATE osrs_hiscore_boss SET order_value = order_value+1 WHERE order_value > 41;

INSERT INTO osrs_hiscore_boss
VALUES
(UUID(),"Cal'varion", 2, 42, 3);

UPDATE osrs_hiscore_boss SET order_value = order_value+1 WHERE order_value > 39;

INSERT INTO osrs_hiscore_boss
VALUES
(UUID(),"Artio", 2, 40, 3);

UPDATE osrs_hiscore_boss SET multiplier = 3 WHERE name = 'Callisto';
UPDATE osrs_hiscore_boss SET multiplier = 3 WHERE name = 'Chaos Elemental';
UPDATE osrs_hiscore_boss SET multiplier = 3 WHERE name = 'Scorpia';
UPDATE osrs_hiscore_boss SET multiplier = 3 WHERE name = 'Venenatis';
UPDATE osrs_hiscore_boss SET multiplier = 3 WHERE name = 'Vet\'ion';

UPDATE osrs_hiscore_boss SET multiplier = 2 WHERE name = 'Chaos Fanatic';
UPDATE osrs_hiscore_boss SET multiplier = 2 WHERE name = 'Crazy Archaeologist';
