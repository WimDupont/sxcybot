CREATE TABLE chat_data (
    id VARCHAR(36) primary key NOT NULL,
    discord_id VARCHAR(100) NOT NULL,
    channel_id VARCHAR(100) NOT NULL,
    message_id VARCHAR(100) NOT NULL,
    with_url TINYINT(1) DEFAULT 0,
    created_date_time DATETIME,
    last_modified_date_time DATETIME
);
