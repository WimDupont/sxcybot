ALTER TABLE pvm_role_user MODIFY discord_id VARCHAR(30) UNIQUE NOT NULL;
ALTER TABLE pvm_role_user DROP COLUMN IF EXISTS discord_name;
