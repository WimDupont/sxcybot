UPDATE osrs_hiscore_boss SET order_value = order_value+1 WHERE order_value > 87;

INSERT INTO osrs_hiscore_boss
VALUES
(UUID(),"Vardorvis", 1, 88, 1);

UPDATE osrs_hiscore_boss SET order_value = order_value+1 WHERE order_value > 80;

INSERT INTO osrs_hiscore_boss
VALUES
(UUID(),"The Whisperer", 1, 81, 1);

UPDATE osrs_hiscore_boss SET order_value = order_value+1 WHERE order_value > 80;

INSERT INTO osrs_hiscore_boss
VALUES
(UUID(),"The Leviathan", 1, 81, 1);

UPDATE osrs_hiscore_boss SET order_value = order_value+1 WHERE order_value > 58;

INSERT INTO osrs_hiscore_boss
VALUES
(UUID(),"Duke Sucellus", 1, 59, 1);
