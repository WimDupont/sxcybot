package com.wimdupont.sxcybot.repository.guild.dao;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import org.hibernate.annotations.UuidGenerator;


@Entity
public class GuildEventDmer {

    @Id
    @UuidGenerator
    private String id;

    private String discordUserId;

    private GuildEventDmer(Builder builder) {
        setId(builder.id);
        setDiscordUserId(builder.discordUserId);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDiscordUserId() {
        return discordUserId;
    }

    public void setDiscordUserId(String discordUserId) {
        this.discordUserId = discordUserId;
    }

    @SuppressWarnings("unused")
    protected GuildEventDmer() {
    }

    public static final class Builder {
        private String id;
        private String discordUserId;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder discordUserId(String val) {
            discordUserId = val;
            return this;
        }

        public GuildEventDmer build() {
            return new GuildEventDmer(this);
        }
    }

    @Override
    public String toString() {
        return "GuildEventDmer{" +
                "id='" + id + '\'' +
                ", discordUserId='" + discordUserId + '\'' +
                '}';
    }
}
