package com.wimdupont.sxcybot.repository.guild.dao;

import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.Id;
import org.hibernate.annotations.UuidGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDate;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class User {

    @Id
    @UuidGenerator
    private String id;
    private String name;
    private String description;
    private boolean banned;
    @CreatedDate
    private LocalDate createdDate;
    private String createdBy;
    @LastModifiedDate
    private LocalDate lastModifiedDate;
    private String lastModifiedBy;

    @SuppressWarnings("unused")
    protected User() {
    }

    private User(Builder builder) {
        setId(builder.id);
        setName(builder.name);
        setDescription(builder.description);
        setBanned(builder.banned);
        setCreatedDate(builder.createdDate);
        setCreatedBy(builder.createdBy);
        setLastModifiedDate(builder.lastModifiedDate);
        setLastModifiedBy(builder.lastModifiedBy);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isBanned() {
        return banned;
    }

    public void setBanned(boolean banned) {
        this.banned = banned;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDate lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public static final class Builder {
        private String id;
        private String name;
        private String description;
        private boolean banned;
        private LocalDate createdDate;
        private String createdBy;
        private LocalDate lastModifiedDate;
        private String lastModifiedBy;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder description(String val) {
            description = val;
            return this;
        }

        public Builder banned(boolean val) {
            banned = val;
            return this;
        }

        public Builder createdDate(LocalDate val) {
            createdDate = val;
            return this;
        }

        public Builder createdBy(String val) {
            createdBy = val;
            return this;
        }

        public Builder lastModifiedDate(LocalDate val) {
            lastModifiedDate = val;
            return this;
        }

        public Builder lastModifiedBy(String val) {
            lastModifiedBy = val;
            return this;
        }

        public User build() {
            return new User(this);
        }
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", banned=" + banned +
                ", createdDate=" + createdDate +
                ", createdBy='" + createdBy + '\'' +
                ", lastModifiedDate=" + lastModifiedDate +
                ", lastModifiedBy='" + lastModifiedBy + '\'' +
                '}';
    }
}
