package com.wimdupont.sxcybot.repository.guild;

import com.wimdupont.sxcybot.repository.guild.dao.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    Optional<User> findByName(String name);

    List<User> findAllByBannedTrue();
}
