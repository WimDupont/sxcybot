package com.wimdupont.sxcybot.repository.guild.dao;

import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.Id;
import org.hibernate.annotations.UuidGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDateTime;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class ChatData {

    @Id
    @UuidGenerator
    private String id;
    private String discordId;
    private String channelId;
    private String messageId;
    private boolean withUrl;
    @CreatedDate
    private LocalDateTime createdDateTime;
    @LastModifiedDate
    private LocalDateTime lastModifiedDateTime;

    @SuppressWarnings("unused")
    protected ChatData() {
    }

    private ChatData(Builder builder) {
        setId(builder.id);
        setDiscordId(builder.discordId);
        setChannelId(builder.channelId);
        setMessageId(builder.messageId);
        setWithUrl(builder.withUrl);
        setCreatedDateTime(builder.createdDateTime);
        setLastModifiedDateTime(builder.lastModifiedDateTime);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDiscordId() {
        return discordId;
    }

    public void setDiscordId(String discordId) {
        this.discordId = discordId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public boolean isWithUrl() {
        return withUrl;
    }

    public void setWithUrl(boolean withUrl) {
        this.withUrl = withUrl;
    }

    public LocalDateTime getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(LocalDateTime createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public LocalDateTime getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    public void setLastModifiedDateTime(LocalDateTime lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
    }

    public static final class Builder {
        private String id;
        private String discordId;
        private String channelId;
        private String messageId;
        private boolean withUrl;
        private LocalDateTime createdDateTime;
        private LocalDateTime lastModifiedDateTime;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder discordId(String val) {
            discordId = val;
            return this;
        }

        public Builder channelId(String val) {
            channelId = val;
            return this;
        }

        public Builder messageId(String val) {
            messageId = val;
            return this;
        }

        public Builder withUrl(boolean val) {
            withUrl = val;
            return this;
        }

        public Builder createdDateTime(LocalDateTime val) {
            createdDateTime = val;
            return this;
        }

        public Builder lastModifiedDateTime(LocalDateTime val) {
            lastModifiedDateTime = val;
            return this;
        }

        public ChatData build() {
            return new ChatData(this);
        }
    }
}
