package com.wimdupont.sxcybot.repository.guild;

import com.wimdupont.sxcybot.repository.guild.dao.ChatData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChatDataRepository extends JpaRepository<ChatData, String> {

    List<ChatData> findAllByDiscordIdAndWithUrlTrueOrderByCreatedDateTimeDesc(String discordId);
}
