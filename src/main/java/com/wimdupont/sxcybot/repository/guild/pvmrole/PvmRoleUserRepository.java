package com.wimdupont.sxcybot.repository.guild.pvmrole;

import com.wimdupont.sxcybot.repository.guild.pvmrole.dao.PvmRoleUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PvmRoleUserRepository extends JpaRepository<PvmRoleUser, String> {

    Optional<PvmRoleUser> findByDiscordId(String discordId);
    Optional<PvmRoleUser> findByRsn(String rsn);
}
