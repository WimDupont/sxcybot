package com.wimdupont.sxcybot.repository.guild.pvmrole;

import com.wimdupont.sxcybot.repository.guild.pvmrole.dao.PvmKcSnapshot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PvmKcSnapshotRepository extends JpaRepository<PvmKcSnapshot, String> {

    List<PvmKcSnapshot> findAllByPvmRoleUserId(String PvmRoleUserId);

}
