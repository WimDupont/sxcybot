package com.wimdupont.sxcybot.repository.guild.dao;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import org.hibernate.annotations.UuidGenerator;

@Entity
public class ChannelDetail {
    @Id
    @UuidGenerator
    private String id;
    private String forumUrl;
    private String botUpdateChannel;
    private String pvmRoleChannel;
    private String pvmRoleGeneral;
    private String pvmRoleRaids;
    private String pvmRoleWilderness;

    @SuppressWarnings("unused")
    protected ChannelDetail() {
    }

    private ChannelDetail(Builder builder) {
        setId(builder.id);
        setForumUrl(builder.forumUrl);
        setBotUpdateChannel(builder.botUpdateChannel);
        setPvmRoleChannel(builder.pvmRoleChannel);
        setPvmRoleGeneral(builder.pvmRoleGeneral);
        setPvmRoleRaids(builder.pvmRoleRaids);
        setPvmRoleWilderness(builder.pvmRoleWilderness);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getForumUrl() {
        return forumUrl;
    }

    public void setForumUrl(String forumUrl) {
        this.forumUrl = forumUrl;
    }

    public String getBotUpdateChannel() {
        return botUpdateChannel;
    }

    public void setBotUpdateChannel(String botUpdateChannel) {
        this.botUpdateChannel = botUpdateChannel;
    }

    public String getPvmRoleChannel() {
        return pvmRoleChannel;
    }

    public void setPvmRoleChannel(String pvmRoleChannel) {
        this.pvmRoleChannel = pvmRoleChannel;
    }

    public String getPvmRoleGeneral() {
        return pvmRoleGeneral;
    }

    public void setPvmRoleGeneral(String pvmRoleGeneral) {
        this.pvmRoleGeneral = pvmRoleGeneral;
    }

    public String getPvmRoleRaids() {
        return pvmRoleRaids;
    }

    public void setPvmRoleRaids(String pvmRoleRaids) {
        this.pvmRoleRaids = pvmRoleRaids;
    }

    public String getPvmRoleWilderness() {
        return pvmRoleWilderness;
    }

    public void setPvmRoleWilderness(String pvmRoleWilderness) {
        this.pvmRoleWilderness = pvmRoleWilderness;
    }

    public static final class Builder {
        private String id;
        private String forumUrl;
        private String botUpdateChannel;
        private String pvmRoleChannel;
        private String pvmRoleGeneral;
        private String pvmRoleRaids;
        private String pvmRoleWilderness;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder forumUrl(String val) {
            forumUrl = val;
            return this;
        }

        public Builder botUpdateChannel(String val) {
            botUpdateChannel = val;
            return this;
        }

        public Builder pvmRoleChannel(String val) {
            pvmRoleChannel = val;
            return this;
        }

        public Builder pvmRoleGeneral(String val) {
            pvmRoleGeneral = val;
            return this;
        }

        public Builder pvmRoleRaids(String val) {
            pvmRoleRaids = val;
            return this;
        }

        public Builder pvmRoleWilderness(String val) {
            pvmRoleWilderness = val;
            return this;
        }

        public ChannelDetail build() {
            return new ChannelDetail(this);
        }
    }

    @Override
    public String toString() {
        return "ChannelDetail{" +
                "id='" + id + '\'' +
                ", forumUrl='" + forumUrl + '\'' +
                ", botUpdateChannel='" + botUpdateChannel + '\'' +
                ", pvmRoleChannel='" + pvmRoleChannel + '\'' +
                ", pvmRoleGeneral='" + pvmRoleGeneral + '\'' +
                ", pvmRoleRaids='" + pvmRoleRaids + '\'' +
                ", pvmRoleWilderness='" + pvmRoleWilderness + '\'' +
                '}';
    }
}
