package com.wimdupont.sxcybot.repository.guild.pvmrole.dao;

import com.wimdupont.sxcybot.repository.osrs.dao.OsrsHiscoreBoss;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import org.hibernate.annotations.UuidGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDateTime;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class PvmUserKc {

    @Id
    @UuidGenerator
    private String id;

    @OneToOne
    @JoinColumn(name = "pvm_role_user_id")
    private PvmRoleUser pvmRoleUser;

    @OneToOne
    @JoinColumn(name = "osrs_hiscore_boss_id")
    private OsrsHiscoreBoss osrsHiscoreBoss;

    private Integer kc;

    @ManyToOne
    @JoinColumn(name = "pvm_kc_snapshot_id")
    private PvmKcSnapshot pvmKcSnapshot;

    @CreatedDate
    private LocalDateTime createdDate;
    @LastModifiedDate
    private LocalDateTime lastModifiedDate;

    @SuppressWarnings("unused")
    protected PvmUserKc() {

    }

    private PvmUserKc(Builder builder) {
        setId(builder.id);
        setPvmRoleUser(builder.pvmRoleUser);
        setOsrsHiscoreBoss(builder.osrsHiscoreBoss);
        setKc(builder.kc);
        setPvmKcSnapshot(builder.pvmKcSnapshot);
        setCreatedDate(builder.createdDate);
        setLastModifiedDate(builder.lastModifiedDate);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PvmRoleUser getPvmRoleUser() {
        return pvmRoleUser;
    }

    public void setPvmRoleUser(PvmRoleUser pvmRoleUser) {
        this.pvmRoleUser = pvmRoleUser;
    }

    public OsrsHiscoreBoss getOsrsHiscoreBoss() {
        return osrsHiscoreBoss;
    }

    public void setOsrsHiscoreBoss(OsrsHiscoreBoss osrsHiscoreBoss) {
        this.osrsHiscoreBoss = osrsHiscoreBoss;
    }

    public Integer getKc() {
        return kc;
    }

    public void setKc(Integer kc) {
        this.kc = kc;
    }

    public PvmKcSnapshot getPvmKcSnapshot() {
        return pvmKcSnapshot;
    }

    public void setPvmKcSnapshot(PvmKcSnapshot pvmKcSnapshot) {
        this.pvmKcSnapshot = pvmKcSnapshot;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public static final class Builder {
        private String id;
        private PvmRoleUser pvmRoleUser;
        private OsrsHiscoreBoss osrsHiscoreBoss;
        private Integer kc;
        private PvmKcSnapshot pvmKcSnapshot;
        private LocalDateTime createdDate;
        private LocalDateTime lastModifiedDate;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder pvmRoleUser(PvmRoleUser val) {
            pvmRoleUser = val;
            return this;
        }

        public Builder osrsHiscoreBoss(OsrsHiscoreBoss val) {
            osrsHiscoreBoss = val;
            return this;
        }

        public Builder kc(Integer val) {
            kc = val;
            return this;
        }

        public Builder pvmKcSnapshot(PvmKcSnapshot val) {
            pvmKcSnapshot = val;
            return this;
        }

        public Builder createdDate(LocalDateTime val) {
            createdDate = val;
            return this;
        }

        public Builder lastModifiedDate(LocalDateTime val) {
            lastModifiedDate = val;
            return this;
        }

        public PvmUserKc build() {
            return new PvmUserKc(this);
        }
    }

    @Override
    public String toString() {
        return "PvmUserKc{" +
                "id='" + id + '\'' +
                ", pvmRoleUser=" + pvmRoleUser +
                ", osrsHiscoreBoss=" + osrsHiscoreBoss +
                ", kc=" + kc +
                ", pvmKcSnapshot=" + pvmKcSnapshot +
                ", createdDate=" + createdDate +
                ", lastModifiedDate=" + lastModifiedDate +
                '}';
    }
}
