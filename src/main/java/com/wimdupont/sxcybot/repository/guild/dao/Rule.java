package com.wimdupont.sxcybot.repository.guild.dao;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import org.hibernate.annotations.UuidGenerator;

@Entity
public class Rule {

    @Id
    @UuidGenerator
    private String id;

    private int number;

    private String description;

    @SuppressWarnings("unused")
    protected Rule() {
    }

    private Rule(Builder builder) {
        setId(builder.id);
        setNumber(builder.number);
        setDescription(builder.description);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static final class Builder {
        private String id;
        private int number;
        private String description;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder number(int val) {
            number = val;
            return this;
        }

        public Builder description(String val) {
            description = val;
            return this;
        }

        public Rule build() {
            return new Rule(this);
        }
    }

    @Override
    public String toString() {
        return "Rule{" +
                "id='" + id + '\'' +
                ", number=" + number +
                ", description='" + description + '\'' +
                '}';
    }
}
