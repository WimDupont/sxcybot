package com.wimdupont.sxcybot.repository.guild;

import com.wimdupont.sxcybot.repository.guild.dao.Poll;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PollRepository extends JpaRepository<Poll, String> {

    Optional<Poll> findByMessageId(String messageId);
}
