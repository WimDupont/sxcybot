package com.wimdupont.sxcybot.repository.guild;

import com.wimdupont.sxcybot.repository.guild.dao.Rule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RuleRepository extends JpaRepository<Rule, String> {

    Optional<Rule> findByNumber(int number);

}
