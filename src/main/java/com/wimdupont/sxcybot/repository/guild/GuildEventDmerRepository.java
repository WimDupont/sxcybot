package com.wimdupont.sxcybot.repository.guild;

import com.wimdupont.sxcybot.repository.guild.dao.GuildEventDmer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GuildEventDmerRepository extends JpaRepository<GuildEventDmer, String> {
}
