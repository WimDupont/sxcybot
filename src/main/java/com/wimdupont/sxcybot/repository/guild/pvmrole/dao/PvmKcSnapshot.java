package com.wimdupont.sxcybot.repository.guild.pvmrole.dao;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import org.hibernate.annotations.UuidGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class PvmKcSnapshot {

    @Id
    @UuidGenerator
    private String id;

    @ManyToOne
    @JoinColumn(name = "pvm_role_user_id")
    private PvmRoleUser pvmRoleUser;

    @OneToMany(mappedBy = "pvmKcSnapshot", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<PvmUserKc> pvmUserKcList = new ArrayList<>();

    @CreatedDate
    private LocalDateTime createdDate;
    @LastModifiedDate
    private LocalDateTime lastModifiedDate;

    @SuppressWarnings("unused")
    protected PvmKcSnapshot() {

    }

    private PvmKcSnapshot(Builder builder) {
        setId(builder.id);
        setPvmRoleUser(builder.pvmRoleUser);
        setPvmUserKcList(builder.pvmUserKcList);
        setCreatedDate(builder.createdDate);
        setLastModifiedDate(builder.lastModifiedDate);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PvmRoleUser getPvmRoleUser() {
        return pvmRoleUser;
    }

    public void setPvmRoleUser(PvmRoleUser pvmRoleUser) {
        this.pvmRoleUser = pvmRoleUser;
    }

    public List<PvmUserKc> getPvmUserKcList() {
        return pvmUserKcList;
    }

    public void setPvmUserKcList(List<PvmUserKc> pvmUserKcList) {
        this.pvmUserKcList = pvmUserKcList;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }


    public static final class Builder {
        private String id;
        private PvmRoleUser pvmRoleUser;
        private List<PvmUserKc> pvmUserKcList;
        private LocalDateTime createdDate;
        private LocalDateTime lastModifiedDate;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder pvmRoleUser(PvmRoleUser val) {
            pvmRoleUser = val;
            return this;
        }

        public Builder pvmUserKcList(List<PvmUserKc> val) {
            pvmUserKcList = val;
            return this;
        }

        public Builder createdDate(LocalDateTime val) {
            createdDate = val;
            return this;
        }

        public Builder lastModifiedDate(LocalDateTime val) {
            lastModifiedDate = val;
            return this;
        }

        public PvmKcSnapshot build() {
            return new PvmKcSnapshot(this);
        }
    }

    @Override
    public String toString() {
        return "PvmKcSnapshot{" +
                "id='" + id + '\'' +
                ", pvmRoleUser=" + pvmRoleUser +
                ", pvmUserKcList=" + pvmUserKcList +
                ", createdDate=" + createdDate +
                ", lastModifiedDate=" + lastModifiedDate +
                '}';
    }
}
