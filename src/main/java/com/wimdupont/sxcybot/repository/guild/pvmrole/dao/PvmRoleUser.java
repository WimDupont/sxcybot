package com.wimdupont.sxcybot.repository.guild.pvmrole.dao;

import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.Id;
import org.hibernate.annotations.UuidGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDateTime;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class PvmRoleUser {

    @Id
    @UuidGenerator
    private String id;
    private String rsn;
    private String discordId;
    @CreatedDate
    private LocalDateTime createdDate;
    private String createdBy;
    @LastModifiedDate
    private LocalDateTime lastModifiedDate;
    private String lastModifiedBy;

    public String getDiscordId() {
        return discordId;
    }

    public void setDiscordId(String discordId) {
        this.discordId = discordId;
    }

    @SuppressWarnings("unused")
    protected PvmRoleUser() {

    }

    private PvmRoleUser(Builder builder) {
        setId(builder.id);
        setRsn(builder.rsn);
        setDiscordId(builder.discordId);
        setCreatedDate(builder.createdDate);
        setCreatedBy(builder.createdBy);
        setLastModifiedDate(builder.lastModifiedDate);
        setLastModifiedBy(builder.lastModifiedBy);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRsn() {
        return rsn;
    }

    public void setRsn(String rsn) {
        this.rsn = rsn;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public static final class Builder {
        private String id;
        private String rsn;
        private String discordId;
        private LocalDateTime createdDate;
        private String createdBy;
        private LocalDateTime lastModifiedDate;
        private String lastModifiedBy;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder rsn(String val) {
            rsn = val;
            return this;
        }

        public Builder discordId(String val) {
            discordId = val;
            return this;
        }

        public Builder createdDate(LocalDateTime val) {
            createdDate = val;
            return this;
        }

        public Builder createdBy(String val) {
            createdBy = val;
            return this;
        }

        public Builder lastModifiedDate(LocalDateTime val) {
            lastModifiedDate = val;
            return this;
        }

        public Builder lastModifiedBy(String val) {
            lastModifiedBy = val;
            return this;
        }

        public PvmRoleUser build() {
            return new PvmRoleUser(this);
        }
    }

    @Override
    public String toString() {
        return "PvmRoleUser{" +
                "id='" + id + '\'' +
                ", rsn='" + rsn + '\'' +
                ", discordId='" + discordId + '\'' +
                ", createdDate=" + createdDate +
                ", createdBy='" + createdBy + '\'' +
                ", lastModifiedDate=" + lastModifiedDate +
                ", lastModifiedBy='" + lastModifiedBy + '\'' +
                '}';
    }
}
