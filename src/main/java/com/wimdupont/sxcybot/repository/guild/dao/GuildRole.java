package com.wimdupont.sxcybot.repository.guild.dao;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import org.hibernate.annotations.UuidGenerator;


@Entity
public class GuildRole {

    @Id
    @UuidGenerator
    private String id;
    private String discordId;
    private Integer orderValue;
    private int elevation;

    @SuppressWarnings("unused")
    protected GuildRole() {
    }

    private GuildRole(Builder builder) {
        setId(builder.id);
        setDiscordId(builder.discordId);
        setOrderValue(builder.orderValue);
        setElevation(builder.elevation);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDiscordId() {
        return discordId;
    }

    public void setDiscordId(String discordId) {
        this.discordId = discordId;
    }

    public Integer getOrderValue() {
        return orderValue;
    }

    public void setOrderValue(Integer orderValue) {
        this.orderValue = orderValue;
    }

    public int getElevation() {
        return elevation;
    }

    public void setElevation(int elevation) {
        this.elevation = elevation;
    }

    public static final class Builder {
        private String id;
        private String discordId;
        private Integer orderValue;
        private int elevation;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder discordId(String val) {
            discordId = val;
            return this;
        }

        public Builder orderValue(Integer val) {
            orderValue = val;
            return this;
        }

        public Builder elevation(int val) {
            elevation = val;
            return this;
        }

        public GuildRole build() {
            return new GuildRole(this);
        }
    }

    @Override
    public String toString() {
        return "GuildRole{" +
                "id='" + id + '\'' +
                ", discordId='" + discordId + '\'' +
                ", orderValue=" + orderValue +
                ", elevation=" + elevation +
                '}';
    }
}
