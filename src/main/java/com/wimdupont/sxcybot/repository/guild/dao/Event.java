package com.wimdupont.sxcybot.repository.guild.dao;

import java.time.LocalDate;

public class Event {

    private String name;
    private String description;
    private LocalDate time;

    @SuppressWarnings("unused")
    protected Event() {

    }

    private Event(Builder builder) {
        setName(builder.name);
        setDescription(builder.description);
        setTime(builder.time);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getTime() {
        return time;
    }

    public void setTime(LocalDate time) {
        this.time = time;
    }

    public static final class Builder {
        private String name;
        private String description;
        private LocalDate time;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder description(String val) {
            description = val;
            return this;
        }

        public Builder time(LocalDate val) {
            time = val;
            return this;
        }

        public Event build() {
            return new Event(this);
        }
    }

    @Override
    public String toString() {
        return "Event{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", time=" + time +
                '}';
    }
}
