package com.wimdupont.sxcybot.repository.guild;

import com.wimdupont.sxcybot.repository.guild.dao.GuildRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GuildRoleRepository extends JpaRepository<GuildRole, String> {

    List<GuildRole> findByElevationLessThanEqual(Integer elevation);

    List<GuildRole> findByElevationGreaterThanEqual(Integer elevation);

    Optional<GuildRole> findByDiscordId(String discordId);
}
