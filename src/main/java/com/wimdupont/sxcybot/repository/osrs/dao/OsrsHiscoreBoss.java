package com.wimdupont.sxcybot.repository.osrs.dao;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import org.hibernate.annotations.UuidGenerator;


@Entity
public class OsrsHiscoreBoss implements OsrsHiscore {

    @Id
    @UuidGenerator
    private String id;
    private String name;
    private int orderValue;
    private double multiplier;
    private int pvmRole;

    @SuppressWarnings("unused")
    protected OsrsHiscoreBoss() {

    }

    private OsrsHiscoreBoss(Builder builder) {
        setId(builder.id);
        setName(builder.name);
        setOrderValue(builder.orderValue);
        setMultiplier(builder.multiplier);
        setPvmRole(builder.pvmRole);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOrderValue() {
        return orderValue;
    }

    public void setOrderValue(int orderValue) {
        this.orderValue = orderValue;
    }

    public double getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(double multiplier) {
        this.multiplier = multiplier;
    }

    public int getPvmRole() {
        return pvmRole;
    }

    public void setPvmRole(int pvmRole) {
        this.pvmRole = pvmRole;
    }

    public static final class Builder {
        private String id;
        private String name;
        private int orderValue;
        private double multiplier;
        private int pvmRole;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder orderValue(int val) {
            orderValue = val;
            return this;
        }

        public Builder multiplier(double val) {
            multiplier = val;
            return this;
        }

        public Builder pvmRole(int val) {
            pvmRole = val;
            return this;
        }

        public OsrsHiscoreBoss build() {
            return new OsrsHiscoreBoss(this);
        }
    }

    @Override
    public String toString() {
        return "OsrsHiscoreBoss{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", orderValue=" + orderValue +
                ", multiplier=" + multiplier +
                ", pvmRole=" + pvmRole +
                '}';
    }
}
