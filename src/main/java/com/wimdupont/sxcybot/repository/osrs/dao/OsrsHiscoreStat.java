package com.wimdupont.sxcybot.repository.osrs.dao;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import org.hibernate.annotations.UuidGenerator;


@Entity
public class OsrsHiscoreStat implements OsrsHiscore {

    @Id
    @UuidGenerator
    private String id;
    private String name;
    private int orderValue;

    @SuppressWarnings("unused")
    protected OsrsHiscoreStat() {

    }

    private OsrsHiscoreStat(Builder builder) {
        setId(builder.id);
        setName(builder.name);
        setOrderValue(builder.orderValue);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOrderValue() {
        return orderValue;
    }

    public void setOrderValue(int orderValue) {
        this.orderValue = orderValue;
    }


    public static final class Builder {
        private String id;
        private String name;
        private int orderValue;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder orderValue(int val) {
            orderValue = val;
            return this;
        }

        public OsrsHiscoreStat build() {
            return new OsrsHiscoreStat(this);
        }
    }

    @Override
    public String toString() {
        return "OsrsHiscoreStat{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", orderValue=" + orderValue +
                '}';
    }
}
