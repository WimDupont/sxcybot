package com.wimdupont.sxcybot.repository.osrs;

import com.wimdupont.sxcybot.repository.osrs.dao.OsrsHiscoreBoss;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OsrsHiscoreBossRepository extends JpaRepository<OsrsHiscoreBoss, String> {

    Optional<OsrsHiscoreBoss> findByName(String name);
}
