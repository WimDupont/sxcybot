package com.wimdupont.sxcybot.repository.osrs;

import com.wimdupont.sxcybot.repository.osrs.dao.OsrsHiscoreStat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OsrsHiscoreStatRepository extends JpaRepository<OsrsHiscoreStat, String> {
}
