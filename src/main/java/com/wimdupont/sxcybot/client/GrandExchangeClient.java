package com.wimdupont.sxcybot.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wimdupont.sxcybot.exceptions.EntityNotFoundException;
import com.wimdupont.sxcybot.model.OsrsItem;
import com.wimdupont.sxcybot.util.NumberFormatter;
import net.dv8tion.jda.api.entities.channel.middleman.MessageChannel;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.util.Map;

@Component
public class GrandExchangeClient {
    private static final String URL = "https://api.weirdgloop.org/exchange/history/osrs/latest?name=%s&lang=en";

    private final ObjectMapper objectMapper;

    public GrandExchangeClient(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @SuppressWarnings("unchecked")
    public String getPrice(String itemName, MessageChannel channel) throws EntityNotFoundException {
        RestTemplate restTemplate = new RestTemplateBuilder()
                .errorHandler(new ClientErrorHandler(channel))
                .setReadTimeout(Duration.ofSeconds(30))
                .build();
        Object result = restTemplate.getForEntity(String.format(URL, itemName), Object.class).getBody();
        if (result != null) {
            try {
                return NumberFormatter.format(objectMapper
                        .convertValue(((Map<String, Object>) result).values().iterator().next(), OsrsItem.class).price());
            } catch (IllegalArgumentException e) {
                throw new EntityNotFoundException(String.format("Item with name %s not found.", itemName));
            }
        } else {
            throw new EntityNotFoundException(String.format("Item with name %s not found.", itemName));
        }
    }
}
