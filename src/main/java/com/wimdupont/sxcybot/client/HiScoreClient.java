package com.wimdupont.sxcybot.client;

import com.wimdupont.sxcybot.exceptions.EntityNotFoundException;
import com.wimdupont.sxcybot.model.HiScoreBuilder;
import com.wimdupont.sxcybot.model.OsrsBossKc;
import com.wimdupont.sxcybot.model.OsrsStat;
import com.wimdupont.sxcybot.repository.osrs.dao.OsrsHiscore;
import com.wimdupont.sxcybot.repository.osrs.dao.OsrsHiscoreBoss;
import com.wimdupont.sxcybot.repository.osrs.dao.OsrsHiscoreStat;
import com.wimdupont.sxcybot.services.osrs.OsrsHiscoreBossService;
import com.wimdupont.sxcybot.services.osrs.OsrsHiscoreStatService;
import com.wimdupont.sxcybot.util.NumberFormatter;
import net.dv8tion.jda.api.entities.channel.middleman.MessageChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Component
public class HiScoreClient {

    private static final String URL = "https://secure.runescape.com/m=hiscore_oldschool/index_lite.ws?player=%s";
    private static final Logger LOGGER = LoggerFactory.getLogger(HiScoreClient.class);

    private final OsrsHiscoreBossService osrsHiscoreBossService;
    private final OsrsHiscoreStatService osrsHiscoreStatService;

    public HiScoreClient(OsrsHiscoreBossService osrsHiscoreBossService,
                         OsrsHiscoreStatService osrsHiscoreStatService) {
        this.osrsHiscoreBossService = osrsHiscoreBossService;
        this.osrsHiscoreStatService = osrsHiscoreStatService;
    }

    public List<OsrsStat> getHiScoreStats(String playername, MessageChannel channel) throws EntityNotFoundException {
        List<OsrsHiscoreStat> hiscoreStats = osrsHiscoreStatService.findAll();
        return getHiScores(playername, channel, (osrsStat, hiScoreStatValues, i) -> OsrsStat.Builder.newBuilder()
                .name(hiscoreStats.stream().filter(f -> f.getOrderValue() == i).findFirst().orElseThrow(()
                                -> new EntityNotFoundException(String.format("OsrsStatName for order %s not found!", i)))
                        .getName())
                .rank(NumberFormatter.format(hiScoreStatValues.getFirst()))
                .level(hiScoreStatValues.get(1))
                .experience(NumberFormatter.format(hiScoreStatValues.get(2)))
                .build(), getMinOrder(hiscoreStats), getMaxOrder(hiscoreStats));
    }

    public List<OsrsBossKc> getHiScoreBossKc(String playername, MessageChannel channel) throws EntityNotFoundException {
        List<OsrsHiscoreBoss> hiscoreBosses = osrsHiscoreBossService.findAll();
        return getHiScores(playername, channel, (osrsKc, hiScoreKcValues, i) -> OsrsBossKc.Builder.newBuilder()
                .name(hiscoreBosses.stream().filter(f -> f.getOrderValue() == i).findFirst().orElseThrow(()
                                -> new EntityNotFoundException(String.format("OsrsBoss for order %s not found!", i)))
                        .getName())
                .rank(hiScoreKcValues.getFirst())
                .kc(hiScoreKcValues.get(1))
                .build(), getMinOrder(hiscoreBosses), getMaxOrder(hiscoreBosses));
    }

    private <T> List<T> getHiScores(String playername, MessageChannel channel, HiScoreBuilder<T> hiScoreBuilder, int fromIndex, int toIndex) throws EntityNotFoundException {
        List<T> hiScoreList = new ArrayList<>();
        RestTemplate restTemplate = new RestTemplateBuilder()
                .errorHandler(new ClientErrorHandler(channel))
                .setReadTimeout(Duration.ofSeconds(30))
                .build();
        try {
            String result = restTemplate.getForObject(String.format(URL, playername), String.class);
            if (result == null) {
                throw new Exception();
            }
            int i = fromIndex;
            List<String> hiScores = Arrays.stream(result.split("\n")).toList();
            for (String osrsHiScoresStats : hiScores.subList(fromIndex, toIndex + 1)) {
                List<String> hiScoreStatValues = Arrays.stream(osrsHiScoresStats.split(",")).toList();
                hiScoreList.add(hiScoreBuilder.accept(osrsHiScoresStats, hiScoreStatValues, i));
                i++;
            }
            if (hiScoreList.isEmpty()) {
                throw new Exception();
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new EntityNotFoundException(String.format("No hiscores available for %s.", playername));
        }
        return hiScoreList;
    }

    private <T extends OsrsHiscore> int getMinOrder(List<T> osrsHiscores) {
        if (CollectionUtils.isEmpty(osrsHiscores)) {
            return 0;
        }
        return Collections.min(osrsHiscores, Comparator.comparing(OsrsHiscore::getOrderValue))
                .getOrderValue();
    }

    private <T extends OsrsHiscore> int getMaxOrder(List<T> osrsHiscores) {
        if (CollectionUtils.isEmpty(osrsHiscores)) {
            return 0;
        }
        return Collections.max(osrsHiscores, Comparator.comparing(OsrsHiscore::getOrderValue))
                .getOrderValue();

    }
}
