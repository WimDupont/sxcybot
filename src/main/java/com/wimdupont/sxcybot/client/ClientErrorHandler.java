package com.wimdupont.sxcybot.client;

import net.dv8tion.jda.api.entities.channel.middleman.MessageChannel;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;

import java.io.IOException;

public class ClientErrorHandler extends DefaultResponseErrorHandler {

    private final MessageChannel channel;

    public ClientErrorHandler(MessageChannel channel) {
        this.channel = channel;
    }

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        String errorMsg = String.format("No results found. (%s)", response.getStatusText());
        if (channel != null) {
            channel.sendMessage(errorMsg).queue();
        } else {
            throw new RuntimeException(errorMsg);
        }
    }

}
