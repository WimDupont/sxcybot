package com.wimdupont.sxcybot.listeners;

import com.wimdupont.sxcybot.enums.Command;
import com.wimdupont.sxcybot.listeners.member.CombatStatsListener;
import com.wimdupont.sxcybot.listeners.member.CustomPollListener;
import com.wimdupont.sxcybot.listeners.member.ForumListener;
import com.wimdupont.sxcybot.listeners.member.HelpListener;
import com.wimdupont.sxcybot.listeners.member.HiscoreBossListener;
import com.wimdupont.sxcybot.listeners.member.KillCountListener;
import com.wimdupont.sxcybot.listeners.member.PingListener;
import com.wimdupont.sxcybot.listeners.member.PollListener;
import com.wimdupont.sxcybot.listeners.member.PriceListener;
import com.wimdupont.sxcybot.listeners.member.PvmListListener;
import com.wimdupont.sxcybot.listeners.member.PvmRoleCheckListener;
import com.wimdupont.sxcybot.listeners.member.PvmRolePollListener;
import com.wimdupont.sxcybot.listeners.member.RuleListener;
import com.wimdupont.sxcybot.listeners.member.RulesListener;
import com.wimdupont.sxcybot.listeners.member.SpinListener;
import com.wimdupont.sxcybot.listeners.member.StatsListener;
import jakarta.annotation.Nonnull;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;

@Component
public class CommandListener {

    private final RuleListener ruleListener;
    private final RulesListener rulesListener;
    private final PingListener pingListener;
    private final ForumListener forumListener;
    private final PollListener pollListener;
    private final CustomPollListener customPollListener;
    private final HelpListener helpListener;
    private final StatsListener statsListener;
    private final CombatStatsListener combatStatsListener;
    private final PriceListener priceListener;
    private final KillCountListener killCountListener;
    private final PvmListListener pvmListListener;
    private final HiscoreBossListener hiscoreBossListener;
    private final PvmRoleCheckListener pvmRoleCheckListener;
    private final PvmRolePollListener pvmRolePollListener;
    private final SpinListener spinListener;

    public CommandListener(RuleListener ruleListener,
                           RulesListener rulesListener,
                           PingListener pingListener,
                           ForumListener forumListener,
                           PollListener pollListener,
                           CustomPollListener customPollListener,
                           HelpListener helpListener,
                           StatsListener statsListener,
                           CombatStatsListener combatStatsListener,
                           PriceListener priceListener,
                           KillCountListener killCountListener,
                           PvmListListener pvmListListener,
                           HiscoreBossListener hiscoreBossListener,
                           PvmRoleCheckListener pvmRoleCheckListener,
                           PvmRolePollListener pvmRolePollListener,
                           SpinListener spinListener) {
        this.ruleListener = ruleListener;
        this.rulesListener = rulesListener;
        this.pingListener = pingListener;
        this.forumListener = forumListener;
        this.pollListener = pollListener;
        this.customPollListener = customPollListener;
        this.helpListener = helpListener;
        this.statsListener = statsListener;
        this.combatStatsListener = combatStatsListener;
        this.priceListener = priceListener;
        this.killCountListener = killCountListener;
        this.pvmListListener = pvmListListener;
        this.hiscoreBossListener = hiscoreBossListener;
        this.pvmRoleCheckListener = pvmRoleCheckListener;
        this.pvmRolePollListener = pvmRolePollListener;
        this.spinListener = spinListener;
    }

    public void executeCommand(@Nonnull MessageReceivedEvent event, Command command) {
        switch (command) {
            case RULE -> ruleListener.process(event);
            case RULES -> rulesListener.process(event);
            case PING -> pingListener.process(event);
            //TODO
//                        case EVENT:
//                            eventListener.proces(event);
//                            break;
            case CBSTATS -> combatStatsListener.process(event);
            case STATS -> statsListener.process(event);
            case PRICE -> priceListener.process(event);
            case FORUM -> forumListener.process(event);
            case POLL -> pollListener.process(event);
            case CPOLL -> customPollListener.process(event);
            case KC -> killCountListener.process(event);
            case PVMLIST -> pvmListListener.process(event);
            case BOSSLIST -> hiscoreBossListener.process(event);
            case PVMPOLL -> pvmRolePollListener.process(event);
            case PVMCHECK -> pvmRoleCheckListener.process(event);
            case SPIN -> spinListener.process(event);
            case HELP -> helpListener.process(event);
        }
    }

}
