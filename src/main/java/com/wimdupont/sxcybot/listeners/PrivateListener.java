package com.wimdupont.sxcybot.listeners;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public interface PrivateListener {

    void process(MessageReceivedEvent privateEvent, MessageReceivedEvent event);
}
