package com.wimdupont.sxcybot.listeners;

import com.wimdupont.sxcybot.services.guild.PollService;
import com.wimdupont.sxcybot.util.JdaUtil;
import jakarta.annotation.Nonnull;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.MessageEmbed.Field;
import net.dv8tion.jda.api.events.message.react.GenericMessageReactionEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Component
public class PollReactionListener extends ListenerAdapter {

    private final PollService pollService;

    public PollReactionListener(PollService pollService) {
        this.pollService = pollService;
    }

    @Override
    public void onMessageReactionAdd(@Nonnull MessageReactionAddEvent event) {
        if (isPoll(event)) {
            event.getChannel().retrieveMessageById(event.getMessageId()).queue(message -> {
                if (message.getEmbeds().size() == 1) {
                    MessageEmbed messageEmbed = message.getEmbeds().getFirst();
                    EmbedBuilder embedBuilder = new EmbedBuilder();
                    embedBuilder.setTitle(messageEmbed.getTitle());
                    embedBuilder.setColor(messageEmbed.getColor());
                    embedBuilder.setFooter(String.format("Last voted on %s", LocalDate.now()), event.getGuild().getIconUrl());
                    String username = JdaUtil.getName(event).orElse(null);
                    for (MessageEmbed.Field field : messageEmbed.getFields()) {
                        List<String> nameSplit = field.getValue() != null
                                ? new ArrayList<>(Arrays.asList(field.getValue().split(System.lineSeparator())))
                                : new ArrayList<>();
                        if (field.getValue() != null && username != null && nameSplit.remove(username)) {
                            String value = String.join(System.lineSeparator(), nameSplit);
                            embedBuilder.addField(field);
                            embedBuilder.getFields()
                                    .set(messageEmbed.getFields().indexOf(field),
                                            new Field(getFieldNameWithCount(field, value)
                                                    .orElse(null), value, field.isInline()));
                        } else {
                            if (field.getName() != null && field.getName().contains(event.getEmoji().getName())) {
                                String value = field.getValue() != null && !field.getValue().contains("‎")
                                        ? field.getValue() + System.lineSeparator() + username
                                        : username;
                                embedBuilder.addField(getFieldNameWithCount(field, value)
                                        .orElse(null), value, field.isInline());
                            } else {
                                embedBuilder.addField(field);
                            }
                        }
                    }
                    message.editMessageEmbeds(embedBuilder.build()).queue();
                    if (event.getUser() != null)
                        message.removeReaction(event.getReaction().getEmoji(), event.getUser()).queue();
                }
            });
        }
    }

    public Optional<String> getFieldNameWithCount(Field field, String updatedValue) {
        return Optional.ofNullable(field.getName())
                .map(f -> f.replaceAll(
                        "\\(+\\d+\\)+",
                        String.format("(%s)", getCount(updatedValue))));
    }

    private long getCount(String fieldValue) {
        if (fieldValue.isEmpty()) {
            return 0;
        }
        return Arrays.stream(fieldValue.split(System.lineSeparator())).count();

    }


    //TODO remove if not used
//    @Override
//    public void onMessageReactionRemove(@Nonnull MessageReactionRemoveEvent event) {
//        //event.user is null here!
//        if (pollService.findByMessageId(event.getMessageId()).isPresent()) {
//            event.getJDA().retrieveUserById(event.getUserId()).queue(user -> {
//                if (!user.isBot()) {
//                    event.getChannel().retrieveMessageById(event.getMessageId()).queue(message -> {
//                        if (message.getEmbeds().size() == 1) {
//                            MessageEmbed messageEmbed = message.getEmbeds().get(0);
//                            EmbedBuilder embedBuilder = new EmbedBuilder();
//                            embedBuilder.setTitle(messageEmbed.getTitle());
//                            embedBuilder.setColor(messageEmbed.getColor());
//                            for (MessageEmbed.Field field : messageEmbed.getFields()) {
//                                embedBuilder.addField(field);
//                                if (field.getName() != null && field.getName().contains(event.getReactionEmote().getEmoji())) {
//                                    if (field.getValue() != null) {
//                                        embedBuilder.getFields()
//                                                .set(messageEmbed.getFields().indexOf(field),
//                                                        new Field(field.getName(), field.getValue().replace(user.getName(), ""), field.isInline()));
//                                        message.editMessageEmbeds(embedBuilder.build()).queue();
//                                    }
//                                }
//                            }
//                            message.editMessageEmbeds(embedBuilder.build()).queue();
//                        }
//                    });
//                }
//            });
//        }
//    }

    private boolean isPoll(GenericMessageReactionEvent event) {
        return event.getUser() != null
                && !event.getUser().isBot()
                && pollService.findByMessageId(event.getMessageId()).isPresent();
    }
}
