package com.wimdupont.sxcybot.listeners;

import com.wimdupont.sxcybot.enums.Command.Admin;
import com.wimdupont.sxcybot.exceptions.InsufficientPrivilegesException;
import com.wimdupont.sxcybot.listeners.admin.BanlistListener;
import com.wimdupont.sxcybot.listeners.admin.BossUpdateMultiplierListener;
import com.wimdupont.sxcybot.listeners.admin.EditBanlistListener;
import com.wimdupont.sxcybot.listeners.admin.EditPvmListener;
import com.wimdupont.sxcybot.listeners.admin.EditRoleListener;
import com.wimdupont.sxcybot.listeners.admin.EditRuleListener;
import com.wimdupont.sxcybot.listeners.admin.RoleAssignListener;
import com.wimdupont.sxcybot.services.AccessService;
import jakarta.annotation.Nonnull;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.springframework.stereotype.Component;

import java.util.stream.Stream;

import static com.wimdupont.sxcybot.util.Constants.ROLE_ACCESS.ADMIN_ROLE;
import static com.wimdupont.sxcybot.util.Constants.ROLE_ACCESS.GENERAL;
import static com.wimdupont.sxcybot.util.Constants.ROLE_ACCESS.GOD;
import static com.wimdupont.sxcybot.util.Constants.ROLE_ACCESS.STAFF_ROLE;
import static com.wimdupont.sxcybot.util.Constants.ROLE_ACCESS.TEACHER;

@Component
public class AdminCommandListener extends ListenerAdapter {


    private final EditRuleListener editRuleListener;
    private final EditBanlistListener editBanlistListener;
    private final RoleAssignListener roleAssignListener;
    private final BanlistListener banlistListener;
    private final EditRoleListener editRoleListener;
    private final EditPvmListener editPvmListener;
    private final BossUpdateMultiplierListener bossUpdateMultiplierListener;
    private final AccessService accessService;

    public AdminCommandListener(EditRuleListener editRuleListener,
                                EditBanlistListener editBanlistListener,
                                RoleAssignListener roleAssignListener,
                                BanlistListener banlistListener,
                                EditRoleListener editRoleListener,
                                EditPvmListener editPvmListener,
                                BossUpdateMultiplierListener bossUpdateMultiplierListener,
                                AccessService accessService) {
        this.editRuleListener = editRuleListener;
        this.editBanlistListener = editBanlistListener;
        this.roleAssignListener = roleAssignListener;
        this.banlistListener = banlistListener;
        this.editRoleListener = editRoleListener;
        this.editPvmListener = editPvmListener;
        this.bossUpdateMultiplierListener = bossUpdateMultiplierListener;
        this.accessService = accessService;
    }

    public void executeCommand(@Nonnull MessageReceivedEvent event, Admin adminCommand) {
        if (event.getMember() != null) {
            Stream<Role> roleStream = event.getMember().getRoles().stream();
            try {
                switch (adminCommand) {
                    case EDITRULE -> {
                        accessService.isAllowed(roleStream, event, ADMIN_ROLE);
                        editRuleListener.process(event);
                    }
                    case EDITBANLIST -> {
                        accessService.isAllowed(roleStream, event, ADMIN_ROLE);
                        editBanlistListener.process(event);
                    }
                    case ROLE -> {
                        accessService.isAllowed(roleStream, event, TEACHER);
                        roleAssignListener.process(event);
                    }
                    case BANLIST -> {
                        accessService.isAllowed(roleStream, event, STAFF_ROLE);
                        banlistListener.process(event);
                    }
                    case EDITROLE -> {
                        accessService.isAllowed(roleStream, event, GOD);
                        editRoleListener.process(event);
                    }
                    case EDITPVM -> {
                        accessService.isAllowed(roleStream, event, STAFF_ROLE);
                        editPvmListener.process(event);
                    }
                    case EDITBOSS -> {
                        accessService.isAllowed(roleStream, event, GENERAL);
                        bossUpdateMultiplierListener.process(event);
                    }
                }
            } catch (InsufficientPrivilegesException e) {
                event.getChannel().sendMessage(e.getMessage()).queue();
            }
        }
    }

}
