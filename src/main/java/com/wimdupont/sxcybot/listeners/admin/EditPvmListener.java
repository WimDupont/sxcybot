package com.wimdupont.sxcybot.listeners.admin;

import com.wimdupont.sxcybot.listeners.Listener;
import com.wimdupont.sxcybot.listeners.admin.pvmrole.AddPvmListener;
import com.wimdupont.sxcybot.listeners.admin.pvmrole.DeletePvmListener;
import com.wimdupont.sxcybot.listeners.admin.pvmrole.UpdatePvmListener;
import com.wimdupont.sxcybot.model.EditListenerDto;
import com.wimdupont.sxcybot.util.EditListenerUtil;
import net.dv8tion.jda.api.entities.channel.concrete.PrivateChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.exceptions.RateLimitedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class EditPvmListener implements Listener {

    private static final Logger LOGGER = LoggerFactory.getLogger(EditPvmListener.class);
    private final EditListenerUtil editListenerUtil;
    private final AddPvmListener addPvmListener;
    private final UpdatePvmListener updatePvmListener;
    private final DeletePvmListener deletePvmListener;

    public EditPvmListener(EditListenerUtil editListenerUtil,
                           AddPvmListener addPvmListener,
                           UpdatePvmListener updatePvmListener,
                           DeletePvmListener deletePvmListener) {
        this.editListenerUtil = editListenerUtil;
        this.addPvmListener = addPvmListener;
        this.updatePvmListener = updatePvmListener;
        this.deletePvmListener = deletePvmListener;
    }

    @Override
    public void process(MessageReceivedEvent event) {
        if (event.getMember() != null) {
            try {
                PrivateChannel privateChannel = event.getMember().getUser().openPrivateChannel().complete(true);
                EditListenerDto editListenerDto = EditListenerDto.Builder.newBuilder()
                        .addListener(addPvmListener)
                        .updateListener(updatePvmListener)
                        .deleteListener(deletePvmListener)
                        .event(event)
                        .privateChannel(privateChannel)
                        .entityName("PvM competitor")
                        .build();
                editListenerUtil.procesEditEvent(editListenerDto);
            } catch (RateLimitedException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
    }
}
