package com.wimdupont.sxcybot.listeners.admin.role;

import com.wimdupont.sxcybot.listeners.EventWaiter;
import com.wimdupont.sxcybot.listeners.PrivateListener;
import com.wimdupont.sxcybot.repository.guild.dao.GuildRole;
import com.wimdupont.sxcybot.services.guild.GuildRoleService;
import com.wimdupont.sxcybot.util.Constants;
import com.wimdupont.sxcybot.util.JdaUtil;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;

import java.util.Comparator;

@Component
public class AddRoleListener implements PrivateListener {

    private final GuildRoleService guildRoleService;
    private final EventWaiter eventWaiter;

    public AddRoleListener(GuildRoleService guildRoleService,
                           EventWaiter eventWaiter) {
        this.guildRoleService = guildRoleService;
        this.eventWaiter = eventWaiter;
    }

    @Override
    public void process(MessageReceivedEvent privateMessageReceivedEvent, MessageReceivedEvent event) {
        var privateChannel = privateMessageReceivedEvent.getChannel().asPrivateChannel();
        privateChannel.sendMessage("Name of the role you want to add?").queue();
        eventWaiter.waitForPrivateChannelEvent(nameReceiver -> {
            var roleName = nameReceiver.getMessage().getContentRaw();
            var role = event.getGuild().getRoles().stream()
                    .filter(f -> roleName.equalsIgnoreCase(f.getName()))
                    .findFirst();
            if (role.isPresent()) {
                var orderValue = guildRoleService.findAll().stream()
                        .filter(f -> f.getOrderValue() != null)
                        .max(Comparator.comparing(GuildRole::getOrderValue))
                        .map(GuildRole::getOrderValue)
                        .orElse(0) + 1;
                var roleToAdd = GuildRole.Builder.newBuilder()
                        .discordId(role.get().getId())
                        .orderValue(orderValue)
                        .elevation(Constants.ADDED_ROLE_ELEVATION)
                        .build();
                guildRoleService.save(roleToAdd);
                privateChannel.sendMessage(String.format("Role %s has been successfully added.",
                                roleName))
                        .queue();
                event.getChannel().sendMessage(String.format("Role %s has been added by %s.",
                                roleName,
                                JdaUtil.getUser(event)))
                        .queue();
            } else {
                privateChannel.sendMessage(String.format("Role with name %s is not available in the channel, " +
                                        "please contact a channel owner to have it added.",
                                roleName))
                        .queue();
            }
        }, privateMessageReceivedEvent, privateChannel);

    }
}
