package com.wimdupont.sxcybot.listeners.admin.pvmrole;

import com.wimdupont.sxcybot.exceptions.EntityNotFoundException;
import com.wimdupont.sxcybot.listeners.EventWaiter;
import com.wimdupont.sxcybot.listeners.PrivateListener;
import com.wimdupont.sxcybot.repository.guild.pvmrole.dao.PvmRoleUser;
import com.wimdupont.sxcybot.services.guild.pvmrole.PvmRoleSnapshotComparatorService;
import com.wimdupont.sxcybot.services.guild.pvmrole.PvmRoleUserService;
import com.wimdupont.sxcybot.util.DiscordMemberFinderUtil;
import com.wimdupont.sxcybot.util.JdaUtil;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.channel.concrete.PrivateChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import java.awt.Color;

@Component
public class AddPvmListener implements PrivateListener {

    private final EventWaiter eventWaiter;
    private final DiscordMemberFinderUtil discordMemberFinderUtil;
    private final PvmRoleUserService pvmRoleUserService;
    private final PvmRoleSnapshotComparatorService pvmRoleSnapshotComparatorService;

    public AddPvmListener(EventWaiter eventWaiter,
                          DiscordMemberFinderUtil discordMemberFinderUtil,
                          PvmRoleUserService pvmRoleUserService,
                          PvmRoleSnapshotComparatorService pvmRoleSnapshotComparatorService) {
        this.eventWaiter = eventWaiter;
        this.discordMemberFinderUtil = discordMemberFinderUtil;
        this.pvmRoleUserService = pvmRoleUserService;
        this.pvmRoleSnapshotComparatorService = pvmRoleSnapshotComparatorService;
    }

    @Override
    public void process(MessageReceivedEvent privateMessageReceivedEvent, MessageReceivedEvent event) {
        PrivateChannel privateChannel = privateMessageReceivedEvent.getChannel().asPrivateChannel();
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setColor(Color.RED);
        embedBuilder.setTitle("Add member for PvM competition");
        discordMemberFinderUtil.onMemberFoundVerification(event, embedBuilder, privateChannel, member -> {
            embedBuilder.clearFields();
            embedBuilder.addField("Type the RSN (RuneScape name) of the member.", "", true);
            privateChannel.sendMessageEmbeds(embedBuilder.build()).queue();
            eventWaiter.waitForPrivateChannelEvent((rsnReceiver) -> {
                String creator = JdaUtil.getName(event).orElse(null);
                try {
                    PvmRoleUser pvmRoleUser = pvmRoleUserService.save(PvmRoleUser.Builder.newBuilder()
                            .rsn(rsnReceiver.getMessage().getContentRaw())
                            .discordId(member.getUser().getId())
                            .createdBy(creator)
                            .lastModifiedBy(creator)
                            .build()
                    );
                    event.getChannel().sendMessage(String.format("%s (RSN: %s) has been added to the PvM competition by %s!",
                            member, pvmRoleUser.getRsn(), creator)).queue();
                    privateChannel.sendMessage(String.format("%s (RSN: %s) has been successfully added!",
                            member, pvmRoleUser.getRsn())).queue();
                    try {
                        pvmRoleSnapshotComparatorService.takeSnapshot(event.getChannel(), pvmRoleUser, true, true);
                    } catch (EntityNotFoundException e) {
                        event.getChannel().sendMessage(e.getMessage()).queue();
                    }
                } catch (DataIntegrityViolationException e) {
                    if (e.getRootCause() != null) {
                        privateChannel.sendMessage(e.getRootCause().getMessage()).queue();
                    } else if (e.getMessage() != null)
                        privateChannel.sendMessage(e.getMessage()).queue();
                    privateChannel.sendMessage("Please try updating the existing PvM competitor instead.").queue();
                }
            }, event, privateChannel);
        });
    }
}
