package com.wimdupont.sxcybot.listeners.admin;

import com.wimdupont.sxcybot.listeners.Listener;
import com.wimdupont.sxcybot.listeners.admin.banlist.AddBanlistUserListener;
import com.wimdupont.sxcybot.listeners.admin.banlist.DeleteBanlistUserListener;
import com.wimdupont.sxcybot.listeners.admin.banlist.UpdateBanlistUserListener;
import com.wimdupont.sxcybot.model.EditListenerDto;
import com.wimdupont.sxcybot.util.EditListenerUtil;
import net.dv8tion.jda.api.entities.channel.concrete.PrivateChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.exceptions.RateLimitedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class EditBanlistListener implements Listener {

    private static final Logger LOGGER = LoggerFactory.getLogger(EditBanlistListener.class);
    private final AddBanlistUserListener addBanlistUserListener;
    private final UpdateBanlistUserListener updateBanlistListener;
    private final DeleteBanlistUserListener deleteBanlistListener;
    private final EditListenerUtil editListenerUtil;

    public EditBanlistListener(AddBanlistUserListener addBanlistUserListener,
                               UpdateBanlistUserListener updateBanlistListener,
                               DeleteBanlistUserListener deleteBanlistListener,
                               EditListenerUtil editListenerUtil) {
        this.addBanlistUserListener = addBanlistUserListener;
        this.updateBanlistListener = updateBanlistListener;
        this.deleteBanlistListener = deleteBanlistListener;
        this.editListenerUtil = editListenerUtil;
    }

    @Override
    public void process(MessageReceivedEvent event) {
        if (event.getMember() != null) {
            try {
                PrivateChannel privateChannel = event.getMember().getUser().openPrivateChannel().complete(true);
                EditListenerDto editListenerDto = EditListenerDto.Builder.newBuilder()
                        .addListener(addBanlistUserListener)
                        .updateListener(updateBanlistListener)
                        .deleteListener(deleteBanlistListener)
                        .event(event)
                        .privateChannel(privateChannel)
                        .entityName("user to banlist")
                        .build();
                editListenerUtil.procesEditEvent(editListenerDto);
            } catch (RateLimitedException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
    }
}
