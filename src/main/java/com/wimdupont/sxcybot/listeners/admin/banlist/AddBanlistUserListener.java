package com.wimdupont.sxcybot.listeners.admin.banlist;

import com.wimdupont.sxcybot.listeners.EventWaiter;
import com.wimdupont.sxcybot.listeners.PrivateListener;
import com.wimdupont.sxcybot.repository.guild.dao.User;
import com.wimdupont.sxcybot.services.guild.UserService;
import com.wimdupont.sxcybot.util.JdaUtil;
import net.dv8tion.jda.api.entities.channel.concrete.PrivateChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;

@Component
public class AddBanlistUserListener implements PrivateListener {

    private final EventWaiter eventWaiter;
    private final UserService userService;

    public AddBanlistUserListener(EventWaiter eventWaiter,
                                  UserService userService) {
        this.eventWaiter = eventWaiter;
        this.userService = userService;
    }

    @Override
    public void process(MessageReceivedEvent privateMessageReceivedEvent, MessageReceivedEvent event) {
        PrivateChannel privateChannel = privateMessageReceivedEvent.getChannel().asPrivateChannel();
        privateChannel.sendMessage("Name of the user you want to ban?").queue();
        eventWaiter.waitForPrivateChannelEvent(nameReceiver -> {
            privateChannel.sendMessage("Description?").queue();
            eventWaiter.waitForPrivateChannelEvent(descriptionReceiver -> {
                User userToBan = User.Builder.newBuilder()
                        .name(nameReceiver.getMessage().getContentRaw())
                        .description(descriptionReceiver.getMessage().getContentRaw())
                        .banned(true)
                        .createdBy(JdaUtil.getName(event).orElse(null))
                        .lastModifiedBy(JdaUtil.getName(event).orElse(null))
                        .build();
                User bannedUser = userService.save(userToBan);
                privateChannel.sendMessage(String.format("User %s has been successfully banned.", bannedUser.getName())).queue();
                event.getChannel().sendMessage(String.format("User %s has been banned by %s. (%s)",
                        bannedUser.getName(), JdaUtil.getUser(event), bannedUser.getDescription()
                )).queue();
            }, privateMessageReceivedEvent, privateChannel);
        }, privateMessageReceivedEvent, privateChannel);
    }
}
