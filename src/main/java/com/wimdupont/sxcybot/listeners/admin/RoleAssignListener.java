package com.wimdupont.sxcybot.listeners.admin;

import com.wimdupont.sxcybot.exceptions.InsufficientPrivilegesException;
import com.wimdupont.sxcybot.listeners.EventWaiter;
import com.wimdupont.sxcybot.listeners.Listener;
import com.wimdupont.sxcybot.repository.guild.dao.GuildRole;
import com.wimdupont.sxcybot.services.AccessService;
import com.wimdupont.sxcybot.services.guild.GuildRoleService;
import com.wimdupont.sxcybot.util.Constants;
import com.wimdupont.sxcybot.util.DiscordMemberFinderUtil;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.exceptions.RateLimitedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.awt.Color;
import java.util.Comparator;
import java.util.stream.Stream;

import static com.wimdupont.sxcybot.util.Constants.ROLE_ACCESS.SUB_ADMIN;

@Component
public class RoleAssignListener implements Listener {

    private static final Logger LOGGER = LoggerFactory.getLogger(RoleAssignListener.class);
    private final EventWaiter eventWaiter;
    private final GuildRoleService guildRoleService;
    private final DiscordMemberFinderUtil discordMemberFinderUtil;
    private final AccessService accessService;

    public RoleAssignListener(EventWaiter eventWaiter,
                              GuildRoleService guildRoleService,
                              DiscordMemberFinderUtil discordMemberFinderUtil,
                              AccessService accessService) {
        this.eventWaiter = eventWaiter;
        this.guildRoleService = guildRoleService;
        this.discordMemberFinderUtil = discordMemberFinderUtil;
        this.accessService = accessService;
    }

    @Override
    public void process(MessageReceivedEvent event) {
        if (event.getMember() != null) {
            try {
                var privateChannel = event.getMember().getUser()
                        .openPrivateChannel()
                        .complete(true);
                var embedBuilder = new EmbedBuilder();
                embedBuilder.setColor(Color.RED);
                discordMemberFinderUtil.onMemberFoundVerification(event, embedBuilder, privateChannel, member -> {
                    embedBuilder.clearFields();
                    embedBuilder.setTitle("Type the numbers of the roles to add separated by a whitespace. If the member already has any role, it will get removed instead.");
                    Stream<Role> roleStream = event.getMember().getRoles().stream();
                    var roleElevation = Constants.ADDED_ROLE_ELEVATION;
                    try {
                        accessService.isAllowed(roleStream, event, SUB_ADMIN);
                    } catch (InsufficientPrivilegesException e) {
                        roleElevation = Constants.ADDED_TEACHER_ROLE_ELEVATION;
                    }
                    var rolesToAdd = guildRoleService.findAllByElevationGreaterThanEqual(roleElevation).stream()
                            .sorted(Comparator.nullsLast(Comparator.comparing(GuildRole::getOrderValue)))
                            .toList();
                    for (GuildRole guildRole : rolesToAdd) {
                        embedBuilder.addField(String.valueOf(guildRole.getOrderValue()),
                                event.getGuild().getRoles().stream()
                                        .filter(f -> f.getId().equals(guildRole.getDiscordId()))
                                        .map(Role::getName)
                                        .findFirst()
                                        .orElseThrow(),
                                true);
                    }
                    privateChannel.sendMessageEmbeds(embedBuilder.build()).queue();
                    eventWaiter.waitForPrivateChannelEvent(roleReceiver -> {
                        var roleMessage = roleReceiver.getMessage().getContentRaw();
                        String[] roleNumbers = roleMessage.split(" ");
                        embedBuilder.clearFields();
                        embedBuilder.setTitle("Role assigner:");
                        for (String roleNumber : roleNumbers) {
                            try {
                                var discordRole = rolesToAdd.stream()
                                        .filter(f -> Integer.parseInt(roleNumber) == f.getOrderValue())
                                        .findFirst();
                                if (discordRole.isPresent()) {
                                    var role = event.getGuild().getRoles().stream()
                                            .filter(f -> discordRole.get().getDiscordId().equalsIgnoreCase(f.getId()))
                                            .findFirst();
                                    if (role.isPresent()) {
                                        String action;
                                        if (member.getRoles().contains(role.get())) {
                                            event.getGuild()
                                                    .removeRoleFromMember(member, role.get())
                                                    .queue();
                                            action = "removed";
                                        } else {
                                            event.getGuild()
                                                    .addRoleToMember(member, role.get())
                                                    .queue();
                                            action = "assigned";
                                        }
                                        embedBuilder.addField(String.format("\"%s\" has been successfully %s.",
                                                role.get().getName(),
                                                action), "", false);
                                        event.getChannel().sendMessage(String.format("Role \"%s\" has been %s to %s by %s",
                                                        role.get().getName(),
                                                        action,
                                                        member.getUser(),
                                                        event.getMember().getUser().getName()))
                                                .queue();
                                    } else {
                                        privateChannel.sendMessage("No such role is available in the channel, please contact your channel owner.")
                                                .queue();
                                    }
                                } else {
                                    privateChannel.sendMessage(String.format("%s is not in the suggested list, please try again later.",
                                                    roleNumber))
                                            .queue();
                                }
                            } catch (NumberFormatException e) {
                                privateChannel.sendMessage(String.format("%s is not a valid number, please try again later.",
                                                roleNumber))
                                        .queue();
                            }
                        }
                        if (embedBuilder.getFields().isEmpty()) {
                            embedBuilder.addField("No roles changed.", "", false);
                        }
                        privateChannel.sendMessageEmbeds(embedBuilder.build())
                                .queue();
                    }, event, privateChannel);
                });
            } catch (RateLimitedException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
    }
}
