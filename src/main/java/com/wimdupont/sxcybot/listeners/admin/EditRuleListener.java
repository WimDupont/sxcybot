package com.wimdupont.sxcybot.listeners.admin;

import com.wimdupont.sxcybot.listeners.Listener;
import com.wimdupont.sxcybot.listeners.admin.rule.AddRuleListener;
import com.wimdupont.sxcybot.listeners.admin.rule.DeleteRuleListener;
import com.wimdupont.sxcybot.listeners.admin.rule.UpdateRuleListener;
import com.wimdupont.sxcybot.model.EditListenerDto;
import com.wimdupont.sxcybot.util.EditListenerUtil;
import net.dv8tion.jda.api.entities.channel.concrete.PrivateChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.exceptions.RateLimitedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class EditRuleListener implements Listener {

    private static final Logger LOGGER = LoggerFactory.getLogger(EditRuleListener.class);
    private final AddRuleListener addRuleListener;
    private final UpdateRuleListener updateRuleListener;
    private final DeleteRuleListener deleteRuleListener;
    private final EditListenerUtil editListenerUtil;

    public EditRuleListener(AddRuleListener addRuleListener,
                            UpdateRuleListener updateRuleListener,
                            DeleteRuleListener deleteRuleListener,
                            EditListenerUtil editListenerUtil) {
        this.addRuleListener = addRuleListener;
        this.updateRuleListener = updateRuleListener;
        this.deleteRuleListener = deleteRuleListener;
        this.editListenerUtil = editListenerUtil;
    }

    @Override
    public void process(MessageReceivedEvent event) {
        if (event.getMember() != null) {
            try {
                PrivateChannel privateChannel = event.getMember().getUser().openPrivateChannel().complete(true);
                EditListenerDto editListenerDto = EditListenerDto.Builder.newBuilder()
                        .addListener(addRuleListener)
                        .updateListener(updateRuleListener)
                        .deleteListener(deleteRuleListener)
                        .event(event)
                        .privateChannel(privateChannel)
                        .entityName("rule")
                        .build();
                editListenerUtil.procesEditEvent(editListenerDto);
            } catch (RateLimitedException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
    }
}
