package com.wimdupont.sxcybot.listeners.admin.pvmrole;

import com.wimdupont.sxcybot.enums.Command;
import com.wimdupont.sxcybot.exceptions.EntityNotFoundException;
import com.wimdupont.sxcybot.listeners.EventWaiter;
import com.wimdupont.sxcybot.listeners.PrivateListener;
import com.wimdupont.sxcybot.repository.guild.pvmrole.dao.PvmRoleUser;
import com.wimdupont.sxcybot.services.guild.pvmrole.PvmKcSnapshotService;
import com.wimdupont.sxcybot.services.guild.pvmrole.PvmRoleUserService;
import com.wimdupont.sxcybot.util.Constants.Commands;
import com.wimdupont.sxcybot.util.JdaUtil;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.entities.channel.concrete.PrivateChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;

import java.awt.Color;

@Component
public class DeletePvmListener implements PrivateListener {

    private final PvmRoleUserService pvmRoleUserService;
    private final PvmKcSnapshotService pvmKcSnapshotService;
    private final EventWaiter eventWaiter;

    public DeletePvmListener(PvmRoleUserService pvmRoleUserService,
                             PvmKcSnapshotService pvmKcSnapshotService,
                             EventWaiter eventWaiter) {
        this.pvmRoleUserService = pvmRoleUserService;
        this.pvmKcSnapshotService = pvmKcSnapshotService;
        this.eventWaiter = eventWaiter;
    }

    @Override
    public void process(MessageReceivedEvent privateMessageReceivedEvent, MessageReceivedEvent event) {
        PrivateChannel privateChannel = privateMessageReceivedEvent.getChannel().asPrivateChannel();
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setColor(Color.RED);
        embedBuilder.setTitle("Delete user from PvM competition.");
        embedBuilder.addField("Type the discord name or RSN of the user you want to delete.", "", false);
        privateChannel.sendMessageEmbeds(embedBuilder.build()).queue();
        embedBuilder.clearFields();
        eventWaiter.waitForPrivateChannelEvent(discordNameReceiver -> {
            String name = discordNameReceiver.getMessage().getContentRaw();
            event.getGuild().loadMembers().onSuccess(members -> {
                try {
                    var userId = JdaUtil.getUserByName(members, name)
                            .map(User::getId);
                    PvmRoleUser pvmRoleUser = userId.isPresent()
                            ? pvmRoleUserService.findByDiscordId(userId.get())
                            : pvmRoleUserService.findByRsn(name);
                    embedBuilder.addField("Confirm by typing ``yes or y``.",
                            String.format("Is " + System.lineSeparator() + "Discord: %s | RSN: %s " + System.lineSeparator() + "the correct member?"
                                    , JdaUtil.getMemberById(members, pvmRoleUser.getDiscordId()).orElse(null), pvmRoleUser.getRsn()), false);
                    privateChannel.sendMessageEmbeds(embedBuilder.build()).queue();
                    embedBuilder.clearFields();
                    eventWaiter.waitForPrivateChannelEvent(memberVerifyReceiver -> {
                        String verifyMessage = memberVerifyReceiver.getMessage().getContentRaw();
                        if ("y".equalsIgnoreCase(verifyMessage) || "yes".equalsIgnoreCase(verifyMessage)) {
                            pvmKcSnapshotService.deleteAll(pvmKcSnapshotService.findAllByPvmRoleUser(pvmRoleUser));
                            pvmRoleUserService.delete(pvmRoleUser);
                            privateChannel.sendMessage(String.format("%s has been successfully deleted.", pvmRoleUser)).queue();
                            event.getChannel().sendMessage(String.format("PvM competitor %s has been deleted by %s", name, JdaUtil.getUser(event))).queue();
                        } else {
                            privateChannel.sendMessage(String.format("You can check all competitors with the \"%s\" command.", Commands.COMMAND_PREFIX + Command.PVMLIST.name().toLowerCase())).queue();
                        }
                    }, privateMessageReceivedEvent, privateChannel);
                } catch (EntityNotFoundException e) {
                    privateChannel.sendMessage(e.getMessage()).queue();
                }
            });
        }, privateMessageReceivedEvent, privateChannel);
    }
}
