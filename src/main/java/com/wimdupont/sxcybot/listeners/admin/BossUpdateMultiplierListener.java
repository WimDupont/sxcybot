package com.wimdupont.sxcybot.listeners.admin;

import com.wimdupont.sxcybot.exceptions.EntityNotFoundException;
import com.wimdupont.sxcybot.listeners.EventWaiter;
import com.wimdupont.sxcybot.services.osrs.OsrsHiscoreBossService;
import com.wimdupont.sxcybot.util.JdaUtil;
import net.dv8tion.jda.api.entities.channel.concrete.PrivateChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.exceptions.RateLimitedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.EventListener;

import static com.wimdupont.sxcybot.enums.Command.BOSSLIST;
import static com.wimdupont.sxcybot.util.Constants.Commands.COMMAND_PREFIX;

@Component
public class BossUpdateMultiplierListener implements EventListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(BossUpdateMultiplierListener.class);

    private final EventWaiter eventWaiter;
    private final OsrsHiscoreBossService osrsHiscoreBossService;

    public BossUpdateMultiplierListener(EventWaiter eventWaiter,
                                        OsrsHiscoreBossService osrsHiscoreBossService) {
        this.eventWaiter = eventWaiter;
        this.osrsHiscoreBossService = osrsHiscoreBossService;
    }

    public void process(MessageReceivedEvent event) {
        if (event.getMember() != null) {
            try {
                PrivateChannel privateChannel = event.getMember().getUser().openPrivateChannel().complete(true);
                privateChannel.sendMessage("Name of the boss you want to update?").queue();
                eventWaiter.waitForPrivateChannelEvent(nameReceiver -> {
                    try {
                        var osrsHiscoreBoss = osrsHiscoreBossService.findByName(nameReceiver.getMessage().getContentRaw());
                        var oldMultiplier = osrsHiscoreBoss.getMultiplier();
                        privateChannel.sendMessage(String.format("Type the multiplier for the boss of %s (current = %s).",
                                nameReceiver.getMessage().getContentRaw(), oldMultiplier)).queue();
                        eventWaiter.waitForPrivateChannelEvent(descriptionReceiver -> {
                            try {
                                osrsHiscoreBoss.setMultiplier(Double.parseDouble(descriptionReceiver.getMessage().getContentRaw()));
                                var saved = osrsHiscoreBossService.save(osrsHiscoreBoss);
                                privateChannel.sendMessage(saved + " edited.").queue();
                                event.getChannel().sendMessage(String.format("Boss multiplier for boss %s has been changed from %s to %s by %s.",
                                                saved.getName(), oldMultiplier, saved.getMultiplier(), JdaUtil.getUser(event)))
                                        .queue();
                            } catch (NumberFormatException e) {
                                privateChannel.sendMessage("Please enter a valid multiplier number.").queue();
                            }
                        }, nameReceiver, privateChannel);
                    } catch (EntityNotFoundException e) {
                        privateChannel.sendMessage(e.getMessage()).queue();
                        privateChannel.sendMessage(String.format("use %s%s command in the guild channel to see all bosses.",
                                COMMAND_PREFIX, BOSSLIST.name().toLowerCase())).queue();
                    }
                }, event, privateChannel);
            } catch (RateLimitedException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
    }
}
