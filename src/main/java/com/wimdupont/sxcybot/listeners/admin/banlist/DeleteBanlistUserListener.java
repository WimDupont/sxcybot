package com.wimdupont.sxcybot.listeners.admin.banlist;

import com.wimdupont.sxcybot.exceptions.EntityNotFoundException;
import com.wimdupont.sxcybot.listeners.EventWaiter;
import com.wimdupont.sxcybot.listeners.PrivateListener;
import com.wimdupont.sxcybot.repository.guild.dao.User;
import com.wimdupont.sxcybot.services.guild.UserService;
import com.wimdupont.sxcybot.util.JdaUtil;
import net.dv8tion.jda.api.entities.channel.concrete.PrivateChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;

@Component
public class DeleteBanlistUserListener implements PrivateListener {

    private final UserService userService;
    private final EventWaiter eventWaiter;

    public DeleteBanlistUserListener(UserService userService,
                                     EventWaiter eventWaiter) {
        this.userService = userService;
        this.eventWaiter = eventWaiter;
    }

    @Override
    public void process(MessageReceivedEvent privateEvent, MessageReceivedEvent event) {
        PrivateChannel privateChannel = privateEvent.getChannel().asPrivateChannel();
        privateChannel.sendMessage("Name of the user you want to delete?").queue();
        eventWaiter.waitForPrivateChannelEvent(nameReceiver -> {
            try {
                User userToDelete = userService.findByName(nameReceiver.getMessage().getContentRaw());
                userService.delete(userToDelete);
                privateChannel.sendMessage(String.format("%s successfully deleted.", userToDelete)).queue();
                event.getChannel().sendMessage(String.format("User %s has been deleted from the banlist by %s", userToDelete, JdaUtil.getUser(event))).queue();
            } catch (EntityNotFoundException e) {
                privateChannel.sendMessage(e.getMessage()).queue();
            }
        }, event, privateChannel);
    }
}
