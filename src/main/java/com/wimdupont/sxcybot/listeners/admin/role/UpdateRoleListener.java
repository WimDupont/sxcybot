package com.wimdupont.sxcybot.listeners.admin.role;

import com.wimdupont.sxcybot.exceptions.EntityNotFoundException;
import com.wimdupont.sxcybot.listeners.EventWaiter;
import com.wimdupont.sxcybot.listeners.PrivateListener;
import com.wimdupont.sxcybot.repository.guild.dao.GuildRole;
import com.wimdupont.sxcybot.services.guild.GuildRoleService;
import com.wimdupont.sxcybot.util.Constants;
import com.wimdupont.sxcybot.util.Constants.Commands;
import com.wimdupont.sxcybot.util.JdaUtil;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;

@Component
public class UpdateRoleListener implements PrivateListener {

    private final GuildRoleService guildRoleService;
    private final EventWaiter eventWaiter;

    public UpdateRoleListener(GuildRoleService guildRoleService,
                              EventWaiter eventWaiter) {
        this.guildRoleService = guildRoleService;
        this.eventWaiter = eventWaiter;
    }

    @Override
    public void process(MessageReceivedEvent privateMessageReceivedEvent, MessageReceivedEvent event) {
        var privateChannel = privateMessageReceivedEvent.getChannel().asPrivateChannel();
        privateChannel.sendMessage("Type the name of the role you want to update.").queue();
        eventWaiter.waitForPrivateChannelEvent(oldRoleNameReceiver -> {
            var roleName = oldRoleNameReceiver.getMessage().getContentRaw();
            privateChannel.sendMessage("Type the new name for the role.").queue();
            eventWaiter.waitForPrivateChannelEvent(newRoleNameReceiver -> {
                try {
                    var newRoleName = newRoleNameReceiver.getMessage().getContentRaw();
                    GuildRole roleToUpdate = guildRoleService.findByName(
                            event.getGuild().getRoles(),
                            roleName);
                    if (roleToUpdate.getElevation() == Constants.ADDED_ROLE_ELEVATION) {
                        var discordId = guildRoleService.discordIdByName(
                                event.getGuild().getRoles(),
                                newRoleName);
                        if (discordId.isPresent()) {
                            roleToUpdate.setDiscordId(discordId.get());
                            GuildRole guildRole = guildRoleService.save(roleToUpdate);
                            newRoleNameReceiver.getChannel().sendMessage(guildRole + " edited").queue();
                            event.getChannel()
                                    .sendMessage(String.format("Role %s has been changed to %s by %s",
                                            roleName,
                                            newRoleNameReceiver.getMessage().getContentRaw(),
                                            JdaUtil.getUser(event)))
                                    .queue();
                        } else {
                            privateChannel.sendMessage(String.format("Role with name %s not found.",
                                            newRoleName))
                                    .queue();
                        }
                    } else {
                        privateChannel.sendMessage(String.format("Only roles that can be added with the %srole command can be updated.",
                                        Commands.COMMAND_PREFIX))
                                .queue();
                    }
                } catch (EntityNotFoundException exc) {
                    oldRoleNameReceiver.getChannel().sendMessage(exc.getMessage()).queue();
                }
            }, privateMessageReceivedEvent, privateChannel);
        }, privateMessageReceivedEvent, privateChannel);
    }
}
