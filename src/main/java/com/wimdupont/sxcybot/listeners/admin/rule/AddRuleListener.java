package com.wimdupont.sxcybot.listeners.admin.rule;

import com.wimdupont.sxcybot.listeners.EventWaiter;
import com.wimdupont.sxcybot.listeners.PrivateListener;
import com.wimdupont.sxcybot.repository.guild.dao.Rule;
import com.wimdupont.sxcybot.services.guild.RuleService;
import com.wimdupont.sxcybot.util.JdaUtil;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;

import java.util.Comparator;

@Component
public class AddRuleListener implements PrivateListener {

    private final RuleService ruleService;
    private final EventWaiter eventWaiter;

    public AddRuleListener(RuleService ruleService,
                           EventWaiter eventWaiter) {
        this.ruleService = ruleService;
        this.eventWaiter = eventWaiter;
    }

    @Override
    public void process(MessageReceivedEvent privateMessageReceivedEvent, MessageReceivedEvent event) {
        privateMessageReceivedEvent.getChannel().sendMessage("Type rule description please").queue();
        eventWaiter.waitForPrivateChannelEvent(e -> {
            String msg = e.getMessage().getContentRaw();
            int number = ruleService.findAll().stream().max(Comparator.comparing(Rule::getNumber)).map(Rule::getNumber).orElse(0) + 1;
            Rule rule = ruleService.save(Rule.Builder.newBuilder()
                    .number(number)
                    .description(msg)
                    .build());
            e.getChannel().sendMessage(rule + " saved").queue();
            event.getChannel().sendMessage(String.format("Rule #%s has been added by %s.", number, JdaUtil.getUser(event))).queue();
        }, privateMessageReceivedEvent, privateMessageReceivedEvent.getChannel().asPrivateChannel());
    }
}
