package com.wimdupont.sxcybot.listeners.admin.rule;

import com.wimdupont.sxcybot.exceptions.EntityNotFoundException;
import com.wimdupont.sxcybot.listeners.EventWaiter;
import com.wimdupont.sxcybot.listeners.PrivateListener;
import com.wimdupont.sxcybot.repository.guild.dao.Rule;
import com.wimdupont.sxcybot.services.guild.RuleService;
import com.wimdupont.sxcybot.util.JdaUtil;
import net.dv8tion.jda.api.entities.channel.concrete.PrivateChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;


@Component
public class UpdateRuleListener implements PrivateListener {

    private final RuleService ruleService;
    private final EventWaiter eventWaiter;

    public UpdateRuleListener(RuleService ruleService,
                              EventWaiter eventWaiter) {
        this.ruleService = ruleService;
        this.eventWaiter = eventWaiter;
    }

    @Override
    public void process(MessageReceivedEvent privateMessageReceivedEvent, MessageReceivedEvent event) {
        PrivateChannel privateChannel = privateMessageReceivedEvent.getChannel().asPrivateChannel();
        privateChannel.sendMessage("Type the rule number you want to update.").queue();
        eventWaiter.waitForPrivateChannelEvent(ruleNumberReciever -> {
            try {
                int ruleNumber = Integer.parseInt(ruleNumberReciever.getMessage().getContentRaw());
                privateChannel.sendMessage(String.format("Type the new rule description for rule %s.", ruleNumber)).queue();
                eventWaiter.waitForPrivateChannelEvent(descriptionReceiver -> {
                    try {
                        Rule ruleToUpdate = ruleService.findByNumber(ruleNumber);
                        ruleToUpdate.setDescription(descriptionReceiver.getMessage().getContentRaw());
                        Rule rule = ruleService.save(ruleToUpdate);
                        descriptionReceiver.getChannel().sendMessage(rule + " edited").queue();
                        event.getChannel().sendMessage(String.format("Rule #%s has been edited by %s", ruleNumber, JdaUtil.getUser(event))).queue();
                    } catch (EntityNotFoundException exc) {
                        ruleNumberReciever.getChannel().sendMessage(exc.getMessage()).queue();
                    }
                }, privateMessageReceivedEvent, privateChannel);
            } catch (NumberFormatException exc) {
                ruleNumberReciever.getChannel().sendMessage("No valid rule number.").queue();
            }
        }, privateMessageReceivedEvent, privateChannel);
    }
}
