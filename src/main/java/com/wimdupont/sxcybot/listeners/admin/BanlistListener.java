package com.wimdupont.sxcybot.listeners.admin;

import com.wimdupont.sxcybot.listeners.Listener;
import com.wimdupont.sxcybot.repository.guild.dao.User;
import com.wimdupont.sxcybot.services.guild.UserService;
import com.wimdupont.sxcybot.util.JdaUtil;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.channel.concrete.PrivateChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.exceptions.RateLimitedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.awt.Color;
import java.util.List;


@Component
public class BanlistListener implements Listener {

    private static final Logger LOGGER = LoggerFactory.getLogger(BanlistListener.class);
    private final UserService userService;

    public BanlistListener(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void process(MessageReceivedEvent event) {
        if (event.getMember() != null) {
            try {
                PrivateChannel privateChannel = event.getMember().getUser().openPrivateChannel().complete(true);
                EmbedBuilder embedBuilder = new EmbedBuilder();
                embedBuilder.setColor(Color.red);
                embedBuilder.setTitle("List of banned users:");
                List<User> bannedList = userService.findAllBanned();
                int i = 1;
                for (User user : bannedList) {
                    embedBuilder.addField(user.getName(),
                            String.format("%s", user.getDescription()) + System.lineSeparator() +
                                    String.format("||_By: %s - %s | Last edit: %s - %s_||",
                                            user.getCreatedBy(), user.getCreatedDate(), user.getLastModifiedBy(), user.getLastModifiedDate()),
                            false);

                    if (JdaUtil.requiresBuild(embedBuilder, bannedList.size(), i)) {
                        privateChannel.sendMessageEmbeds(embedBuilder.build()).queue();
                        embedBuilder.clear();
                    }
                    i++;
                }
            } catch (RateLimitedException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
    }
}
