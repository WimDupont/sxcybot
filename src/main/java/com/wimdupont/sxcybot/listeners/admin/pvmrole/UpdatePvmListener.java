package com.wimdupont.sxcybot.listeners.admin.pvmrole;

import com.wimdupont.sxcybot.enums.Command;
import com.wimdupont.sxcybot.exceptions.EntityNotFoundException;
import com.wimdupont.sxcybot.listeners.EventWaiter;
import com.wimdupont.sxcybot.listeners.PrivateListener;
import com.wimdupont.sxcybot.repository.guild.pvmrole.dao.PvmRoleUser;
import com.wimdupont.sxcybot.services.guild.pvmrole.PvmRoleUserService;
import com.wimdupont.sxcybot.util.Constants.Commands;
import com.wimdupont.sxcybot.util.JdaUtil;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.entities.channel.concrete.PrivateChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import java.awt.Color;

@Component
public class UpdatePvmListener implements PrivateListener {

    private final PvmRoleUserService pvmRoleUserService;
    private final EventWaiter eventWaiter;

    public UpdatePvmListener(PvmRoleUserService pvmRoleUserService,
                             EventWaiter eventWaiter) {
        this.pvmRoleUserService = pvmRoleUserService;
        this.eventWaiter = eventWaiter;
    }

    @Override
    public void process(MessageReceivedEvent privateMessageReceivedEvent, MessageReceivedEvent event) {
        PrivateChannel privateChannel = privateMessageReceivedEvent.getChannel().asPrivateChannel();
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setColor(Color.RED);
        embedBuilder.setTitle("Update user for PvM competition.");
        embedBuilder.addField("Type the discord name of the user you want to update.", "", false);
        privateChannel.sendMessageEmbeds(embedBuilder.build()).queue();
        embedBuilder.clearFields();
        eventWaiter.waitForPrivateChannelEvent(discordNameReceiver -> {
            String discordName = discordNameReceiver.getMessage().getContentRaw();
            event.getGuild().loadMembers().onSuccess(members -> {
                try {
                    var userId = JdaUtil.getUserByName(members, discordName)
                            .map(User::getId)
                            .orElseThrow(() -> new EntityNotFoundException(String.format("No discord user found with name: %s", discordName)));
                    PvmRoleUser pvmRoleUserToUpdate = pvmRoleUserService.findByDiscordId(userId);
                    embedBuilder.addField("Confirm by typing ``yes or y``.",
                            String.format("Is " + System.lineSeparator() + "Discord: %s | RSN: %s " + System.lineSeparator() + "the correct member?"
                                    , JdaUtil.getUserByName(members, discordName).orElse(null), pvmRoleUserToUpdate.getRsn()), false);
                    privateChannel.sendMessageEmbeds(embedBuilder.build()).queue();
                    embedBuilder.clearFields();
                    eventWaiter.waitForPrivateChannelEvent(memberVerifyReceiver -> {
                        String verifyMessage = memberVerifyReceiver.getMessage().getContentRaw();
                        if ("y".equalsIgnoreCase(verifyMessage) || "yes".equalsIgnoreCase(verifyMessage)) {
                            embedBuilder.addField("Type the new RSN for the update.", "", false);
                            privateChannel.sendMessageEmbeds(embedBuilder.build()).queue();
                            embedBuilder.clearFields();
                            eventWaiter.waitForPrivateChannelEvent(newRsnReceiver -> {
                                String newRsn = newRsnReceiver.getMessage().getContentRaw();
                                pvmRoleUserToUpdate.setRsn(newRsn);
                                pvmRoleUserToUpdate.setLastModifiedBy(JdaUtil.getName(event).orElse(null));
                                try {
                                    PvmRoleUser savedPvmRoleUser = pvmRoleUserService.save(pvmRoleUserToUpdate);
                                    newRsnReceiver.getChannel().sendMessage(savedPvmRoleUser + " edited").queue();
                                    event.getChannel().sendMessage(String.format("RSN for PvM competitor %s has been changed to %s by %s",
                                            discordName, newRsn, JdaUtil.getUser(event))).queue();
                                } catch (DataIntegrityViolationException e) {
                                    if (e.getMessage() != null)
                                        privateChannel.sendMessage(e.getMessage()).queue();
                                }
                            }, privateMessageReceivedEvent, privateChannel);
                        } else {
                            privateChannel.sendMessage(String.format("You can check all competitors with the \"%s\" command.", Commands.COMMAND_PREFIX + Command.PVMLIST.name().toLowerCase())).queue();
                        }
                    }, privateMessageReceivedEvent, privateChannel);
                } catch (EntityNotFoundException e) {
                    privateChannel.sendMessage(e.getMessage()).queue();
                }
            });
        }, privateMessageReceivedEvent, privateChannel);
    }
}
