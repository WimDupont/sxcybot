package com.wimdupont.sxcybot.listeners.admin.role;

import com.wimdupont.sxcybot.exceptions.EntityNotFoundException;
import com.wimdupont.sxcybot.listeners.EventWaiter;
import com.wimdupont.sxcybot.listeners.PrivateListener;
import com.wimdupont.sxcybot.repository.guild.dao.GuildRole;
import com.wimdupont.sxcybot.services.guild.GuildRoleService;
import com.wimdupont.sxcybot.util.Constants;
import com.wimdupont.sxcybot.util.Constants.Commands;
import com.wimdupont.sxcybot.util.JdaUtil;
import net.dv8tion.jda.api.entities.channel.concrete.PrivateChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;

@Component
public class DeleteRoleListener implements PrivateListener {

    private final GuildRoleService guildRoleService;
    private final EventWaiter eventWaiter;

    public DeleteRoleListener(GuildRoleService guildRoleService,
                              EventWaiter eventWaiter) {
        this.guildRoleService = guildRoleService;
        this.eventWaiter = eventWaiter;
    }

    @Override
    public void process(MessageReceivedEvent privateMessageReceivedEvent, MessageReceivedEvent event) {
        PrivateChannel privateChannel = privateMessageReceivedEvent.getChannel().asPrivateChannel();
        privateChannel.sendMessage("Name of the role you want to delete?").queue();
        eventWaiter.waitForPrivateChannelEvent(nameReceiver -> {
            try {
                var guildRole = guildRoleService.findByName(
                        event.getGuild().getRoles(),
                        nameReceiver.getMessage().getContentRaw());
                if (guildRole.getElevation() == Constants.ADDED_ROLE_ELEVATION) {
                    guildRoleService.delete(guildRole);
                    guildRoleService.findAll().stream()
                            .filter(f -> f.getOrderValue() != null)
                            .filter(f -> f.getOrderValue() > guildRole.getOrderValue())
                            .forEach(role -> {
                                role.setOrderValue(role.getOrderValue() - 1);
                                guildRoleService.save(role);
                            });
                    privateChannel.sendMessage(String.format("%s successfully deleted.", guildRole))
                            .queue();
                    event.getChannel()
                            .sendMessage(String.format("Role %s has been deleted by %s",
                                    guildRole,
                                    JdaUtil.getUser(event)))
                            .queue();
                } else {
                    privateChannel.sendMessage(String.format("Only roles that can be added with the %srole command can be deleted.",
                                    Commands.COMMAND_PREFIX))
                            .queue();
                }
            } catch (EntityNotFoundException e) {
                privateChannel.sendMessage(e.getMessage()).queue();
            }
        }, event, privateChannel);
    }
}
