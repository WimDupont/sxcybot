package com.wimdupont.sxcybot.listeners.admin.rule;

import com.wimdupont.sxcybot.exceptions.EntityNotFoundException;
import com.wimdupont.sxcybot.listeners.EventWaiter;
import com.wimdupont.sxcybot.listeners.PrivateListener;
import com.wimdupont.sxcybot.repository.guild.dao.Rule;
import com.wimdupont.sxcybot.services.guild.RuleService;
import com.wimdupont.sxcybot.util.JdaUtil;
import net.dv8tion.jda.api.entities.channel.concrete.PrivateChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;


@Component
public class DeleteRuleListener implements PrivateListener {

    private final RuleService ruleService;
    private final EventWaiter eventWaiter;

    public DeleteRuleListener(RuleService ruleService,
                              EventWaiter eventWaiter) {
        this.ruleService = ruleService;
        this.eventWaiter = eventWaiter;
    }

    @Override
    public void process(MessageReceivedEvent privateMessageReceivedEvent, MessageReceivedEvent event) {
        PrivateChannel privateChannel = privateMessageReceivedEvent.getChannel().asPrivateChannel();
        privateChannel.sendMessage("Type the # of the rule you wish to delete please.").queue();
        eventWaiter.waitForPrivateChannelEvent(ruleDeleltionReceiver -> {
            try {
                int ruleNumber = Integer.parseInt(ruleDeleltionReceiver.getMessage().getContentRaw());
                Rule rule = ruleService.findByNumber(ruleNumber);
                ruleService.delete(rule);
                ruleService.findAll().stream().filter(f -> f.getNumber() > ruleNumber).toList()
                        .forEach(ruleToUpdate -> {
                            ruleToUpdate.setNumber(ruleToUpdate.getNumber() - 1);
                            ruleService.save(ruleToUpdate);
                        });
                privateChannel.sendMessage(String.format("%s has been successfully deleted.", rule)).queue();
                event.getChannel().sendMessage(String.format("Rule %s has been deleted by %s", rule, JdaUtil.getUser(event))).queue();
            } catch (NumberFormatException e) {
                privateChannel.sendMessage("Please try again later with a valid rule number.").queue();
            } catch (EntityNotFoundException e) {
                privateChannel.sendMessage(e.getMessage()).queue();
            }
        }, privateMessageReceivedEvent, privateChannel);

    }
}
