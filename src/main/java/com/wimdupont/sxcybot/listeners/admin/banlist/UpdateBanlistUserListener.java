package com.wimdupont.sxcybot.listeners.admin.banlist;

import com.wimdupont.sxcybot.exceptions.EntityNotFoundException;
import com.wimdupont.sxcybot.listeners.EventWaiter;
import com.wimdupont.sxcybot.listeners.PrivateListener;
import com.wimdupont.sxcybot.repository.guild.dao.User;
import com.wimdupont.sxcybot.services.guild.UserService;
import com.wimdupont.sxcybot.util.JdaUtil;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.channel.concrete.PrivateChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;

import java.awt.Color;

@Component
public class UpdateBanlistUserListener implements PrivateListener {

    private final EventWaiter eventWaiter;
    private final UserService userService;

    public UpdateBanlistUserListener(EventWaiter eventWaiter,
                                     UserService userService) {
        this.eventWaiter = eventWaiter;
        this.userService = userService;
    }

    @Override
    public void process(MessageReceivedEvent privateMessageReceivedEvent, MessageReceivedEvent event) {
        PrivateChannel privateChannel = privateMessageReceivedEvent.getChannel().asPrivateChannel();
        privateChannel.sendMessage("Name of the user you want to update (\"showlist\" to show all names in banlist).").queue();
        eventWaiter.waitForPrivateChannelEvent(nameReceiver -> {
            if (nameReceiver.getMessage().getContentRaw().equalsIgnoreCase("showlist")) {
                EmbedBuilder embedBuilder = new EmbedBuilder();
                embedBuilder.setColor(Color.red);
                embedBuilder.setTitle("Banned users:");
                userService.findAllBanned().stream().map(User::getName).toList().forEach(f ->
                        embedBuilder.addField(f, "", false));
                privateChannel.sendMessageEmbeds(embedBuilder.build()).queue();
                this.process(privateMessageReceivedEvent, event);
                return;
            }
            try {
                User userToUpdate = userService.findByName(nameReceiver.getMessage().getContentRaw());
                privateChannel.sendMessage(String.format("Type the new description for the ban of %s.", nameReceiver.getMessage().getContentRaw())).queue();
                eventWaiter.waitForPrivateChannelEvent(descriptionReceiver -> {
                    userToUpdate.setDescription(descriptionReceiver.getMessage().getContentRaw());
                    userToUpdate.setLastModifiedBy(JdaUtil.getName(event).orElse(null));
                    User saved = userService.save(userToUpdate);
                    privateChannel.sendMessage(saved + " edited.").queue();
                    event.getChannel().sendMessage(String.format("Banned user %s has been edited by %s.", saved.getName(), JdaUtil.getUser(event))).queue();
                }, privateMessageReceivedEvent, privateChannel);
            } catch (EntityNotFoundException e) {
                privateChannel.sendMessage(e.getMessage()).queue();
            }
        }, event, privateChannel);
    }
}
