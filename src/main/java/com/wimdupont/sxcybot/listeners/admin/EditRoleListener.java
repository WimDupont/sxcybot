package com.wimdupont.sxcybot.listeners.admin;

import com.wimdupont.sxcybot.listeners.Listener;
import com.wimdupont.sxcybot.listeners.admin.role.AddRoleListener;
import com.wimdupont.sxcybot.listeners.admin.role.DeleteRoleListener;
import com.wimdupont.sxcybot.model.EditListenerDto;
import com.wimdupont.sxcybot.util.EditListenerUtil;
import net.dv8tion.jda.api.entities.channel.concrete.PrivateChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.exceptions.RateLimitedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class EditRoleListener implements Listener {

    private static final Logger LOGGER = LoggerFactory.getLogger(EditRoleListener.class);
    private final AddRoleListener addRoleListener;
    private final DeleteRoleListener deleteRoleListener;
    private final EditListenerUtil editListenerUtil;

    public EditRoleListener(AddRoleListener addRoleListener,
                            DeleteRoleListener deleteRoleListener,
                            EditListenerUtil editListenerUtil) {
        this.addRoleListener = addRoleListener;
        this.deleteRoleListener = deleteRoleListener;
        this.editListenerUtil = editListenerUtil;
    }

    @Override
    public void process(MessageReceivedEvent event) {
        if (event.getMember() != null) {
            try {
                PrivateChannel privateChannel = event.getMember().getUser().openPrivateChannel().complete(true);
                EditListenerDto editListenerDto = EditListenerDto.Builder.newBuilder()
                        .addListener(addRoleListener)
                        .deleteListener(deleteRoleListener)
                        //TODO why not implemented?
//                            .updateListener(updateRoleListener)
                        .event(event)
                        .privateChannel(privateChannel)
                        .entityName("role")
                        .build();
                editListenerUtil.procesEditEvent(editListenerDto);
            } catch (RateLimitedException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
    }
}
