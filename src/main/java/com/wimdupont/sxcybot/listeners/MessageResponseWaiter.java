package com.wimdupont.sxcybot.listeners;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.channel.ChannelType;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class MessageResponseWaiter extends ListenerAdapter {

    private final JDA jda;
    private final Long authorId;
    private final Consumer<MessageReceivedEvent> action;
    private final ScheduledExecutorService scheduledExecutorService;

    public MessageResponseWaiter(JDA jda, Long authorId, Consumer<MessageReceivedEvent> action) {
        this.scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        this.jda = jda;
        this.authorId = authorId;
        this.action = action;
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        var authorId = event.getAuthor().getIdLong();

        if (event.getChannelType() == ChannelType.PRIVATE
                && this.authorId == authorId) {
            this.delete();
            action.accept(event);
        }
    }

    public void waitForEvent(long timeout, TimeUnit unit, Runnable timeoutAction) {
        jda.addEventListener(this);

        scheduledExecutorService.schedule(() -> {
            timeoutAction.run();
            jda.removeEventListener(this);
        }, timeout, unit);
    }

    public void delete() {
        scheduledExecutorService.shutdownNow();
        jda.removeEventListener(this);
    }

}
