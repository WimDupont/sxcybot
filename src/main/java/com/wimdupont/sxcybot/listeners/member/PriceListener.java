package com.wimdupont.sxcybot.listeners.member;

import com.wimdupont.sxcybot.client.GrandExchangeClient;
import com.wimdupont.sxcybot.exceptions.EntityNotFoundException;
import com.wimdupont.sxcybot.listeners.Listener;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;

import java.awt.Color;

@Component
public class PriceListener implements Listener {

    private final GrandExchangeClient grandExchangeClient;

    public PriceListener(GrandExchangeClient grandExchangeClient) {
        this.grandExchangeClient = grandExchangeClient;
    }

    @Override
    public void process(MessageReceivedEvent event) {
        EmbedBuilder embedBuilder = new EmbedBuilder();
        String msg = event.getMessage().getContentRaw();
        try {
            String itemName = msg.substring(msg.indexOf(" ")).trim();
            event.getChannel().sendTyping().queue(typing -> {
                try {
                    String result = grandExchangeClient.getPrice(itemName, event.getChannel());
                    embedBuilder.setTitle("Price of item");
                    embedBuilder.setColor(Color.RED);
                    embedBuilder.addField(itemName, result, false);
                    event.getChannel().sendMessageEmbeds(embedBuilder.build()).queue();
                } catch (EntityNotFoundException e) {
                    event.getChannel().sendMessage(e.getMessage()).queue();
                }
            });
        } catch (StringIndexOutOfBoundsException e) {
            event.getChannel().sendMessage("Please enter an item name after the command, separated by space.").queue();
        }

    }
}
