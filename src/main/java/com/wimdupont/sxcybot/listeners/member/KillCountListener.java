package com.wimdupont.sxcybot.listeners.member;

import com.wimdupont.sxcybot.client.HiScoreClient;
import com.wimdupont.sxcybot.exceptions.EntityNotFoundException;
import com.wimdupont.sxcybot.listeners.Listener;
import com.wimdupont.sxcybot.model.OsrsBossKc;
import com.wimdupont.sxcybot.util.JdaUtil;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;

import java.awt.Color;
import java.util.List;

@Component
public class KillCountListener implements Listener {

    private final HiScoreClient hiScoreClient;

    public KillCountListener(HiScoreClient hiScoreClient) {
        this.hiScoreClient = hiScoreClient;
    }

    @Override
    public void process(MessageReceivedEvent event) {
        String msg = event.getMessage().getContentRaw();
        try {
            String player = msg.substring(msg.indexOf(" ")).trim();
            List<OsrsBossKc> bossKcList = hiScoreClient.getHiScoreBossKc(player, event.getChannel());

            event.getChannel().sendTyping().queue(q -> {
                EmbedBuilder embedBuilder = new EmbedBuilder();
                embedBuilder.setTitle(String.format("Killcount for %s", player));
                embedBuilder.setColor(Color.RED);
                int i = 1;
                bossKcList.stream().filter(f -> !"-1".equals(f.kc())).forEach(f -> {
                    embedBuilder.addField(f.name(), String.format("Kc: %s%sRank: %s", f.kc(), System.lineSeparator(), f.rank()), true);
                    if (JdaUtil.requiresBuild(embedBuilder, bossKcList.size(), i)) {
                        event.getChannel().sendMessageEmbeds(embedBuilder.build()).queue();
                        embedBuilder.clearFields();
                    }
                });
                event.getChannel().sendMessageEmbeds(embedBuilder.build()).queue();
            });

        } catch (StringIndexOutOfBoundsException e) {
            event.getChannel().sendMessage("Please enter a player name after the command, separated by space.").queue();
        } catch (EntityNotFoundException e) {
            event.getChannel().sendMessage(e.getMessage()).queue();
        }
    }
}
