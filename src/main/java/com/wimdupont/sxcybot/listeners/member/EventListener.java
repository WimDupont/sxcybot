package com.wimdupont.sxcybot.listeners.member;

import com.wimdupont.sxcybot.listeners.EventWaiter;
import com.wimdupont.sxcybot.listeners.Listener;
import com.wimdupont.sxcybot.repository.guild.dao.Event;
import net.dv8tion.jda.api.entities.channel.concrete.PrivateChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.exceptions.RateLimitedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class EventListener implements Listener {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventListener.class);
    private final EventWaiter eventWaiter;

    public EventListener(EventWaiter eventWaiter) {
        this.eventWaiter = eventWaiter;
    }

    @Override
    public void process(MessageReceivedEvent event) {
        if (!event.getAuthor().isBot() && event.getMember() != null) {
            try {
                PrivateChannel privateChannel = event.getMember().getUser().openPrivateChannel().complete(true);
                privateChannel.sendMessage("Name of the event?").queue();
                eventWaiter.waitForPrivateChannelEvent(nameReceiver -> {
                    privateChannel.sendMessage("Description?").queue();
                    eventWaiter.waitForPrivateChannelEvent(descriptionReceiver -> {
                        privateChannel.sendMessage("TimeZone, UTC or PT?").queue();
                        eventWaiter.waitForPrivateChannelEvent(timeZoneReceiver -> {
                            privateChannel.sendMessage("Date?").queue();
                            eventWaiter.waitForPrivateChannelEvent(dateReceiver ->
                                    finishEventCreation(nameReceiver, descriptionReceiver, timeZoneReceiver, dateReceiver, event), event, privateChannel);
                        }, event, privateChannel);
                    }, event, privateChannel);
                }, event, privateChannel);
            } catch (RateLimitedException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
    }

    private void finishEventCreation(MessageReceivedEvent nameReceiver,
                                     MessageReceivedEvent descriptionReceiver,
                                     MessageReceivedEvent timeZoneReceiver,
                                     MessageReceivedEvent dateReceiver,
                                     MessageReceivedEvent event) {
        //TODO finish or remove
        Event createdEvent = Event.Builder.newBuilder()
                .name(nameReceiver.getMessage().getContentRaw())
                .description(descriptionReceiver.getMessage().getContentRaw())
                .build();
        event.getChannel().sendMessage("Event created :" + createdEvent).queue();
    }
}
