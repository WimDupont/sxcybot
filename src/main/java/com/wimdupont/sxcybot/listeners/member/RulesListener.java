package com.wimdupont.sxcybot.listeners.member;

import com.wimdupont.sxcybot.listeners.Listener;
import com.wimdupont.sxcybot.repository.guild.dao.Rule;
import com.wimdupont.sxcybot.services.guild.RuleService;
import com.wimdupont.sxcybot.util.JdaUtil;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;

import java.awt.Color;
import java.util.Comparator;
import java.util.List;

@Component
public class RulesListener implements Listener {

    private final RuleService ruleService;

    public RulesListener(RuleService ruleService) {
        this.ruleService = ruleService;
    }

    @Override
    public void process(MessageReceivedEvent event) {
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setColor(Color.red);
        embedBuilder.setTitle(String.format("List of %s rules:", event.getGuild().getName()));
        List<Rule> ruleList = ruleService.findAll();
        ruleList.sort(Comparator.comparing(Rule::getNumber));
        int i = 1;
        for (Rule rule : ruleList) {
            embedBuilder.addField(String.format("Rule #%s", rule.getNumber()), rule.getDescription(), false);
            if (JdaUtil.requiresBuild(embedBuilder, ruleList.size(), i)) {
                event.getChannel().sendMessageEmbeds(embedBuilder.build()).queue();
                embedBuilder.clear();
            }
            i++;
        }
    }
}
