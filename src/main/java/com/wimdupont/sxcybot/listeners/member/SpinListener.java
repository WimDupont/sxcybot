package com.wimdupont.sxcybot.listeners.member;

import com.wimdupont.sxcybot.enums.Command;
import com.wimdupont.sxcybot.listeners.Listener;
import com.wimdupont.sxcybot.util.Constants.Commands;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.awt.Color;
import java.util.random.RandomGenerator;

@Component
public class SpinListener implements Listener {

    @Override
    public void process(MessageReceivedEvent event) {
        var channel = event.getChannel();
        try {
            var bounds = getBounds(event.getMessage().getContentRaw());
            var random = String.valueOf(getRandom(bounds));

            EmbedBuilder embedBuilder = new EmbedBuilder();
            embedBuilder.setColor(Color.red);
            embedBuilder.setTitle(String.format("Spinning Result (%s)", bounds));
            embedBuilder.setDescription(random);

            channel.sendMessageEmbeds(embedBuilder.build()).queue();
        } catch (Exception e) {
            channel.sendMessage(e.getMessage()).queue();
        }
    }

    private Pair<Integer, Integer> getBounds(String params) {
        int min = 1, max = 20;
        params = getArgs(params);

        var args = StringUtils.hasLength(params)
                ? params.split("[ -/]")
                : null;

        if (args != null) {
            if (args.length == 1) {
                max = Integer.parseInt(args[0]);
            }
            if (args.length > 1) {
                min = Integer.parseInt(args[0]);
                max = Integer.parseInt(args[1]);
            }
        }
        return Pair.of(min, max);
    }

    private int getRandom(Pair<Integer, Integer> bounds) {
        return RandomGenerator.getDefault().nextInt(bounds.getFirst(), bounds.getSecond() + 1);
    }

    private String getArgs(String message) {
        var commandLength = Commands.COMMAND_PREFIX.length() + Command.SPIN.name().length();
        return message.substring(message.length() > commandLength
                ? commandLength + 1
                : commandLength);
    }

}
