package com.wimdupont.sxcybot.listeners.member;

import com.wimdupont.sxcybot.enums.PvmRole;
import com.wimdupont.sxcybot.listeners.Listener;
import com.wimdupont.sxcybot.repository.osrs.dao.OsrsHiscoreBoss;
import com.wimdupont.sxcybot.services.osrs.OsrsHiscoreBossService;
import com.wimdupont.sxcybot.util.JdaUtil;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;

import java.awt.Color;
import java.util.List;

@Component
public class HiscoreBossListener implements Listener {

    private final OsrsHiscoreBossService osrsHiscoreBossService;

    public HiscoreBossListener(OsrsHiscoreBossService osrsHiscoreBossService) {
        this.osrsHiscoreBossService = osrsHiscoreBossService;
    }

    @Override
    public void process(MessageReceivedEvent event) {
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setColor(Color.red);
        List<OsrsHiscoreBoss> hiscoreBossList = osrsHiscoreBossService.findAll();
        for (PvmRole pvmRole : PvmRole.values()) {
            int i = 1;
            embedBuilder.setTitle(String.format("List of %s bosses:", pvmRole));
            for (OsrsHiscoreBoss osrsHiscoreBoss : hiscoreBossList) {
                if (pvmRole.value == osrsHiscoreBoss.getPvmRole()) {
                    embedBuilder.addField(osrsHiscoreBoss.getName(),
                            String.format("Multiplier: %s",
                                    osrsHiscoreBoss.getMultiplier()),
                            true);
                    if (JdaUtil.requiresBuild(embedBuilder, hiscoreBossList.size(), i)) {
                        event.getChannel().sendMessageEmbeds(embedBuilder.build()).queue();
                        embedBuilder.clearFields();
                    }
                }
                i++;
            }
            if (!embedBuilder.getFields().isEmpty())
                event.getChannel().sendMessageEmbeds(embedBuilder.build()).queue();
            embedBuilder.clearFields();
        }
    }
}
