package com.wimdupont.sxcybot.listeners.member;

import com.wimdupont.sxcybot.enums.Command;
import com.wimdupont.sxcybot.enums.Command.Admin;
import com.wimdupont.sxcybot.listeners.Listener;
import com.wimdupont.sxcybot.util.Constants.Commands;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;

import java.awt.Color;

@Component
public class HelpListener implements Listener {

    @Override
    public void process(MessageReceivedEvent event) {
        event.getChannel().sendTyping().queue();
        EmbedBuilder embedBuilder = new EmbedBuilder();

        embedBuilder.setColor(Color.red);
        embedBuilder.setTitle("List of Commands");
        for (Command command : Command.values()) {
            embedBuilder.addField(Commands.COMMAND_PREFIX + command.name().toLowerCase(), command.description, false);
        }
        EmbedBuilder adminEmbedBuilder = new EmbedBuilder();
        adminEmbedBuilder.setColor(Color.red);
        adminEmbedBuilder.setTitle("List of Admin Commands");
        for (Admin admin : Admin.values()) {
            adminEmbedBuilder.addField(Commands.COMMAND_PREFIX + admin.name().toLowerCase(), admin.description, false);
        }
        event.getChannel().sendMessageEmbeds(embedBuilder.build()).queue();
        event.getChannel().sendMessageEmbeds(adminEmbedBuilder.build()).queue();
    }
}
