package com.wimdupont.sxcybot.listeners.member;

import com.wimdupont.sxcybot.listeners.Listener;
import com.wimdupont.sxcybot.services.OsrsMonitoringService;
import net.dv8tion.jda.api.entities.channel.concrete.PrivateChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.exceptions.RateLimitedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class PvmRoleCheckListener implements Listener {

    private static final Logger LOGGER = LoggerFactory.getLogger(PvmRoleCheckListener.class);
    private final OsrsMonitoringService osrsMonitoringService;

    public PvmRoleCheckListener(OsrsMonitoringService osrsMonitoringService) {
        this.osrsMonitoringService = osrsMonitoringService;
    }

    @Override
    public void process(MessageReceivedEvent event) {
        if (event.getMember() != null) {
            try {
                PrivateChannel privateChannel = event.getMember().getUser().openPrivateChannel().complete(true);
                osrsMonitoringService.monitorPvmRoleUserHiscores(privateChannel);
                osrsMonitoringService.monitorDiscordMembers(privateChannel);
            } catch (RateLimitedException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
    }
}
