package com.wimdupont.sxcybot.listeners.member;

import com.wimdupont.sxcybot.listeners.Listener;
import net.dv8tion.jda.api.entities.channel.middleman.MessageChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;

@Component
public class PingListener implements Listener {

    @Override
    public void process(MessageReceivedEvent event) {
        MessageChannel channel;
        channel = event.getChannel();
        long time = System.currentTimeMillis();
        channel.sendMessage("Pong")
                .queue(response -> response.editMessageFormat("Pong: %d ms", System.currentTimeMillis() - time).queue());
    }
}
