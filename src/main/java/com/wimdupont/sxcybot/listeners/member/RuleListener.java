package com.wimdupont.sxcybot.listeners.member;


import com.wimdupont.sxcybot.enums.Command;
import com.wimdupont.sxcybot.exceptions.EntityNotFoundException;
import com.wimdupont.sxcybot.listeners.Listener;
import com.wimdupont.sxcybot.services.guild.RuleService;
import com.wimdupont.sxcybot.util.Constants.Commands;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.channel.middleman.MessageChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;

import java.awt.Color;


@Component
public class RuleListener implements Listener {

    private final RuleService ruleService;

    public RuleListener(RuleService ruleService) {
        this.ruleService = ruleService;
    }

    @Override
    public void process(MessageReceivedEvent event) {
        MessageChannel channel = event.getChannel();
        int ruleNumber;
        String description;
        try {
            ruleNumber = Integer.parseInt(event.getMessage().getContentRaw().substring(Commands.COMMAND_PREFIX.length() + Command.RULE.name().length() + 1));
            description = ruleService.findByNumber(ruleNumber).getDescription();
        } catch (NumberFormatException | EntityNotFoundException e) {
            channel.sendMessage(e.getMessage()).queue();
            return;
        }
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setColor(Color.RED);
        embedBuilder.setTitle("Show rule result");
        embedBuilder.addField(String.format("Rule #%s", ruleNumber), description, false);
        channel.sendMessageEmbeds(embedBuilder.build()).queue();
    }

}
