package com.wimdupont.sxcybot.listeners.member;

import com.wimdupont.sxcybot.listeners.Listener;
import com.wimdupont.sxcybot.services.osrs.StatMessageSender;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class StatsListener implements Listener {

    private final StatMessageSender statMessageSender;

    public StatsListener(StatMessageSender statMessageSender) {
        this.statMessageSender = statMessageSender;
    }

    @Override
    public void process(MessageReceivedEvent event) {
        EmbedBuilder embedBuilder = new EmbedBuilder();
        event.getChannel().sendTyping().queue(e ->
                statMessageSender.sendStatMessage(event, embedBuilder, Objects::nonNull,
                        f -> embedBuilder.addField(f.name(),
                                "Level " + f.level() + System.lineSeparator() +
                                        "Rank " + f.rank() + System.lineSeparator() +
                                        "Exp " + f.experience(), true), null
                ));
    }
}
