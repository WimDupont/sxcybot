package com.wimdupont.sxcybot.listeners.member;

import com.wimdupont.sxcybot.listeners.Listener;
import com.wimdupont.sxcybot.services.guild.ChannelDetailService;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.channel.middleman.MessageChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;

import java.awt.Color;

@Component
public class ForumListener implements Listener {

    private final ChannelDetailService channelDetailService;

    public ForumListener(ChannelDetailService channelDetailService) {
        this.channelDetailService = channelDetailService;
    }

    @Override
    public void process(MessageReceivedEvent event) {
        channelDetailService.findAll().stream().findAny().ifPresent(channelDetail -> {
            MessageChannel channel = event.getChannel();
            EmbedBuilder embedBuilder = new EmbedBuilder();
            embedBuilder.setColor(Color.RED);
            embedBuilder.setTitle("Link to the forum post", channelDetail.getForumUrl());
            channel.sendMessageEmbeds(embedBuilder.build()).queue();
        });
    }
}
