package com.wimdupont.sxcybot.listeners.member;

import com.wimdupont.sxcybot.listeners.Listener;
import com.wimdupont.sxcybot.repository.guild.pvmrole.dao.PvmRoleUser;
import com.wimdupont.sxcybot.services.guild.pvmrole.PvmRoleUserService;
import com.wimdupont.sxcybot.util.JdaUtil;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;

import java.awt.Color;
import java.util.List;

@Component
public class PvmListListener implements Listener {

    private final PvmRoleUserService pvmRoleUserService;

    public PvmListListener(PvmRoleUserService pvmRoleUserService) {
        this.pvmRoleUserService = pvmRoleUserService;
    }

    @Override
    public void process(MessageReceivedEvent event) {
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setColor(Color.red);
        embedBuilder.setTitle("List of PvM Role competitors:");
        List<PvmRoleUser> pvmRoleUsers = pvmRoleUserService.findAll();
        event.getGuild().loadMembers().onSuccess(members -> {
            int i = 1;
            for (PvmRoleUser pvmRoleUser : pvmRoleUsers) {
                embedBuilder.addField(JdaUtil.getNameById(members, pvmRoleUser.getDiscordId())
                                .orElse("UNKNOWN"),
                        String.format("RSN: %s", pvmRoleUser.getRsn()),
                        true);
                if (JdaUtil.requiresBuild(embedBuilder, pvmRoleUsers.size(), i)) {
                    event.getChannel().sendMessageEmbeds(embedBuilder.build()).queue();
                    embedBuilder.clear();
                }
                i++;
            }
        });
    }
}
