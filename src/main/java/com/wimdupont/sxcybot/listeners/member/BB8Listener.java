package com.wimdupont.sxcybot.listeners.member;

import com.wimdupont.sxcybot.listeners.Listener;
import com.wimdupont.sxcybot.util.Constants.Emoji;
import net.dv8tion.jda.api.entities.channel.middleman.MessageChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.utils.FileUpload;
import org.springframework.stereotype.Component;

import java.io.File;
import java.net.URL;

@Component
public class BB8Listener implements Listener {

    private static final String BB8_IMAGE = "/images/bb8.jpg";

    @Override
    public void process(MessageReceivedEvent event) {
        MessageChannel channel = event.getChannel();
        URL url = getClass().getResource(BB8_IMAGE);

        if (url != null) {
            var fileUpload = FileUpload.fromData(new File(url.getFile()).toPath());
            channel.sendMessage(String.format("BEEP BOOP! %s %s", Emoji.THUMBS_UP, Emoji.FLAMES))
                    .addFiles(fileUpload).queue();
        } else {
            channel.sendMessage("Bzzzt image not found error.").queue();
        }

    }
}
