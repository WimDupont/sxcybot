package com.wimdupont.sxcybot.listeners.member;

import com.wimdupont.sxcybot.listeners.Listener;
import com.wimdupont.sxcybot.model.CombatDto;
import com.wimdupont.sxcybot.model.OsrsStat;
import com.wimdupont.sxcybot.services.osrs.CombatCalculatorService;
import com.wimdupont.sxcybot.services.osrs.StatMessageSender;
import com.wimdupont.sxcybot.enums.OsrsCombatStat;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CombatStatsListener implements Listener {

    private final StatMessageSender statMessageSender;
    private final CombatCalculatorService combatCalculatorService;

    private static final List<String> COMBAT_STAT_LIST = List.of(OsrsCombatStat.ATTACK.value, OsrsCombatStat.STRENGTH.value, OsrsCombatStat.DEFENCE.value, OsrsCombatStat.HITPOINTS.value, OsrsCombatStat.RANGED.value, OsrsCombatStat.PRAYER.value, OsrsCombatStat.MAGIC.value);

    public CombatStatsListener(StatMessageSender statMessageSender,
                               CombatCalculatorService combatCalculatorService) {
        this.statMessageSender = statMessageSender;
        this.combatCalculatorService = combatCalculatorService;
    }

    @Override
    public void process(MessageReceivedEvent event) {
        EmbedBuilder embedBuilder = new EmbedBuilder();
        event.getChannel().sendTyping().queue(e ->
                statMessageSender.sendStatMessage(event, embedBuilder, this::isCombatStat,
                        f -> embedBuilder.addField(f.name(), "Level " + f.level(), true)
                        , osrsStatList -> embedBuilder.addField("COMBAT", String.valueOf(combatCalculatorService.getCombatLevel(CombatDto.build(osrsStatList))), false)
                )
        );
    }

    private boolean isCombatStat(OsrsStat osrsStat) {
        return COMBAT_STAT_LIST.contains(osrsStat.name());
    }

}
