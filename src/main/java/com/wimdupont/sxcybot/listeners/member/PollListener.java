package com.wimdupont.sxcybot.listeners.member;

import com.wimdupont.sxcybot.listeners.Listener;
import com.wimdupont.sxcybot.repository.guild.dao.Poll;
import com.wimdupont.sxcybot.services.guild.PollService;
import com.wimdupont.sxcybot.util.Constants.Reaction;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.emoji.Emoji;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;

import java.awt.Color;

@Component
public class PollListener implements Listener {

    private final PollService pollService;

    public PollListener(PollService pollService) {
        this.pollService = pollService;
    }

    @Override
    public void process(MessageReceivedEvent event) {
        EmbedBuilder embedBuilder = new EmbedBuilder();
        String msg = event.getMessage().getContentRaw();
        try {
            String title = msg.substring(msg.indexOf(" "));

            embedBuilder.setColor(Color.red);
            embedBuilder.setTitle(title);
            embedBuilder.addField("✅ Accept (0)", "", true);
            embedBuilder.addField("❎ Deny (0)", "", true);
            embedBuilder.addField("❓ Unsure (0)", "", true);
            embedBuilder.setFooter("Let the polling begin!", event.getGuild().getIconUrl());

            event.getChannel().sendTyping().queue(typing ->
                    event.getChannel().sendMessageEmbeds(embedBuilder.build()).queue(message -> {
                        message.addReaction(Emoji.fromUnicode(Reaction.CHECK_MARK_BUTTON))
                                .and(message.addReaction(Emoji.fromUnicode(Reaction.CROSS_MARK_BUTTON)))
                                .and(message.addReaction(Emoji.fromUnicode(Reaction.QUESTION_MARK)))
                                .queue();
                        pollService.save(Poll.Builder.newBuilder().messageId(message.getId()).build());
                    }));
        } catch (StringIndexOutOfBoundsException e) {
            event.getChannel().sendMessage("Please enter a descriptive title for the poll after the command, separated by space.").queue();
        }
    }

    //TODO fix & implement or remove
//    private Poll createPoll(String messageId) {
//        List<PollReaction> pollReactionList = newArrayList();
//        Poll poll = Poll.builder()
//                .pollReactions(newArrayList(pollReactionList))
//                .messageId(messageId)
//                .build();
//        PollReaction pollReaction = PollReaction.builder()
//                .reactionId("reactionId")
//                .poll(poll)
//                .build();
//        pollReactionList.add(pollReaction);
//        poll.setPollReactions(pollReactionList);
//
//        return poll;
//
//    }
}

