package com.wimdupont.sxcybot.listeners.member;

import com.wimdupont.sxcybot.listeners.Listener;
import com.wimdupont.sxcybot.services.PvMRoleResolver;
import net.dv8tion.jda.api.entities.channel.concrete.PrivateChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.exceptions.RateLimitedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class PvmRolePollListener implements Listener {

    private static final Logger LOGGER = LoggerFactory.getLogger(PvmRolePollListener.class);
    private final PvMRoleResolver pvMRoleResolver;

    public PvmRolePollListener(PvMRoleResolver pvMRoleResolver) {
        this.pvMRoleResolver = pvMRoleResolver;
    }

    @Override
    public void process(MessageReceivedEvent event) {
        if (event.getMember() != null) {
            try {
                PrivateChannel privateChannel = event.getMember().getUser().openPrivateChannel().complete(true);
                pvMRoleResolver.resolvePvMRoles(false, privateChannel);
            } catch (RateLimitedException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
    }
}
