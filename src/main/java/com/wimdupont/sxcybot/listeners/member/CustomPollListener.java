package com.wimdupont.sxcybot.listeners.member;

import com.wimdupont.sxcybot.listeners.Listener;
import com.wimdupont.sxcybot.util.CustomPollFiller;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.channel.concrete.PrivateChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.exceptions.RateLimitedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.awt.Color;
import java.util.LinkedHashSet;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CustomPollListener implements Listener {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomPollListener.class);
    private final CustomPollFiller customPollFiller;

    public CustomPollListener(CustomPollFiller customPollFiller) {
        this.customPollFiller = customPollFiller;
    }

    @Override
    public void process(MessageReceivedEvent event) {
        String msg = event.getMessage().getContentRaw();
        try {
            String title = msg.substring(msg.indexOf(" "));
            if (event.getMember() != null) {
                try {
                    PrivateChannel privateChannel = event.getMember().getUser().openPrivateChannel().complete(true);
                    EmbedBuilder embedBuilder = new EmbedBuilder();
                    embedBuilder.setColor(Color.red);
                    embedBuilder.setTitle(title);
                    embedBuilder.setFooter("Let the polling begin!", event.getGuild().getIconUrl());

                    customPollFiller.fillPoll(privateChannel, event, embedBuilder, new LinkedHashSet<>());
                } catch (RateLimitedException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        } catch (StringIndexOutOfBoundsException e) {
            event.getChannel().sendMessage("Please enter a descriptive title for the poll after the command, separated by space.").queue();
        }
    }
}
