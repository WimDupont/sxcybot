package com.wimdupont.sxcybot.listeners;

import com.wimdupont.sxcybot.services.guild.GuildEventDmerService;
import jakarta.annotation.Nonnull;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.channel.concrete.PrivateChannel;
import net.dv8tion.jda.api.events.guild.member.GuildMemberRemoveEvent;
import net.dv8tion.jda.api.exceptions.RateLimitedException;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class GuildMemberEventListener extends ListenerAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(GuildMemberEventListener.class);
    private final GuildEventDmerService guildEventDmerService;

    public GuildMemberEventListener(GuildEventDmerService guildEventDmerService) {
        this.guildEventDmerService = guildEventDmerService;
    }

    @Override
    public void onGuildMemberRemove(@Nonnull GuildMemberRemoveEvent event) {
        guildEventDmerService.findAll().forEach(guildEventDmer ->
                event.getGuild().loadMembers().onSuccess(memberList -> {
                    if (!memberList.isEmpty()) {
                        Optional<Member> member = memberList.stream()
                                .filter(f -> guildEventDmer.getDiscordUserId().equalsIgnoreCase(f.getUser().getId()))
                                .findFirst();
                        if (member.isPresent()) {
                            try {
                                PrivateChannel privateChannel = member.get()
                                        .getUser()
                                        .openPrivateChannel()
                                        .complete(true);
                                privateChannel.sendMessage(String.format("**%s** has been removed from channel _%s_",
                                                event.getUser().getName(),
                                                event.getGuild().getName()))
                                        .queue();
                            } catch (RateLimitedException e) {
                                LOGGER.error(e.getMessage(), e);
                            }
                        }
                    }
                })
        );
    }
}

