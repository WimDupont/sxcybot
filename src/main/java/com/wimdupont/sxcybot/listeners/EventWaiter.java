package com.wimdupont.sxcybot.listeners;

import com.wimdupont.sxcybot.services.guild.ChannelDetailService;
import net.dv8tion.jda.api.entities.channel.concrete.PrivateChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

@Component
public class EventWaiter {

    private final ChannelDetailService channelDetailService;
    private static final int TIMEOUT = 1;
    private static final TimeUnit TIME_UNIT = TimeUnit.MINUTES;
    private final Map<Long, MessageResponseWaiter> waitingMessages;

    public EventWaiter(ChannelDetailService channelDetailService) {
        this.channelDetailService = channelDetailService;
        this.waitingMessages = new HashMap<>();
    }

    public void waitForPrivateChannelEvent(Consumer<MessageReceivedEvent> action,
                                           MessageReceivedEvent event,
                                           PrivateChannel privateChannel) {
        var authorId = event.getAuthor().getIdLong();
        var messageResponseWaiter = new MessageResponseWaiter(channelDetailService.getJda(), authorId, action);

        if (waitingMessages.containsKey(authorId))
            waitingMessages.get(authorId).delete();

        waitingMessages.put(authorId, messageResponseWaiter);

        messageResponseWaiter.waitForEvent(TIMEOUT, TIME_UNIT,
                () -> privateChannel.sendMessage("Timed out... Please try again later.").queue());
    }

}
