package com.wimdupont.sxcybot.listeners;

import com.wimdupont.sxcybot.enums.Command;
import com.wimdupont.sxcybot.enums.Command.Admin;
import com.wimdupont.sxcybot.listeners.member.BB8Listener;
import com.wimdupont.sxcybot.services.guild.ChatModService;
import com.wimdupont.sxcybot.util.Constants.Commands;
import jakarta.annotation.Nonnull;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.springframework.stereotype.Component;

import java.util.EnumSet;
import java.util.Optional;

@Component
public class ChatListener extends ListenerAdapter {

    private final CommandListener commandListener;
    private final AdminCommandListener adminCommandListener;
    private final BB8Listener bb8Listener;
    private final ChatModService chatModService;

    public ChatListener(CommandListener commandListener,
                        AdminCommandListener adminCommandListener,
                        BB8Listener bb8Listener,
                        ChatModService chatModService) {
        this.commandListener = commandListener;
        this.adminCommandListener = adminCommandListener;
        this.bb8Listener = bb8Listener;
        this.chatModService = chatModService;
    }

    @Override
    public void onMessageReceived(@Nonnull MessageReceivedEvent event) {
        if (event.getChannelType().isGuild()) {
            var msg = event.getMessage().getContentRaw().toLowerCase();

            if (chatModService.isAllowed(event)
                    && msg.startsWith(Commands.COMMAND_PREFIX)) {
                executeCommand(event, msg);
            }
        }
    }

    private void executeCommand(MessageReceivedEvent event, String msg) {
        var command = messageToCommand(Command.class, msg);
        if (command.isPresent()) {
            commandListener.executeCommand(event, command.get());
        } else {
            var adminCommand = messageToCommand(Admin.class, msg);
            if (adminCommand.isPresent()) {
                adminCommandListener.executeCommand(event, adminCommand.get());
            } else if (Commands.BB8.equals(msg)) {
                bb8Listener.process(event);
            }
        }
    }

    private <T extends Enum<T>> Optional<T> messageToCommand(Class<T> clazz, String msg) {
        for (T command : EnumSet.allOf(clazz)) {
            if (msg.split(" ")[0].equals(Commands.COMMAND_PREFIX + command.name().toLowerCase())) {
                return Optional.of(command);
            }
        }
        return Optional.empty();
    }
}
