package com.wimdupont.sxcybot.model;

import com.wimdupont.sxcybot.enums.OsrsCombatStat;

import java.util.List;

public record CombatDto(
        int attackLevel,
        int strengthLevel,
        int defenceLevel,
        int hitpointsLevel,
        int magicLevel,
        int rangeLevel,
        int prayerLevel
) {


    private CombatDto(Builder builder) {
        this(builder.attackLevel,
                builder.strengthLevel,
                builder.defenceLevel,
                builder.hitpointsLevel,
                builder.magicLevel,
                builder.rangeLevel,
                builder.prayerLevel);
    }

    public static CombatDto build(List<OsrsStat> osrsStatList) {
        CombatDto.Builder builder = CombatDto.Builder.newBuilder();
        for (OsrsStat osrsStat : osrsStatList) {
            if (OsrsCombatStat.ATTACK.value.equalsIgnoreCase(osrsStat.name())) {
                builder.attackLevel(Integer.parseInt(osrsStat.level()));
            }
            if (OsrsCombatStat.STRENGTH.value.equalsIgnoreCase(osrsStat.name())) {
                builder.strengthLevel(Integer.parseInt(osrsStat.level()));
            }
            if (OsrsCombatStat.DEFENCE.value.equalsIgnoreCase(osrsStat.name())) {
                builder.defenceLevel(Integer.parseInt(osrsStat.level()));
            }
            if (OsrsCombatStat.HITPOINTS.value.equalsIgnoreCase(osrsStat.name())) {
                builder.hitpointsLevel(Integer.parseInt(osrsStat.level()));
            }
            if (OsrsCombatStat.MAGIC.value.equalsIgnoreCase(osrsStat.name())) {
                builder.magicLevel(Integer.parseInt(osrsStat.level()));
            }
            if (OsrsCombatStat.RANGED.value.equalsIgnoreCase(osrsStat.name())) {
                builder.rangeLevel(Integer.parseInt(osrsStat.level()));
            }
            if (OsrsCombatStat.PRAYER.value.equalsIgnoreCase(osrsStat.name())) {
                builder.prayerLevel(Integer.parseInt(osrsStat.level()));
            }
        }
        return builder.build();
    }

    public static final class Builder {
        private int attackLevel;
        private int strengthLevel;
        private int defenceLevel;
        private int hitpointsLevel;
        private int magicLevel;
        private int rangeLevel;
        private int prayerLevel;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder attackLevel(int val) {
            attackLevel = val;
            return this;
        }

        public Builder strengthLevel(int val) {
            strengthLevel = val;
            return this;
        }

        public Builder defenceLevel(int val) {
            defenceLevel = val;
            return this;
        }

        public Builder hitpointsLevel(int val) {
            hitpointsLevel = val;
            return this;
        }

        public Builder magicLevel(int val) {
            magicLevel = val;
            return this;
        }

        public Builder rangeLevel(int val) {
            rangeLevel = val;
            return this;
        }

        public Builder prayerLevel(int val) {
            prayerLevel = val;
            return this;
        }

        public CombatDto build() {
            return new CombatDto(this);
        }
    }

    @Override
    public String toString() {
        return "CombatDto{" +
                "attackLevel=" + attackLevel +
                ", strengthLevel=" + strengthLevel +
                ", defenceLevel=" + defenceLevel +
                ", hitpointsLevel=" + hitpointsLevel +
                ", magicLevel=" + magicLevel +
                ", rangeLevel=" + rangeLevel +
                ", prayerLevel=" + prayerLevel +
                '}';
    }
}
