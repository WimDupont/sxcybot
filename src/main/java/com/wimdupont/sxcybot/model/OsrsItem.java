package com.wimdupont.sxcybot.model;

public record OsrsItem(
        String id,
        String timestamp,
        Integer price,
        String volume
) {

    private OsrsItem(Builder builder) {
        this(builder.id,
                builder.timestamp,
                builder.price,
                builder.volume);
    }

    public static final class Builder {
        private String id;
        private String timestamp;
        private Integer price;
        private String volume;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder timestamp(String val) {
            timestamp = val;
            return this;
        }

        public Builder price(Integer val) {
            price = val;
            return this;
        }

        public Builder volume(String val) {
            volume = val;
            return this;
        }

        public OsrsItem build() {
            return new OsrsItem(this);
        }
    }

    @Override
    public String toString() {
        return "OsrsItem{" +
                "id='" + id + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", price=" + price +
                ", volume='" + volume + '\'' +
                '}';
    }
}
