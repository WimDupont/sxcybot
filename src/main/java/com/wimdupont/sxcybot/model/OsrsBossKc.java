package com.wimdupont.sxcybot.model;

public record OsrsBossKc(
        String name,
        String rank,
        String kc

) {

    private OsrsBossKc(Builder builder) {
        this(builder.name,
                builder.rank,
                builder.kc);
    }

    public static final class Builder {
        private String name;
        private String rank;
        private String kc;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder rank(String val) {
            rank = val;
            return this;
        }

        public Builder kc(String val) {
            kc = val;
            return this;
        }

        public OsrsBossKc build() {
            return new OsrsBossKc(this);
        }
    }

    @Override
    public String toString() {
        return "OsrsBossKc{" +
                "name='" + name + '\'' +
                ", rank='" + rank + '\'' +
                ", kc='" + kc + '\'' +
                '}';
    }
}
