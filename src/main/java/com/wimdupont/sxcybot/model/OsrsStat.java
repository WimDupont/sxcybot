package com.wimdupont.sxcybot.model;

public record OsrsStat(
        String name,
        String experience,
        String rank,
        String level
) {


    private OsrsStat(Builder builder) {
        this(builder.name,
                builder.experience,
                builder.rank,
                builder.level);
    }

    public static final class Builder {
        private String name;
        private String experience;
        private String rank;
        private String level;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder experience(String val) {
            experience = val;
            return this;
        }

        public Builder rank(String val) {
            rank = val;
            return this;
        }

        public Builder level(String val) {
            level = val;
            return this;
        }

        public OsrsStat build() {
            return new OsrsStat(this);
        }
    }

    @Override
    public String toString() {
        return "OsrsStat{" +
                "name='" + name + '\'' +
                ", experience='" + experience + '\'' +
                ", rank='" + rank + '\'' +
                ", level='" + level + '\'' +
                '}';
    }
}
