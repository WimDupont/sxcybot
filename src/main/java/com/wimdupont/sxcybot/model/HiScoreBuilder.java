package com.wimdupont.sxcybot.model;

import com.wimdupont.sxcybot.exceptions.EntityNotFoundException;

import java.util.List;

@FunctionalInterface
public interface HiScoreBuilder<T> {

    T accept(String string, List<String> stringList, int i) throws EntityNotFoundException;
}
