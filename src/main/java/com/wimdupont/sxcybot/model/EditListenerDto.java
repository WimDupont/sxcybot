package com.wimdupont.sxcybot.model;

import com.wimdupont.sxcybot.listeners.PrivateListener;
import net.dv8tion.jda.api.entities.channel.concrete.PrivateChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.jetbrains.annotations.NotNull;

public record EditListenerDto(
        @NotNull
        PrivateListener addListener,
        PrivateListener updateListener,
        @NotNull
        PrivateListener deleteListener,
        @NotNull
        MessageReceivedEvent event,
        @NotNull
        PrivateChannel privateChannel,
        @NotNull
        String entityName
) {


    private EditListenerDto(Builder builder) {
        this(builder.addListener,
                builder.updateListener,
                builder.deleteListener,
                builder.event,
                builder.privateChannel,
                builder.entityName);
    }

    public static final class Builder {
        private PrivateListener addListener;
        private PrivateListener updateListener;
        private PrivateListener deleteListener;
        private MessageReceivedEvent event;
        private PrivateChannel privateChannel;
        private String entityName;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder addListener(@NotNull PrivateListener val) {
            addListener = val;
            return this;
        }

        public Builder updateListener(PrivateListener val) {
            updateListener = val;
            return this;
        }

        public Builder deleteListener(@NotNull PrivateListener val) {
            deleteListener = val;
            return this;
        }

        public Builder event(@NotNull MessageReceivedEvent val) {
            event = val;
            return this;
        }

        public Builder privateChannel(@NotNull PrivateChannel val) {
            privateChannel = val;
            return this;
        }

        public Builder entityName(@NotNull String val) {
            entityName = val;
            return this;
        }

        public EditListenerDto build() {
            return new EditListenerDto(this);
        }
    }

    @Override
    public String toString() {
        return "EditListenerDto{" +
                "addListener=" + addListener +
                ", updateListener=" + updateListener +
                ", deleteListener=" + deleteListener +
                ", event=" + event +
                ", privateChannel=" + privateChannel +
                ", entityName='" + entityName + '\'' +
                '}';
    }
}
