package com.wimdupont.sxcybot.enums;

public enum Command {

    FORUM("Get forum link - BUMP IT UP!"),
    PING("Get time in ms for bot to respond."),
    SPIN("Get random number; separate command by numbers to specify min/max (default is 1-20)."),
    RULE("Show rule by using command with rule number, separated by space."),
    RULES("Show list of all the rules."),
    //    EVENT("Create an event."),
    POLL("Start a poll with default options (Accept, Deny, Unsure), separate command by title with space."),
    CPOLL("Start a custom poll - choose your own poll options and reaction emoji's, separate command by title with space."),
    PVMPOLL("Poll current standings of the PvM Role competition."),
    PVMCHECK("Check if PvM competitors can be found."),
    HELP("Show all commands with description."),


    //-----OSRS-----
    STATS("Check HiScores for all stats, separate command by playername with space."),
    CBSTATS("Check HiScores for combat stats, separate command by playername with space."),
    KC("Check HiScores for all kc's, separate command by playername with space."),
    PRICE("Check the GE for the latest price of an item, separate command by name of item with space."),
    PVMLIST("Show list of PvM Role competitors."),
    BOSSLIST("Shows list of all hiscore bosses with their PvM Role point multipliers.");

    public enum Admin {

        EDITRULE("Add/Edit/Delete record of rulelist."),
        EDITBANLIST("Add/Edit/Delete record of banlist."),
        BANLIST("Show list of banned users."),
        ROLE("Add/remove a role to/from a member."),
        EDITROLE("Add/Delete record of roles."),
        EDITPVM("Add/Edit/Delete record of the PvM Role competition"),
        EDITBOSS("Update multiplier for a OSRS Boss of the PvM Role competition");

        public final String description;

        Admin(String description) {
            this.description = description;
        }
    }

    public final String description;

    Command(String description) {
        this.description = description;
    }
}
