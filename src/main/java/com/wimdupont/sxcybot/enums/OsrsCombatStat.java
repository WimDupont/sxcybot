package com.wimdupont.sxcybot.enums;

public enum OsrsCombatStat {
    ATTACK("Attack"),
    DEFENCE("Defence"),
    HITPOINTS("Hitpoints"),
    MAGIC("Magic"),
    PRAYER("Prayer"),
    RANGED("Ranged"),
    STRENGTH("Strength");

    public final String value;

    OsrsCombatStat(String value) {
        this.value = value;
    }
}
