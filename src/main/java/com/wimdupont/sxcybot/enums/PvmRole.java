package com.wimdupont.sxcybot.enums;

public enum PvmRole {
    UNUSED(0),
    GENERAL(1),
    RAIDS(2),
    WILDERNESS(3);

    public final int value;

    PvmRole(int value) {
        this.value = value;
    }
}
