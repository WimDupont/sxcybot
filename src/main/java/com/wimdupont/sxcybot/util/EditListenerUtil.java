package com.wimdupont.sxcybot.util;

import com.wimdupont.sxcybot.listeners.EventWaiter;
import com.wimdupont.sxcybot.model.EditListenerDto;
import net.dv8tion.jda.api.EmbedBuilder;
import org.springframework.stereotype.Component;

import java.awt.Color;

@Component
public class EditListenerUtil {

    private final EventWaiter eventWaiter;

    public EditListenerUtil(EventWaiter eventWaiter) {
        this.eventWaiter = eventWaiter;
    }

    public void procesEditEvent(EditListenerDto editListenerDto) {
        EmbedBuilder embedBuilder = getEmbedBuilder(editListenerDto);
        editListenerDto.privateChannel().sendMessageEmbeds(embedBuilder.build()).queue();
        eventWaiter.waitForPrivateChannelEvent(commandReceiver -> {
            switch (commandReceiver.getMessage().getContentRaw()) {
                case "1" -> editListenerDto.addListener().process(commandReceiver, editListenerDto.event());
                case "2" -> editListenerDto.deleteListener().process(commandReceiver, editListenerDto.event());
                case "3" -> {
                    if (editListenerDto.updateListener() != null)
                        editListenerDto.updateListener().process(commandReceiver, editListenerDto.event());
                }
                default -> editListenerDto.privateChannel().sendMessage("Unknown command, please try again.").queue();
            }
        }, editListenerDto.event(), editListenerDto.privateChannel());
    }

    private static EmbedBuilder getEmbedBuilder(EditListenerDto editListenerDto) {
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setColor(Color.red);
        embedBuilder.setTitle("What do you want to do?");
        embedBuilder.addField("1", String.format("Add a new %s.", editListenerDto.entityName()), false);
        embedBuilder.addField("2", String.format("Delete an existing %s.", editListenerDto.entityName()), false);
        if (editListenerDto.updateListener() != null)
            embedBuilder.addField("3", String.format("Update an existing %s.", editListenerDto.entityName()), false);
        return embedBuilder;
    }

}
