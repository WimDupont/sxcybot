package com.wimdupont.sxcybot.util;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.react.GenericMessageReactionEvent;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class JdaUtil {

    public static Optional<String> getName(GenericMessageReactionEvent event) {
        var nickName = Optional.ofNullable(event.getMember())
                .map(Member::getNickname);
        return nickName.isPresent() ? nickName : Optional.ofNullable(event.getUser())
                .map(User::getName);
    }

    public static Optional<String> getName(MessageReceivedEvent event) {
        var nickName = Optional.ofNullable(event.getMember())
                .map(Member::getNickname);
        return nickName.isPresent() ? nickName : Optional.ofNullable(event.getMember())
                .map(Member::getUser)
                .map(User::getName);
    }

    public static Optional<String> getNameById(List<Member> members, String discordId) {
        return getMemberById(members, discordId)
                .map(Member::getEffectiveName);
    }

    public static Optional<Member> getMemberById(List<Member> members, String discordId) {
        return members.stream()
                .filter(f -> f.getUser().getId().equals(discordId))
                .findAny();
    }

    public static User getUser(MessageReceivedEvent event) {
        return event.getMember() != null ? event.getMember().getUser() : null;
    }

    public static Optional<User> getUserByName(List<Member> members, String discordName) {
        return members.stream()
                .filter(member -> member.getEffectiveName().equalsIgnoreCase(discordName)
                        || (member.getNickname() != null && member.getNickname().equalsIgnoreCase(discordName))
                        || member.getUser().getName().equalsIgnoreCase(discordName))
                .findAny()
                .map(Member::getUser);
    }

    public static boolean requiresBuild(EmbedBuilder embedBuilder, int size, int count) {
        return (embedBuilder.length() > 5000 || embedBuilder.getFields().size() == 25 || size == count);
    }
}
