package com.wimdupont.sxcybot.util;

import java.text.NumberFormat;
import java.util.Locale;

public class NumberFormatter {

    public static String format(String numberToFormat) {
        return format(Integer.valueOf(numberToFormat));
    }

    public static String format(Integer numberToFormat) {
        return NumberFormat.getNumberInstance(Locale.UK).format(numberToFormat);
    }
}
