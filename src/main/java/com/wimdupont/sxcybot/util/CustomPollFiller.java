package com.wimdupont.sxcybot.util;

import com.wimdupont.sxcybot.listeners.EventWaiter;
import com.wimdupont.sxcybot.repository.guild.dao.Poll;
import com.wimdupont.sxcybot.services.guild.PollService;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.channel.concrete.PrivateChannel;
import net.dv8tion.jda.api.entities.emoji.Emoji;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.awt.Color;
import java.util.Set;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CustomPollFiller {

    private final EventWaiter eventWaiter;
    private final PollService pollService;
    private static final String STOP_WORD = "stop";

    public CustomPollFiller(EventWaiter eventWaiter,
                            PollService pollService) {
        this.eventWaiter = eventWaiter;
        this.pollService = pollService;
    }

//    private static final String EMOJI_REGEX = "([\\u20a0-\\u32ff\\ud83c\\udc00-\\ud83d\\udeff\\udbb9\\udce5-\\udbb9\\udcee])";

    public void fillPoll(PrivateChannel privateChannel, MessageReceivedEvent event, EmbedBuilder embedBuilder, Set<String> emojiList) {
        EmbedBuilder embeddedOption = new EmbedBuilder();
        embeddedOption.setColor(Color.RED);
        embeddedOption.addField("Please enter an option for the poll", "`stop` to finish and create the poll.", false);
        privateChannel.sendMessageEmbeds(embeddedOption.build()).queue();
        embeddedOption.clear();
        eventWaiter.waitForPrivateChannelEvent(optionReceiver -> {
            if (isReadyToStop(optionReceiver)) {
                event.getChannel().sendTyping().queue(typing ->
                        event.getChannel().sendMessageEmbeds(embedBuilder.build()).queue(message -> {
                            emojiList.forEach(emoji -> message.addReaction(Emoji.fromUnicode(emoji)).queue());
                            pollService.save(Poll.Builder.newBuilder().messageId(message.getId()).build());
                        }));
                return;
            }
            embeddedOption.setColor(Color.RED);
            embeddedOption.addField(String.format("Please type in a reaction emoji for _%s_.", optionReceiver.getMessage().getContentRaw()), "", false);
            privateChannel.sendMessageEmbeds(embeddedOption.build()).queue();
            embeddedOption.clear();
            eventWaiter.waitForPrivateChannelEvent(emojiReceiver -> {
                if (!emojiList.contains(emojiReceiver.getMessage().getContentRaw())) {
                    if (!isValidEmoji(emojiReceiver.getMessage().getContentRaw())) {
                        privateChannel.sendMessage(String.format("%s is not a valid emoji, please retry this option.", emojiReceiver.getMessage().getContentRaw())).queue();
                        this.fillPoll(privateChannel, event, embedBuilder, emojiList);
                        return;
                    }
                    embedBuilder.addField(String.format("%s %s %s", emojiReceiver.getMessage().getContentRaw(), optionReceiver.getMessage().getContentRaw(), "(0)"), "", true);
                    emojiList.add(emojiReceiver.getMessage().getContentRaw());
                } else {
                    privateChannel.sendMessage(String.format("Emoji %s was already picked for another options, please retry this option.", emojiReceiver.getMessage().getContentRaw())).queue();
                }
                this.fillPoll(privateChannel, event, embedBuilder, emojiList);
            }, event, privateChannel);
        }, event, privateChannel);
    }

    private boolean isReadyToStop(MessageReceivedEvent privateMessageReceivedEvent) {
        return privateMessageReceivedEvent.getMessage().getContentRaw().equalsIgnoreCase(STOP_WORD);
    }

    private boolean isValidEmoji(String emoji) {
        /*
        TODO fix:
        return Pattern.compile(EMOJI_REGEX).matcher(emoji).find();
         */
        return true;
    }
}
