package com.wimdupont.sxcybot.util;

import com.wimdupont.sxcybot.listeners.EventWaiter;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.channel.concrete.PrivateChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.function.Consumer;

@Component
public class DiscordMemberFinderUtil {

    private final EventWaiter eventWaiter;

    public DiscordMemberFinderUtil(EventWaiter eventWaiter) {
        this.eventWaiter = eventWaiter;
    }

    public void onMemberFoundVerification(MessageReceivedEvent event, EmbedBuilder embedBuilder, PrivateChannel privateChannel, Consumer<Member> memberConsumer) {
        embedBuilder.addField("Type the name of the user.", "Discord username will be looked up first, if no match is found it will search on nickname.", false);
        privateChannel.sendMessageEmbeds(embedBuilder.build()).queue();
        eventWaiter.waitForPrivateChannelEvent(memberReceiver -> {
            String name = memberReceiver.getMessage().getContentRaw();
            event.getGuild().loadMembers().onSuccess(memberList -> {
                if (memberList.isEmpty()) {
                    privateChannel.sendMessage(String.format("User with name %s not found, please try again later.", name)).queue();
                } else {
                    Optional<Member> member = memberList.stream().filter(f -> name.equalsIgnoreCase(f.getUser().getName()) || name.equalsIgnoreCase(f.getNickname())).findFirst();
                    if (member.isPresent()) {
                        embedBuilder.clearFields();
                        embedBuilder.addField("Confirm by typing ``yes or y``.", String.format("Is %s the correct member?", member.get()), false);
                        privateChannel.sendMessageEmbeds(embedBuilder.build()).queue();
                        eventWaiter.waitForPrivateChannelEvent(memberVerifyReceiver -> {
                            String verifyMessage = memberVerifyReceiver.getMessage().getContentRaw();
                            if ("y".equalsIgnoreCase(verifyMessage) || "yes".equalsIgnoreCase(verifyMessage)) {
                                memberConsumer.accept(member.get());
                            }
                            embedBuilder.clearFields();
                        }, event, privateChannel);
                    } else {
                        privateChannel.sendMessage(String.format("User with name %s not found, please try again later.", name)).queue();
                    }
                }
            });
        }, event, privateChannel);
    }
}
