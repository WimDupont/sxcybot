package com.wimdupont.sxcybot.util;


public class Constants {

    private Constants() {
    }

    public static final int ADDED_ROLE_ELEVATION = 100;
    public static final int ADDED_TEACHER_ROLE_ELEVATION = 120;

    public static class Commands {
        private Commands() {
        }

        public static final String COMMAND_PREFIX = "sb-";
        public static final String BB8 = COMMAND_PREFIX + "8";
    }

    public static class Emoji {

        private Emoji() {
        }

        public static final String THUMBS_UP = ":+1:";
        public static final String FLAMES = ":flame:";
    }

    public static class Reaction {
        private Reaction() {
        }

        //TODO remove if not needed
        public static final String CHECK_MARK = "U+2714";
        public static final String CROSS_MARK = "U+274C";
        public static final String QUESTION_MARK = "U+2753";
        public static final String CHECK_MARK_BUTTON = "U+2705";
        public static final String CROSS_MARK_BUTTON = "U+274E";
    }

    public static class ROLE_ACCESS {

        private ROLE_ACCESS() {
        }

        public static final int GOD = 0;
        public static final int ADMIN_ROLE = 1;
        public static final int GENERAL = 3;
        public static final int SUB_ADMIN = 5;
        public static final int STAFF_ROLE = 10;
        public static final int TEACHER = 20;
    }
}
