package com.wimdupont.sxcybot.util;


import com.wimdupont.sxcybot.repository.guild.dao.ChannelDetail;
import com.wimdupont.sxcybot.services.guild.ChannelDetailService;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.awt.Color;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

@Component
public class ReleaseNotesUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReleaseNotesUtil.class);
    private final ChannelDetailService channelDetailService;

    private static final String GITLAB_URL = "https://git.wimdupont.com/sxcybot";

    public ReleaseNotesUtil(ChannelDetailService channelDetailService) {
        this.channelDetailService = channelDetailService;
    }

    public void showReleaseNotes(JDA jda) {
        Map<String, String> releaseNotes = ReleaseNotesUtil.getReleaseNotes();
        if (!releaseNotes.isEmpty()) {
            EmbedBuilder embedBuilder = new EmbedBuilder();
            embedBuilder.setColor(Color.RED);
            embedBuilder.setTitle(String.format("Latest updates %s", LocalDate.now()), GITLAB_URL);
            for (Entry<String, String> entry : releaseNotes.entrySet()) {
                embedBuilder.addField(entry.getKey(), entry.getValue(), false);
            }

            for (ChannelDetail channelDetail : channelDetailService.findAll()) {
                var channel = jda.getTextChannelById(channelDetail.getBotUpdateChannel());
                if (channel != null)
                    channel.sendMessageEmbeds(embedBuilder.build()).queue();
            }
        }
    }

    private static Map<String, String> getReleaseNotes() {
        Map<String, String> releaseNotes = new LinkedHashMap<>();
        try {
            InputStream in = Optional.ofNullable(ReleaseNotesUtil.class.getResourceAsStream("/releasenotes.csv"))
                    .orElseThrow(() -> new IOException("File releasenotes.csv not found!"));
            Arrays.stream(new String(in.readAllBytes(), StandardCharsets.UTF_8).split("\n")).forEach(line -> {
                if (!line.isEmpty())
                    releaseNotes.put(line.split(";")[0], line.split(";")[1].trim());
            });
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return releaseNotes;
    }
}
