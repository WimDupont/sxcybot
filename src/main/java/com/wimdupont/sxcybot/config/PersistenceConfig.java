package com.wimdupont.sxcybot.config;

import com.wimdupont.sxcybot.util.SpringSecurityAuditorAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditorProvider")
public class PersistenceConfig {

    private final SpringSecurityAuditorAware springSecurityAuditorAware;

    public PersistenceConfig(SpringSecurityAuditorAware springSecurityAuditorAware) {
        this.springSecurityAuditorAware = springSecurityAuditorAware;
    }

    @Bean
    public AuditorAware<String> auditorProvider() {
        return springSecurityAuditorAware;
    }
}
