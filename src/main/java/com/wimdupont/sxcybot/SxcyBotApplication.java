package com.wimdupont.sxcybot;

import com.wimdupont.sxcybot.enums.Command;
import com.wimdupont.sxcybot.listeners.ChatListener;
import com.wimdupont.sxcybot.listeners.GuildMemberEventListener;
import com.wimdupont.sxcybot.listeners.PollReactionListener;
import com.wimdupont.sxcybot.services.PvMRoleResolver;
import com.wimdupont.sxcybot.services.guild.ChannelDetailService;
import com.wimdupont.sxcybot.util.Constants.Commands;
import com.wimdupont.sxcybot.util.ReleaseNotesUtil;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.requests.GatewayIntent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.Arrays;

@SpringBootApplication
@EnableScheduling
@EnableAsync
public class SxcyBotApplication implements CommandLineRunner {

    private static final Logger LOGGER = LoggerFactory.getLogger(SxcyBotApplication.class);
    private final ChatListener chatListener;
    private final PollReactionListener pollReactionListener;
    private final GuildMemberEventListener guildMemberEventListener;
    private final ReleaseNotesUtil releaseNotesUtil;
    private final ChannelDetailService channelDetailService;
    private final PvMRoleResolver pvMRoleResolver;
    private final String token;

    public SxcyBotApplication(ChatListener chatListener,
                              PollReactionListener pollReactionListener,
                              GuildMemberEventListener guildMemberEventListener,
                              ReleaseNotesUtil releaseNotesUtil,
                              ChannelDetailService channelDetailService,
                              PvMRoleResolver pvMRoleResolver,
                              @Value("${discord.bot.token}") String token) {
        this.chatListener = chatListener;
        this.pollReactionListener = pollReactionListener;
        this.guildMemberEventListener = guildMemberEventListener;
        this.releaseNotesUtil = releaseNotesUtil;
        this.channelDetailService = channelDetailService;
        this.pvMRoleResolver = pvMRoleResolver;
        this.token = token;
    }

    public static void main(String[] args) {
        SpringApplication.run(SxcyBotApplication.class, args);
    }

    @Override
    public void run(String[] args) {
        LOGGER.debug("Jda to be created");
        JDA jda = JDABuilder.createLight(token,
                        GatewayIntent.MESSAGE_CONTENT,
                        GatewayIntent.GUILD_MESSAGES,
                        GatewayIntent.DIRECT_MESSAGES,
                        GatewayIntent.GUILD_MESSAGE_REACTIONS,
                        GatewayIntent.GUILD_MEMBERS,
                        GatewayIntent.GUILD_MODERATION
                )
                .addEventListeners(chatListener, guildMemberEventListener, pollReactionListener)
                .setActivity(Activity.listening(Commands.COMMAND_PREFIX + Command.HELP.name().toLowerCase()))
                .build();
        try {
            LOGGER.debug("Awaiting jda {}", jda);
            jda.awaitReady();
            LOGGER.debug("Jda {} ready", jda);
            channelDetailService.setJda(jda);
            processArgs(jda, args);
        } catch (InterruptedException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    private void processArgs(JDA jda, String... args) {
        if (args.length > 0) {
            if (Arrays.stream(args).anyMatch("notes"::equalsIgnoreCase))
                releaseNotesUtil.showReleaseNotes(jda);
            if (Arrays.stream(args).anyMatch("snap"::equalsIgnoreCase))
                pvMRoleResolver.resolvePvMRoles();
        }
    }
}
