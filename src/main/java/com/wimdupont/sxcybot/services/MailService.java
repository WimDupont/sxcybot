package com.wimdupont.sxcybot.services;

import com.wimdupont.sxcybot.config.MailConfig;
import jakarta.el.PropertyNotFoundException;
import jakarta.mail.Authenticator;
import jakarta.mail.Message;
import jakarta.mail.MessagingException;
import jakarta.mail.PasswordAuthentication;
import jakarta.mail.Session;
import jakarta.mail.Transport;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Properties;

@Service
public class MailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MailService.class);

    private final MailConfig mailConfig;

    public MailService(MailConfig mailConfig) {
        this.mailConfig = mailConfig;
        if (mailConfig.isEnabled()
                && (mailConfig.getFrom() == null
                || mailConfig.getTo() == null
                || mailConfig.getHost() == null)) {
            throw new PropertyNotFoundException("Mail is enabled and required properties are missing.");
        }
    }

    @Async
    public void sendMail(String subject, String body) {
        if (mailConfig.isEnabled()) {
            var properties = new Properties();
            properties.put("mail.smtp.auth", withAuth());
            properties.put("mail.smtp.starttls.enable", mailConfig.isSsl());
            properties.put("mail.smtp.host", mailConfig.getHost());
            if (mailConfig.getPort() != null) {
                properties.put("mail.smtp.port", mailConfig.getPort());
            }
            var session = Session.getInstance(properties, getAuthenticator());

            try {
                var message = new MimeMessage(session);
                message.setFrom(new InternetAddress(mailConfig.getFrom()));
                message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mailConfig.getTo()));
                message.setSubject(subject);
                message.setText(body);
                Transport.send(message);
                LOGGER.info("Email sent successfully.");
            } catch (MessagingException e) {
                throw new RuntimeException(e);
            }
        } else {
            LOGGER.info("Mail is disabled - no mail sent for subject {} ", subject);
        }
    }

    private Authenticator getAuthenticator() {
        if (!withAuth()) {
            return null;
        }
        return new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(
                        mailConfig.getUsername(),
                        mailConfig.getPassword());
            }
        };
    }

    private boolean withAuth() {
        return mailConfig.getUsername() != null && mailConfig.getPassword() != null;
    }

}
