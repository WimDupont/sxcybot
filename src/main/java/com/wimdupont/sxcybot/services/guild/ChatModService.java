package com.wimdupont.sxcybot.services.guild;

import com.wimdupont.sxcybot.repository.guild.ChatDataRepository;
import com.wimdupont.sxcybot.repository.guild.dao.ChatData;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.exceptions.ErrorResponseException;
import net.dv8tion.jda.api.exceptions.HierarchyException;
import net.dv8tion.jda.api.exceptions.RateLimitedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Transactional
public class ChatModService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChatModService.class);
    private final ChatDataRepository chatDataRepository;

    public ChatModService(ChatDataRepository chatDataRepository) {
        this.chatDataRepository = chatDataRepository;
    }

    public boolean isAllowed(MessageReceivedEvent event) {
        if (event.getMember() == null || event.getMember().getUser().isBot() || !containsUrl(event))
            return true;

        var discordId = event.getMember().getUser().getId();
        var previousUrlMessages = chatDataRepository.findAllByDiscordIdAndWithUrlTrueOrderByCreatedDateTimeDesc(discordId);
        var currentData = persist(discordId, event.getChannel().asTextChannel().getId(), event.getMessageId());

        return isValidForHistory(previousUrlMessages, event, currentData);
    }

    private boolean isValidForHistory(List<ChatData> previousUrlMessages,
                                      MessageReceivedEvent event,
                                      ChatData currentData) {
        if (previousUrlMessages.isEmpty())
            return true;
        try {
            LocalDateTime latest = previousUrlMessages.stream().findFirst()
                    .map(ChatData::getCreatedDateTime)
                    .orElseThrow();
            if (latest.plusMinutes(1L).isBefore(LocalDateTime.now())) {
                chatDataRepository.deleteAll(previousUrlMessages);
            } else if (previousUrlMessages.size() >= 2 && previousUrlMessages.size() < 4) {
                dmWarning(event);
            } else if (previousUrlMessages.size() >= 4 && event.getMember() != null) {
                try {
                    event.getMember().kick().reason("Spamming URL's.").queue();
                } catch (HierarchyException | ErrorResponseException e) {
                    LOGGER.error(e.getMessage(), e);
                }
                for (ChatData chatData : previousUrlMessages) {
                    removeMessage(event.getGuild(), chatData);
                }
                removeMessage(event.getGuild(), currentData);
                return false;
            }
        } catch (RateLimitedException | ErrorResponseException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return true;
    }

    private ChatData persist(String discordId, String channelId, String messageId) {
        return chatDataRepository.save(ChatData.Builder.newBuilder()
                .discordId(discordId)
                .channelId(channelId)
                .messageId(messageId)
                .withUrl(true)
                .build());
    }

    private void dmWarning(MessageReceivedEvent event) throws RateLimitedException {
        if (event.getMember() != null) {
            var privateChannel = event.getMember().getUser().openPrivateChannel().complete(true);
            privateChannel.sendMessage(String.format("Warning: stop spamming URL's! (server: %s)",
                    event.getGuild().getName())).queue();
        }
    }

    private void removeMessage(Guild guild, ChatData chatData) throws RateLimitedException {
        var channel = guild.getTextChannelById(chatData.getChannelId());
        if (channel != null) {
            channel.retrieveMessageById(chatData.getMessageId()).complete(true).delete().queue();
            chatDataRepository.delete(chatData);
        }
    }

    private boolean containsUrl(MessageReceivedEvent event) {
        var msg = event.getMessage().getContentRaw().toLowerCase();
        if (containsUrl(msg))
            return true;

        if (!event.getMessage().getEmbeds().isEmpty()) {
            return event.getMessage().getEmbeds().stream()
                    .anyMatch(f -> StringUtils.hasLength(f.getUrl())
                            || containsUrl(f.getTitle())
                            || containsUrl(f.getDescription()));
        }
        return false;
    }

    private boolean containsUrl(String msg) {
        return msg != null && msg.matches(".*https?://.*|.*www\\..*");
    }
}
