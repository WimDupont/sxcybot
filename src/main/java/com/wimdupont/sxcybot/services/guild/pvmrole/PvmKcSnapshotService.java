package com.wimdupont.sxcybot.services.guild.pvmrole;

import com.wimdupont.sxcybot.repository.guild.pvmrole.PvmKcSnapshotRepository;
import com.wimdupont.sxcybot.repository.guild.pvmrole.dao.PvmKcSnapshot;
import com.wimdupont.sxcybot.repository.guild.pvmrole.dao.PvmRoleUser;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PvmKcSnapshotService {

    private final PvmKcSnapshotRepository pvmKcSnapshotRepository;

    public PvmKcSnapshotService(PvmKcSnapshotRepository pvmKcSnapshotRepository) {
        this.pvmKcSnapshotRepository = pvmKcSnapshotRepository;
    }

    public List<PvmKcSnapshot> findAll() {
        return pvmKcSnapshotRepository.findAll();
    }

    public List<PvmKcSnapshot> findAllByPvmRoleUser(PvmRoleUser pvmRoleUser) {
        return pvmKcSnapshotRepository.findAllByPvmRoleUserId(pvmRoleUser.getId());
    }

    public PvmKcSnapshot save(PvmKcSnapshot pvmKcSnapshot) {
        return pvmKcSnapshotRepository.save(pvmKcSnapshot);
    }

    public void deleteAll(List<PvmKcSnapshot> pvmKcSnapshots) {
        pvmKcSnapshotRepository.deleteAll(pvmKcSnapshots);
    }
}
