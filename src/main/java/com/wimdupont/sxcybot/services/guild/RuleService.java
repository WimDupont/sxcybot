package com.wimdupont.sxcybot.services.guild;

import com.wimdupont.sxcybot.exceptions.EntityNotFoundException;
import com.wimdupont.sxcybot.repository.guild.RuleRepository;
import com.wimdupont.sxcybot.repository.guild.dao.Rule;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RuleService {

    private final RuleRepository ruleRepository;

    public RuleService(RuleRepository ruleRepository) {
        this.ruleRepository = ruleRepository;
    }

    public List<Rule> findAll() {
        return ruleRepository.findAll();
    }

    public Rule findByNumber(int number) throws EntityNotFoundException {
        return ruleRepository.findByNumber(number).orElseThrow(
                () -> new EntityNotFoundException(String.format("Rule with number %s not found.", number)));
    }

    public Rule save(Rule rule) {
        return ruleRepository.save(rule);
    }

    public void delete(Rule rule) {
        ruleRepository.delete(rule);
    }
}
