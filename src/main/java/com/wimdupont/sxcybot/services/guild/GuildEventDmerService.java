package com.wimdupont.sxcybot.services.guild;

import com.wimdupont.sxcybot.repository.guild.GuildEventDmerRepository;
import com.wimdupont.sxcybot.repository.guild.dao.GuildEventDmer;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class GuildEventDmerService {

    private final GuildEventDmerRepository guildEventDmerRepository;

    public GuildEventDmerService(GuildEventDmerRepository guildEventDmerRepository) {
        this.guildEventDmerRepository = guildEventDmerRepository;
    }

    public List<GuildEventDmer> findAll() {
        return guildEventDmerRepository.findAll();
    }
}
