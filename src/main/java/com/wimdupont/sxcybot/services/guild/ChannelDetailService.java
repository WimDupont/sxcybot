package com.wimdupont.sxcybot.services.guild;

import com.wimdupont.sxcybot.repository.guild.ChannelDetailRepository;
import com.wimdupont.sxcybot.repository.guild.dao.ChannelDetail;
import net.dv8tion.jda.api.JDA;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ChannelDetailService {

    private final ChannelDetailRepository channelDetailRepository;

    private JDA jda;

    public JDA getJda() {
        return jda;
    }

    public void setJda(JDA jda) {
        this.jda = jda;
    }

    public ChannelDetailService(ChannelDetailRepository channelDetailRepository) {
        this.channelDetailRepository = channelDetailRepository;
    }

    public List<ChannelDetail> findAll() {
        return channelDetailRepository.findAll();
    }
}
