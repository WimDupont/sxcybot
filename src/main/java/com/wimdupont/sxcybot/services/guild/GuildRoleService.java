package com.wimdupont.sxcybot.services.guild;

import com.wimdupont.sxcybot.exceptions.EntityNotFoundException;
import com.wimdupont.sxcybot.repository.guild.GuildRoleRepository;
import com.wimdupont.sxcybot.repository.guild.dao.GuildRole;
import net.dv8tion.jda.api.entities.ISnowflake;
import net.dv8tion.jda.api.entities.Role;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class GuildRoleService {

    private final GuildRoleRepository guildRoleRepository;

    public GuildRoleService(GuildRoleRepository guildRoleRepository) {
        this.guildRoleRepository = guildRoleRepository;
    }

    public List<GuildRole> findAllByElevationLessThanEqual(int elevation) {
        return guildRoleRepository.findByElevationLessThanEqual(elevation);
    }

    public List<GuildRole> findAllByElevationGreaterThanEqual(int elevation) {
        return guildRoleRepository.findByElevationGreaterThanEqual(elevation);
    }

    public GuildRole findByDiscordId(String discordId) throws EntityNotFoundException {
        return guildRoleRepository.findByDiscordId(discordId).orElseThrow(
                () -> new EntityNotFoundException(String.format("No role found with discordId %s.", discordId)));
    }

    public GuildRole save(GuildRole guildRole) {
        return guildRoleRepository.save(guildRole);
    }

    public void delete(GuildRole guildRole) {
        guildRoleRepository.delete(guildRole);
    }

    public List<GuildRole> findAll() {
        return guildRoleRepository.findAll();
    }

    public GuildRole findByName(List<Role> roles,
                                String roleName)
            throws EntityNotFoundException {
        var discordId = discordIdByName(roles, roleName);
        if (discordId.isPresent()) {
            return findByDiscordId(discordId.get());
        }
        throw new EntityNotFoundException(String.format("No role found with discordId %s.", discordId));
    }

    public Optional<String> discordIdByName(List<Role> roles,
                                            String roleName) {
        return roles.stream()
                .filter(f -> roleName.equalsIgnoreCase(f.getName()))
                .map(ISnowflake::getId)
                .findFirst();
    }
}
