package com.wimdupont.sxcybot.services.guild.pvmrole;

import com.wimdupont.sxcybot.exceptions.EntityNotFoundException;
import com.wimdupont.sxcybot.repository.guild.pvmrole.PvmRoleUserRepository;
import com.wimdupont.sxcybot.repository.guild.pvmrole.dao.PvmRoleUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.wimdupont.sxcybot.enums.Command.PVMLIST;
import static com.wimdupont.sxcybot.util.Constants.Commands.COMMAND_PREFIX;

@Service
public class PvmRoleUserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PvmRoleUserService.class);
    private final PvmRoleUserRepository pvmRoleUserRepository;

    public PvmRoleUserService(PvmRoleUserRepository pvmRoleUserRepository) {
        this.pvmRoleUserRepository = pvmRoleUserRepository;
    }

    public List<PvmRoleUser> findAll() {
        return pvmRoleUserRepository.findAll();
    }

    public PvmRoleUser save(PvmRoleUser pvmRoleUser) {
        return pvmRoleUserRepository.save(pvmRoleUser);
    }

    public PvmRoleUser findByDiscordId(String discordId) throws EntityNotFoundException {
        LOGGER.debug("Searching pvmRoleUser by id: {}", discordId);
        return pvmRoleUserRepository.findByDiscordId(discordId).orElseThrow(()
                -> new EntityNotFoundException(String.format("No such discord user found in the PvM competition.%sYou can check all competitors with the \"%s\" command."
                , System.lineSeparator(), COMMAND_PREFIX + PVMLIST.name().toLowerCase())));
    }

    public PvmRoleUser findByRsn(String rsn) throws EntityNotFoundException {
        LOGGER.debug("Searching pvmRoleUser by RSN: {}", rsn);
        return pvmRoleUserRepository.findByRsn(rsn).orElseThrow(()
                -> new EntityNotFoundException(String.format("No such discord user found in the PvM competition.%sYou can check all competitors with the \"%s\" command."
                , System.lineSeparator(), COMMAND_PREFIX + PVMLIST.name().toLowerCase())));
    }

    public void delete(PvmRoleUser pvmRoleUser) {
        pvmRoleUserRepository.delete(pvmRoleUser);
    }
}
