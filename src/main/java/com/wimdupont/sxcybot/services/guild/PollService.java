package com.wimdupont.sxcybot.services.guild;

import com.wimdupont.sxcybot.repository.guild.PollRepository;
import com.wimdupont.sxcybot.repository.guild.dao.Poll;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class PollService {

    private final PollRepository pollRepository;

    public PollService(PollRepository pollRepository) {
        this.pollRepository = pollRepository;
    }

    public List<Poll> findAll() {
        return pollRepository.findAll();
    }

    public Optional<Poll> findByMessageId(String messageId) {
        return pollRepository.findByMessageId(messageId);
    }

    public Poll save(Poll poll) {
        return pollRepository.save(poll);
    }

    public void delete(Poll poll) {
        pollRepository.delete(poll);
    }
}
