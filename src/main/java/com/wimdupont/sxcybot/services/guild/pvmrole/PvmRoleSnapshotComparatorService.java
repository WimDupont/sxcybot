package com.wimdupont.sxcybot.services.guild.pvmrole;

import com.wimdupont.sxcybot.client.HiScoreClient;
import com.wimdupont.sxcybot.enums.PvmRole;
import com.wimdupont.sxcybot.exceptions.EntityNotFoundException;
import com.wimdupont.sxcybot.model.OsrsBossKc;
import com.wimdupont.sxcybot.repository.guild.pvmrole.dao.PvmKcSnapshot;
import com.wimdupont.sxcybot.repository.guild.pvmrole.dao.PvmRoleUser;
import com.wimdupont.sxcybot.repository.guild.pvmrole.dao.PvmUserKc;
import com.wimdupont.sxcybot.services.osrs.OsrsHiscoreBossService;
import net.dv8tion.jda.api.entities.channel.middleman.MessageChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class PvmRoleSnapshotComparatorService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PvmRoleSnapshotComparatorService.class);
    private final OsrsHiscoreBossService osrsHiscoreBossService;
    private final PvmKcSnapshotService pvmKcSnapshotService;
    private final HiScoreClient hiScoreClient;

    public PvmRoleSnapshotComparatorService(OsrsHiscoreBossService osrsHiscoreBossService,
                                            PvmKcSnapshotService pvmKcSnapshotService,
                                            HiScoreClient hiScoreClient) {
        this.osrsHiscoreBossService = osrsHiscoreBossService;
        this.pvmKcSnapshotService = pvmKcSnapshotService;
        this.hiScoreClient = hiScoreClient;
    }

    public void updateScoreboard(PvmRoleUser pvmRoleUser, Map<PvmRole, LinkedHashMap<PvmRoleUser, BigDecimal>> scoreBoard, List<PvmKcSnapshot> unpersistedSnapshots) {
        boolean persisted = unpersistedSnapshots == null;
        List<PvmKcSnapshot> pvmKcSnapshots = pvmKcSnapshotService.findAllByPvmRoleUser(pvmRoleUser);
        if (pvmKcSnapshots != null && (pvmKcSnapshots.size() > 1 || (!persisted && !pvmKcSnapshots.isEmpty()))) {
            pvmKcSnapshots = pvmKcSnapshots.stream()
                    .sorted(Comparator.comparing(PvmKcSnapshot::getCreatedDate)
                            .reversed())
                    .limit(persisted ? 2 : 1)
                    .toList();
            for (PvmRole pvmRole : scoreBoard.keySet()) {
                BigDecimal points1 = calculatePoints(pvmRole, persisted ? pvmKcSnapshots.get(0) : unpersistedSnapshots.getFirst());
                BigDecimal points2 = calculatePoints(pvmRole, pvmKcSnapshots.get(persisted ? 1 : 0));
                BigDecimal result = points1.subtract(points2);
                if (BigDecimal.ZERO.compareTo(result) < 0)
                    scoreBoard.get(pvmRole).put(pvmRoleUser, result);
            }
        }
    }

    private BigDecimal calculatePoints(PvmRole pvmRole, PvmKcSnapshot pvmKcSnapshot) {
        BigDecimal points = BigDecimal.ZERO;
        for (PvmUserKc pvmUserKc : pvmKcSnapshot.getPvmUserKcList().stream().filter(f -> pvmRole.value == f.getOsrsHiscoreBoss().getPvmRole()).toList()) {
            points = points.add(BigDecimal.valueOf(pvmUserKc.getOsrsHiscoreBoss().getMultiplier()).multiply(BigDecimal.valueOf(pvmUserKc.getKc())));
        }
        return points;
    }

    public PvmKcSnapshot takeSnapshot(MessageChannel textChannel, PvmRoleUser pvmRoleUser, boolean notify, boolean persist) throws EntityNotFoundException {
        PvmKcSnapshot pvmKcSnapshot = PvmKcSnapshot.Builder.newBuilder()
                .pvmRoleUser(pvmRoleUser)
                .build();
        List<OsrsBossKc> osrsBossKcList = hiScoreClient.getHiScoreBossKc(pvmRoleUser.getRsn(), textChannel);
        List<PvmUserKc> pvmUserKcList = new ArrayList<>();
        for (OsrsBossKc osrsBossKc : osrsBossKcList.stream().filter(r -> Integer.parseInt(r.kc()) > 0).toList()) {
            try {
                PvmUserKc pvmUserKc = PvmUserKc.Builder.newBuilder()
                        .pvmRoleUser(pvmRoleUser)
                        .osrsHiscoreBoss(osrsHiscoreBossService.findByName(osrsBossKc.name()))
                        .pvmKcSnapshot(pvmKcSnapshot)
                        .kc(Integer.valueOf(osrsBossKc.kc()))
                        .build();
                pvmUserKcList.add(pvmUserKc);
            } catch (EntityNotFoundException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
        pvmKcSnapshot.setPvmUserKcList(pvmUserKcList);

        if (persist) {
            pvmKcSnapshot = pvmKcSnapshotService.save(pvmKcSnapshot);
            if (notify)
                textChannel.sendMessage(String.format("KC snapshot saved for %s.%n", pvmRoleUser.getRsn())).queue();
        }

        return pvmKcSnapshot;
    }
}
