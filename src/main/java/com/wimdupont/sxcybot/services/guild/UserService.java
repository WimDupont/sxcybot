package com.wimdupont.sxcybot.services.guild;

import com.wimdupont.sxcybot.exceptions.EntityNotFoundException;
import com.wimdupont.sxcybot.repository.guild.UserRepository;
import com.wimdupont.sxcybot.repository.guild.dao.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> findAllBanned() {
        return userRepository.findAllByBannedTrue();
    }

    public User save(User user) {
        return userRepository.save(user);
    }

    public void delete(User user) throws EntityNotFoundException {
        findByName(user.getName());
        userRepository.delete(user);
    }

    public User findByName(String name) throws EntityNotFoundException {
        return userRepository.findByName(name).orElseThrow(
                () -> new EntityNotFoundException(String.format("No user found with name %s.", name)));
    }
}
