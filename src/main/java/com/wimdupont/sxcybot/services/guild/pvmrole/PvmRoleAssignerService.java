package com.wimdupont.sxcybot.services.guild.pvmrole;

import com.wimdupont.sxcybot.enums.PvmRole;
import com.wimdupont.sxcybot.exceptions.EntityNotFoundException;
import com.wimdupont.sxcybot.repository.guild.dao.ChannelDetail;
import com.wimdupont.sxcybot.repository.guild.pvmrole.dao.PvmRoleUser;
import com.wimdupont.sxcybot.util.JdaUtil;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.channel.middleman.MessageChannel;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.awt.Color;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Transactional
public class PvmRoleAssignerService {

    public void postScoreboardAndAssignRoles(Guild guild, MessageChannel textChannel, Map<PvmRole,
            LinkedHashMap<PvmRoleUser, BigDecimal>> scoreBoard, ChannelDetail channelDetail, boolean persist) {
        try {
            for (PvmRole pvmRole : scoreBoard.keySet()) {
                var role = getRole(pvmRole, channelDetail, guild);
                EmbedBuilder embedBuilder = new EmbedBuilder();
                embedBuilder.setColor(Color.red);
                embedBuilder.setTitle(String.format("**%s** Scoreboard results:", role.getName()));
                if (scoreBoard.get(pvmRole).isEmpty()) {
                    embedBuilder.addField(String.format("No KC gains detected for %s", pvmRole.name().toLowerCase()), "No increase in KC found for participants, role will remain unchanged.", false);
                    textChannel.sendMessageEmbeds(embedBuilder.build()).queue();
                    continue;
                }
                List<Map.Entry<PvmRoleUser, BigDecimal>> entries = new ArrayList<>(scoreBoard.get(pvmRole).entrySet());
                entries.sort(Map.Entry.comparingByValue(Comparator.reverseOrder()));
                scoreBoard.get(pvmRole).clear();
                entries.forEach(f -> scoreBoard.get(pvmRole).put(f.getKey(), f.getValue()));
                guild.loadMembers().onSuccess(memberlist -> {
                    long limit = 5;
                    int i = 1;
                    int winningRank = 1;
                    for (Map.Entry<PvmRoleUser, BigDecimal> entry : scoreBoard.get(pvmRole).entrySet()) {
                        if (limit-- == 0) break;
                        Optional<Member> member = memberlist.stream()
                                .filter(f -> f.getUser().getId().equals(entry.getKey().getDiscordId())).findAny();
                        if (member.isPresent()) {
                            embedBuilder.addField(String.format("#%s %s", i, entry.getKey().getRsn()), String.format("%s with a score of %s!", member.get(), entry.getValue()), false);
                            if (i == winningRank && persist) {
                                guild.findMembersWithRoles(role)
                                        .onSuccess(oldWinners -> {
                                            if (oldWinners.isEmpty()) {
                                                addRoleToWinner(guild, member.get(), role, textChannel, entry.getKey().getRsn());
                                            } else {
                                                oldWinners.forEach(f -> guild.removeRoleFromMember(f, role).queue(complete ->
                                                        addRoleToWinner(guild, member.get(), role, textChannel, entry.getKey().getRsn())));
                                            }
                                        });
                            }
                        } else {
                            if (i == winningRank) winningRank++;
                            embedBuilder.addField(String.format("#%s %s", i, entry.getKey().getRsn()), String.format("%s with a score of %s!",
                                    JdaUtil.getNameById(memberlist, entry.getKey().getDiscordId())
                                            .orElse(String.format("{discordId:%s}", entry.getKey().getDiscordId())), entry.getValue()), false);
                            textChannel.sendMessage(String.format("No guildmember found for discordname %s. User did not receive any awards.",
                                    JdaUtil.getNameById(memberlist, entry.getKey().getDiscordId())
                                            .orElse(String.format("{discordId:%s}", entry.getKey().getDiscordId())))).queue();
                        }
                        i++;
                    }
                    textChannel.sendMessageEmbeds(embedBuilder.build()).queue();
                });
            }
        } catch (EntityNotFoundException e) {
            textChannel.sendMessage(e.getMessage()).queue();
        }
    }

    private Role getRole(PvmRole pvmRole, ChannelDetail channelDetail, Guild guild) throws EntityNotFoundException {
        var roleId = switch (pvmRole) {
            case GENERAL -> channelDetail.getPvmRoleGeneral();
            case RAIDS -> channelDetail.getPvmRoleRaids();
            case WILDERNESS -> channelDetail.getPvmRoleWilderness();
            default -> "Something went wrong.";
        };
        return guild.getRoles().stream().filter(f -> roleId.equals(f.getId())).findFirst()
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("No role %s found in the channel, role could not get assigned.", roleId)));
    }

    private void addRoleToWinner(Guild guild, Member member, Role role, MessageChannel textChannel, String rsn) {
        guild.addRoleToMember(member, role).queue(ok ->
                textChannel.sendMessage(String.format("%s is our new %s!", rsn, role)).queue());
    }
}
