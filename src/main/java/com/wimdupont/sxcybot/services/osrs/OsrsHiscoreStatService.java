package com.wimdupont.sxcybot.services.osrs;

import com.wimdupont.sxcybot.repository.osrs.OsrsHiscoreStatRepository;
import com.wimdupont.sxcybot.repository.osrs.dao.OsrsHiscoreStat;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class OsrsHiscoreStatService {

    private final OsrsHiscoreStatRepository osrsHiscoreStatRepository;

    public OsrsHiscoreStatService(OsrsHiscoreStatRepository osrsHiscoreStatRepository) {
        this.osrsHiscoreStatRepository = osrsHiscoreStatRepository;
    }

    public List<OsrsHiscoreStat> findAll() {
        return osrsHiscoreStatRepository.findAll(Sort.by("orderValue").ascending());
    }
}
