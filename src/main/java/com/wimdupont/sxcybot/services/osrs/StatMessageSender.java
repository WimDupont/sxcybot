package com.wimdupont.sxcybot.services.osrs;

import com.wimdupont.sxcybot.client.HiScoreClient;
import com.wimdupont.sxcybot.exceptions.EntityNotFoundException;
import com.wimdupont.sxcybot.model.OsrsStat;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.awt.Color;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

@Service
@Transactional
public class StatMessageSender {

    private final HiScoreClient hiScoreClient;

    public StatMessageSender(HiScoreClient hiScoreClient) {
        this.hiScoreClient = hiScoreClient;
    }

    public void sendStatMessage(MessageReceivedEvent event, EmbedBuilder embedBuilder,
                                Predicate<OsrsStat> predicate, Consumer<OsrsStat> consumer
            , Consumer<List<OsrsStat>> calcCombatLevel) {
        String msg = event.getMessage().getContentRaw();
        try {
            String player = msg.substring(msg.indexOf(" ")).trim();
            try {
                embedBuilder.setColor(Color.RED);
                embedBuilder.setTitle(String.format("Stats of %s", player));
                List<OsrsStat> osrsStatList = hiScoreClient.getHiScoreStats(player, event.getChannel());

                osrsStatList.stream().filter(predicate).forEach(consumer);
                if (calcCombatLevel != null) {
                    calcCombatLevel.accept(osrsStatList);
                }
                event.getChannel().sendMessageEmbeds(embedBuilder.build()).queue();
            } catch (EntityNotFoundException entityNotFoundException) {
                event.getChannel().sendMessage(entityNotFoundException.getMessage()).queue();
            }
        } catch (StringIndexOutOfBoundsException e) {
            event.getChannel().sendMessage("Please enter a player name after the command, separated by space.").queue();
        }
    }
}
