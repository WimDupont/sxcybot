package com.wimdupont.sxcybot.services.osrs;

import com.wimdupont.sxcybot.model.CombatDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Service
@Transactional
public class CombatCalculatorService {

    public double getCombatLevel(CombatDto combatDto) {
        double base = 0.25 * (combatDto.defenceLevel() + combatDto.hitpointsLevel() + ((double) combatDto.prayerLevel() / 2));

        double typeContribution = getMeleeRangeOrMagicCombatLevelContribution(combatDto.attackLevel(), combatDto.strengthLevel(), combatDto.magicLevel(), combatDto.rangeLevel());

        return BigDecimal.valueOf(base + typeContribution).setScale(3, RoundingMode.HALF_UP).doubleValue();
    }

    private double getMeleeRangeOrMagicCombatLevelContribution(int attackLevel, int strengthLevel, int magicLevel, int rangeLevel) {
        double melee = 0.325 * (attackLevel + strengthLevel);
        double range = 0.325 * ((double) rangeLevel / 2 + rangeLevel);
        double magic = 0.325 * ((double) magicLevel / 2 + magicLevel);

        return Math.max(melee, Math.max(range, magic));
    }
}
