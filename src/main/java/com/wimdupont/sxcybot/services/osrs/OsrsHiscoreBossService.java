package com.wimdupont.sxcybot.services.osrs;

import com.wimdupont.sxcybot.exceptions.EntityNotFoundException;
import com.wimdupont.sxcybot.repository.osrs.OsrsHiscoreBossRepository;
import com.wimdupont.sxcybot.repository.osrs.dao.OsrsHiscoreBoss;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class OsrsHiscoreBossService {

    private final OsrsHiscoreBossRepository osrsHiscoreBossRepository;

    public OsrsHiscoreBossService(OsrsHiscoreBossRepository osrsHiscoreBossRepository) {
        this.osrsHiscoreBossRepository = osrsHiscoreBossRepository;
    }

    public List<OsrsHiscoreBoss> findAll() {
        return osrsHiscoreBossRepository.findAll(Sort.by("orderValue").ascending());
    }

    public OsrsHiscoreBoss findByName(String name) throws EntityNotFoundException {
        return osrsHiscoreBossRepository.findByName(name).orElseThrow(
                () -> new EntityNotFoundException(String.format("No hiscoreBoss found with name %s.", name)));
    }

    public OsrsHiscoreBoss save(OsrsHiscoreBoss osrsHiscoreBoss) {
        return osrsHiscoreBossRepository.save(osrsHiscoreBoss);
    }
}
