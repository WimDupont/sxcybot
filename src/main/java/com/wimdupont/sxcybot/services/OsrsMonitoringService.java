package com.wimdupont.sxcybot.services;

import com.wimdupont.sxcybot.client.HiScoreClient;
import com.wimdupont.sxcybot.exceptions.EntityNotFoundException;
import com.wimdupont.sxcybot.model.OsrsBossKc;
import com.wimdupont.sxcybot.repository.guild.pvmrole.dao.PvmRoleUser;
import com.wimdupont.sxcybot.services.guild.ChannelDetailService;
import com.wimdupont.sxcybot.services.guild.pvmrole.PvmRoleUserService;
import com.wimdupont.sxcybot.util.JdaUtil;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.channel.concrete.PrivateChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

@Service
public class OsrsMonitoringService {

    private final Logger LOGGER = LoggerFactory.getLogger("monitor");
    private final HiScoreClient hiScoreClient;
    private final PvmRoleUserService pvmRoleUserService;
    private final MailService mailService;
    private final String monitorUserName;
    private final String monitorBossName;
    private final String monitorBossKc;
    private final ChannelDetailService channelDetailService;

    public OsrsMonitoringService(HiScoreClient hiScoreClient,
                                 PvmRoleUserService pvmRoleUserService,
                                 MailService mailService,
                                 @Value("${monitor.user.name}") String monitorUserName,
                                 @Value("${monitor.boss.name}") String monitorBossName,
                                 @Value("${monitor.boss.kc}") String monitorBossKc,
                                 ChannelDetailService channelDetailService) {
        this.hiScoreClient = hiScoreClient;
        this.pvmRoleUserService = pvmRoleUserService;
        this.mailService = mailService;
        this.monitorUserName = monitorUserName;
        this.monitorBossName = monitorBossName;
        this.monitorBossKc = monitorBossKc;
        this.channelDetailService = channelDetailService;
    }

    @Scheduled(cron = "${monitor.schedule}")
    @SuppressWarnings("unused")
    public void monitor() {
        try {
            monitorHiscores();
            monitorPvmRoleUserHiscores(null);
            monitorDiscordMembers(null);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

    }

    public void monitorDiscordMembers(PrivateChannel privateChannel) {
        List<PvmRoleUser> unfound = new ArrayList<>();
        channelDetailService.getJda().getGuilds().forEach(guild ->
                guild.loadMembers().onSuccess(memberlist -> {
                    pvmRoleUserService.findAll().stream()
                            .filter(pvmRoleUser -> JdaUtil.getNameById(memberlist, pvmRoleUser.getDiscordId()).isEmpty())
                            .forEach(unfound::add);
                    messageDiscordMemberResults(unfound, privateChannel);
                })
        );
    }

    public void monitorPvmRoleUserHiscores(PrivateChannel privateChannel) {
        List<PvmRoleUser> unfound = new ArrayList<>();
        for (PvmRoleUser pvmRoleUser : pvmRoleUserService.findAll()) {
            try {
                hiScoreClient.getHiScoreBossKc(pvmRoleUser.getRsn(), null);
            } catch (Exception e) {
                unfound.add(pvmRoleUser);
            }
        }
        messageHiScoreResults(unfound, privateChannel);
    }

    private void messageDiscordMemberResults(List<PvmRoleUser> unfound, PrivateChannel privateChannel) {
        if (privateChannel == null) {
            if (!unfound.isEmpty()) {
                handleError(String.format("No Discord member found for: %s", unfound.stream()
                        .map(pvmRoleUser -> String.format("DiscordId: %s , Rsn: %s",
                                pvmRoleUser.getDiscordId(), pvmRoleUser.getRsn()))
                        .toList()));
            } else {
                LOGGER.info("All PvM Role competitors have been found in discord.");
            }
        } else {
            EmbedBuilder embedBuilder = new EmbedBuilder();
            embedBuilder.setColor(Color.red);
            embedBuilder.setTitle("PvM competitor Discord check");
            if (!unfound.isEmpty()) {
                embedBuilder.setDescription("No Discord Member found for following competitor RSN's:");
                unfound.forEach(competitor ->
                        embedBuilder.addField(competitor.getRsn(), String.format("DiscordId: %s", competitor.getDiscordId()), true));
            } else {
                embedBuilder.setDescription("All PvM role competitors have been found in discord.");
            }
            privateChannel.sendMessageEmbeds(embedBuilder.build()).queue();
        }
    }

    private void messageHiScoreResults(List<PvmRoleUser> unfound, PrivateChannel privateChannel) {
        if (privateChannel == null) {
            if (!unfound.isEmpty()) {
                handleError(String.format("No hiScores found for: %s", unfound.stream().map(PvmRoleUser::getRsn).toList()));
            } else {
                LOGGER.info("All PvM Role competitors have been found in the hiscores.");
            }
        } else {
            EmbedBuilder embedBuilder = new EmbedBuilder();
            embedBuilder.setColor(Color.red);
            embedBuilder.setTitle("PvM competitor Hiscore check");
            if (!unfound.isEmpty()) {
                embedBuilder.setDescription("No hiscores found for following competitor RSN's:");
                unfound.forEach(competitor ->
                        embedBuilder.addField(competitor.getRsn(), "", true));
            } else {
                embedBuilder.setDescription("All PvM role competitors have been found in the hiscores.");
            }
            privateChannel.sendMessageEmbeds(embedBuilder.build()).queue();
        }
    }

    public void monitorHiscores() throws EntityNotFoundException {
        List<OsrsBossKc> osrsBossKcs = hiScoreClient.getHiScoreBossKc(monitorUserName, null);
        OsrsBossKc wintertodtBossKc = osrsBossKcs.stream()
                .filter(f -> f.name().equals(monitorBossName))
                .findAny()
                .orElseThrow(() -> new EntityNotFoundException(String.format("No \"%s\" hiscores found.", monitorBossName)));

        if (wintertodtBossKc.kc().equals(monitorBossKc)) {
            LOGGER.info(String.format("Hiscores check is OK. (%s has %s kc on %s)", monitorUserName, monitorBossKc, monitorBossName));
        } else {
            handleError(String.format("Hiscores check not OK: %s kc for user %s is not %s, but is %s",
                    monitorBossName, monitorUserName, monitorBossKc, wintertodtBossKc.kc()));
        }
    }

    private void handleError(String message) {
        mailService.sendMail("SxcyBot Monitoring", message);
        LOGGER.error(message);
    }
}
