package com.wimdupont.sxcybot.services;

import com.wimdupont.sxcybot.exceptions.InsufficientPrivilegesException;
import com.wimdupont.sxcybot.repository.guild.dao.GuildRole;
import com.wimdupont.sxcybot.services.guild.GuildRoleService;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

@Service
public class AccessService {

    private final GuildRoleService guildRoleService;

    public AccessService(GuildRoleService guildRoleService) {
        this.guildRoleService = guildRoleService;
    }

    public void isAllowed(Stream<Role> roleStream, MessageReceivedEvent event, int elevation)
            throws InsufficientPrivilegesException {
        var guildRoles = guildRoleService.findAllByElevationLessThanEqual(elevation);
        if (roleStream.noneMatch(hasPrivilege(event, guildRoles)))
            throw new InsufficientPrivilegesException(guildRoles.stream()
                    .map(GuildRole::getDiscordId)
                    .flatMap(discordId -> event.getGuild().getRoles().stream()
                            .filter(f -> f.getId().equals(discordId)))
                    .map(Role::getName)
                    .toList());
    }

    private Predicate<Role> hasPrivilege(MessageReceivedEvent event, List<GuildRole> guildRoles) {
        return f -> f.hasPermission(Permission.ADMINISTRATOR)
                || (event.getMember() != null && event.getMember().isOwner())
                || guildRoles.stream()
                .map(GuildRole::getDiscordId)
                .anyMatch(e -> e.equalsIgnoreCase(f.getId()));
    }
}
