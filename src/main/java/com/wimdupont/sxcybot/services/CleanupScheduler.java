package com.wimdupont.sxcybot.services;

import com.wimdupont.sxcybot.repository.guild.ChatDataRepository;
import com.wimdupont.sxcybot.repository.guild.pvmrole.dao.PvmKcSnapshot;
import com.wimdupont.sxcybot.repository.guild.pvmrole.dao.PvmRoleUser;
import com.wimdupont.sxcybot.services.guild.PollService;
import com.wimdupont.sxcybot.services.guild.pvmrole.PvmKcSnapshotService;
import com.wimdupont.sxcybot.services.guild.pvmrole.PvmRoleUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.List;

@Component
public class CleanupScheduler {

    private static final Logger LOGGER = LoggerFactory.getLogger(CleanupScheduler.class);
    private static final int POLL_DAYS_VALID = 60;
    private static final int CHAT_DATA_VALID = 14;
    private final PvmKcSnapshotService pvmKcSnapshotService;
    private final PvmRoleUserService pvmRoleUserService;
    private final PollService pollService;
    private final ChatDataRepository chatDataRepository;

    public CleanupScheduler(PvmKcSnapshotService pvmKcSnapshotService,
                            PvmRoleUserService pvmRoleUserService,
                            PollService pollService,
                            ChatDataRepository chatDataRepository) {
        this.pvmKcSnapshotService = pvmKcSnapshotService;
        this.pvmRoleUserService = pvmRoleUserService;
        this.pollService = pollService;
        this.chatDataRepository = chatDataRepository;
    }

    @Scheduled(cron = "${db.cleanup.schedule}")
    @SuppressWarnings("unused")
    public void cleanup() {
        cleanupPvmKcSnapshots();
        cleanupPolls();
        cleanupChatData();
    }

    private void cleanupPvmKcSnapshots() {
        List<PvmKcSnapshot> pvmKcSnapshotList;
        for (PvmRoleUser pvmRoleUser : pvmRoleUserService.findAll()) {
            pvmKcSnapshotList = pvmKcSnapshotService.findAllByPvmRoleUser(pvmRoleUser);
            if (pvmKcSnapshotList.size() > 3) {
                pvmKcSnapshotList = pvmKcSnapshotList.stream()
                        .sorted(Comparator.comparing(PvmKcSnapshot::getCreatedDate).reversed())
                        .skip(2)
                        .toList();
                LOGGER.info("deleted {} from RSN: {}, discordId: {}",
                        pvmKcSnapshotList.size(), pvmRoleUser.getRsn(), pvmRoleUser.getDiscordId());
                pvmKcSnapshotService.deleteAll(pvmKcSnapshotList);
            }
        }
    }

    private void cleanupPolls() {
        pollService.findAll().stream()
                .filter(f -> ChronoUnit.DAYS.between(f.getCreatedDate(), LocalDateTime.now()) > POLL_DAYS_VALID)
                .peek(f -> LOGGER.info("removed poll {} from {}", f.getMessageId(), f.getCreatedDate()))
                .forEach(pollService::delete);
    }

    private void cleanupChatData() {
        chatDataRepository.findAll().stream()
                .filter(f -> ChronoUnit.DAYS.between(f.getCreatedDateTime(), LocalDateTime.now()) > CHAT_DATA_VALID)
                .peek(f -> LOGGER.info("removed chat data {} from {}", f.getMessageId(), f.getCreatedDateTime()))
                .forEach(chatDataRepository::delete);
    }
}
