package com.wimdupont.sxcybot.services;

import com.wimdupont.sxcybot.enums.PvmRole;
import com.wimdupont.sxcybot.exceptions.EntityNotFoundException;
import com.wimdupont.sxcybot.repository.guild.dao.ChannelDetail;
import com.wimdupont.sxcybot.repository.guild.pvmrole.dao.PvmKcSnapshot;
import com.wimdupont.sxcybot.repository.guild.pvmrole.dao.PvmRoleUser;
import com.wimdupont.sxcybot.services.guild.ChannelDetailService;
import com.wimdupont.sxcybot.services.guild.pvmrole.PvmRoleAssignerService;
import com.wimdupont.sxcybot.services.guild.pvmrole.PvmRoleSnapshotComparatorService;
import com.wimdupont.sxcybot.services.guild.pvmrole.PvmRoleUserService;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.channel.concrete.PrivateChannel;
import net.dv8tion.jda.api.entities.channel.middleman.MessageChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Component
public class PvMRoleResolver {

    private static final Logger LOGGER = LoggerFactory.getLogger(PvMRoleResolver.class);
    private final PvmRoleUserService pvmRoleUserService;
    private final ChannelDetailService channelDetailService;
    private final PvmRoleSnapshotComparatorService pvmRoleSnapshotComparatorService;
    private final PvmRoleAssignerService pvmRoleAssignerService;

    public PvMRoleResolver(PvmRoleUserService pvmRoleUserService,
                           ChannelDetailService channelDetailService,
                           PvmRoleSnapshotComparatorService pvmRoleSnapshotComparatorService,
                           PvmRoleAssignerService pvmRoleAssignerService) {
        this.pvmRoleUserService = pvmRoleUserService;
        this.channelDetailService = channelDetailService;
        this.pvmRoleSnapshotComparatorService = pvmRoleSnapshotComparatorService;
        this.pvmRoleAssignerService = pvmRoleAssignerService;
    }

    @Scheduled(cron = "${pvm.role.schedule}")
    @SuppressWarnings("unused")
    public void resolvePvMRoles() {
        LOGGER.info("Running scheduled pvmRoleResolver.");
        resolvePvMRoles(true, null);
    }

    public void resolvePvMRoles(boolean persist, PrivateChannel privateChannel) {
        Map<PvmRole, LinkedHashMap<PvmRoleUser, BigDecimal>> scoreBoard = new HashMap<>();
        for (PvmRole pvmRole : PvmRole.values()) {
            if (!pvmRole.equals(PvmRole.UNUSED))
                scoreBoard.put(pvmRole, new LinkedHashMap<>());
        }
        channelDetailService.getJda().getGuilds().forEach(guild ->
                channelDetailService.findAll().forEach(channelDetail -> {
                            var textChannel = getTextChannel(guild, channelDetail, privateChannel);
                            for (PvmRoleUser pvmRoleUser : pvmRoleUserService.findAll()) {
                                List<PvmKcSnapshot> pvmKcSnapshots = new ArrayList<>();
                                try {
                                    pvmKcSnapshots.add(pvmRoleSnapshotComparatorService.takeSnapshot(textChannel, pvmRoleUser, false, persist));
                                } catch (EntityNotFoundException e) {
                                    textChannel.sendMessage(e.getMessage()).queue();
                                    continue;
                                }
                                pvmRoleSnapshotComparatorService.updateScoreboard(pvmRoleUser, scoreBoard, persist ? null : pvmKcSnapshots);
                            }
                            pvmRoleAssignerService.postScoreboardAndAssignRoles(guild, textChannel, scoreBoard, channelDetail, persist);
                        }
                )
        );
    }

    private MessageChannel getTextChannel(Guild guild, ChannelDetail channelDetail, PrivateChannel privateChannel) {
        MessageChannel textChannel = null;
        if (privateChannel == null) {
            textChannel = guild.getTextChannelById(channelDetail.getPvmRoleChannel());
        }
        return textChannel == null ? privateChannel : textChannel;
    }
}
