package com.wimdupont.sxcybot.exceptions;

public class EntityNotFoundException extends Exception {

    public EntityNotFoundException(String msg) {
        super(msg);
    }
}
