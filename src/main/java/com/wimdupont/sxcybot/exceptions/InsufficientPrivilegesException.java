package com.wimdupont.sxcybot.exceptions;

import java.util.List;

public class InsufficientPrivilegesException extends Exception {

    public InsufficientPrivilegesException(List<String> rolelist) {
        super(String.format("Only following ranks can execute this command: %s", rolelist));
    }
}
